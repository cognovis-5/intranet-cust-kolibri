# /packages/intranet-cust-kolibri/www/inactivate-freelancer.tcl
#
# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Inactivate a freelancer.
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2016-10-18
} {
    company_id:integer
}

# -----------------------------------------------------------
# Defaults
# -----------------------------------------------------------
set user_id [ad_maybe_redirect_for_registration]

    
# Check if the user has admin rights on the parent_id
# to allow freelancers to add sub-projects
im_project_permissions $user_id $company_id view read write admin
if {$write} { set perm_p 1 }
    
if {!$perm_p} { 
    ad_return_complaint "Insufficient Privileges" "
        <li>You don't have sufficient privileges to see this page."
    return
}

# set the company to inactive
db_dml update_company "update im_companies set company_status_id = [im_company_status_inactive] where company_id = :company_id"
set primary_contact_id [db_string primary_contact "select primary_contact_id from im_companies where company_id = :company_id" -default ""]

if {$primary_contact_id ne ""} {
    acs_user::change_state -user_id $primary_contact_id -state banned
}

ad_returnredirect [export_vars -base "/intranet/companies/view" {company_id}]