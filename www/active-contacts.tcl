ad_page_contract {
    Display the active users

} {
    {start_date ""}
    {end_date ""}
}

if {$start_date eq ""} {
    set start_year [clock format [clock seconds] -format "%Y"]
    set start_date "${start_year}-01-01"
}

if {$end_date eq ""} {
    set end_year [clock format [clock seconds] -format "%Y"]
    set end_date "${end_year}-12-31"
}

set contact_find_sql "
    select distinct user_id, 'kunde' as type from (
        select distinct company_contact_id as user_id
            from im_projects where ( 
                start_date between :start_date and :end_date OR
                end_date between :start_date and :end_date)
            and project_status_id not in (82,83)
        UNION
        select distinct company_contact_id as user_id
            from im_invoices i, im_costs c
            where c.cost_id = i.invoice_id
            and c.effective_date between :start_date and :end_date
            and c.cost_type_id in ([template::util::tcl_to_sql_list [im_sub_categories 3708]])
            and c.cost_status_id not in (3812,3818,3819)
    ) o
    UNION
    select distinct company_contact_id as user_id, 'freelancer' as type
        from im_invoices i, im_costs c
        where c.cost_id = i.invoice_id
        and c.effective_date between :start_date and :end_date
        and c.cost_type_id in ([template::util::tcl_to_sql_list [im_sub_categories 3710]])
        and c.cost_status_id not in (3812,3818,3819)
    "

set sql "select im_name_from_id(salutation_id) as salutation, first_names, last_name, email, type,c.user_id from 
cc_users c, ( $contact_find_sql ) t
where c.user_id = t.user_id"

db_multirow -extend [list locale] active_contacts active_contacts $sql {
    set locale [lang::user::locale -user_id $user_id]
}

template::multirow sort active_contacts type locale first_names last_name

template::list::create -key user_id -name active_contacts -multirow active_contacts \
    -elements {
        salutation {
            label "[_ intranet-core.Salutation]"
        }
        first_names {
            label "[_ intranet-core.First_names]"
        }
        last_name {
            label "[_ intranet-core.Last_name]"
        }
        email {
            label "[_ intranet-core.Email]"
        }
        locale {
            label "[_ intranet-core.User_Locale]"
        }
        type {
            label "[_ intranet-core.Type]"
        }
    }
