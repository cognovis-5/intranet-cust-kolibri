 ad_page_contract {
    
    Purpose: Download a report for the project hours
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2021-11-16
} {
    {start_date ""}
    {end_date ""}
}

if {$start_date eq ""} {
    set start_year [expr [clock format [clock seconds] -format "%Y"]]
    set start_date "${start_year}-01-01"
}
	
set where_clause_list [list "end_date >= '$start_date'"]
if {$end_date eq ""} {
    set end_date [clock format [clock seconds] -format "%Y-%m-%d"]
}
lappend where_clause_list "end_date <= '$end_date'"
	
set csv [new_CkCsv]
CkCsv_put_Delimiter $csv ";"
CkCsv_put_HasColumnNames $csv 1

CkCsv_SetColumnName $csv 0 "Projektnummer"
CkCsv_SetColumnName $csv 1 "Projektname"
CkCsv_SetColumnName $csv 2 "Projekt Typ"
CkCsv_SetColumnName $csv 3 "Kunde"
CkCsv_SetColumnName $csv 4 "Projektleiter"
CkCsv_SetColumnName $csv 5 "Start Datum"
CkCsv_SetColumnName $csv 6 "Ende Datum"
CkCsv_SetColumnName $csv 7 "Monat"
CkCsv_SetColumnName $csv 8 "Stunden"
CkCsv_SetColumnName $csv 9 "Stunden (akk.)"
CkCsv_SetColumnName $csv 10 "Rechnungen"
CkCsv_SetColumnName $csv 11 "Rechnungen (akk.)"
CkCsv_SetColumnName $csv 12 "Gutschriften"
CkCsv_SetColumnName $csv 13 "Gutschriften (akk.)"

set row 0
set project_ids [list]
set sql "select project_id, project_nr, project_name,
    im_name_from_id(project_type_id) as project_type,
    im_name_from_id(company_id) as company,
    im_name_from_id(project_lead_id) as project_lead,
    to_char(start_date, 'YYYY-MM-DD') as start_date,
    to_char(end_date, 'YYYY-MM-DD') as end_date
    from im_projects where [join $where_clause_list " and "] order by project_nr asc"

db_foreach projects $sql {
        lappend project_ids $project_id 
        set project_arr_nr($project_id) $project_nr
        set project_arr_name($project_id) $project_name
        set project_arr_type($project_id) $project_type
        set project_arr_lead($project_id) $project_lead
        set company_arr($project_id) $company
        set start_date_arr($project_id) $start_date
        set end_date_arr($project_id) $end_date
    }

foreach project_id $project_ids {
    set months [list]

    db_foreach hours {
        select sum(hours) as hours, to_char(day,'YYYY-MM') as month from im_hours where project_id = :project_id group by month order by month asc
    } {
        set logged_hours($month) $hours
        lappend months $month
    }

    db_foreach invoices {
        select sum(amount) as total_amount,to_char(effective_date,'YYYY-MM') as month from im_costs where project_id = :project_id and cost_type_id in (3700, 3725, 3740) group by month order by month asc
    } {
        set invoices($month) $total_amount
        if {[lsearch $months $month]<0} {
            lappend months $month
        }
    }

    db_foreach bills {
        select sum(amount) as total_amount,to_char(effective_date,'YYYY-MM') as month from im_costs where project_id = :project_id and cost_type_id in (3704, 3735, 3741) group by month order by month asc
    } {
        set bills($month) $total_amount
        if {[lsearch $months $month]<0} {
            lappend months $month
        }
    }

    set logged_akk 0
    set invoices_akk 0
    set bills_akk 0

    foreach month [lsort $months] {
        if {![info exists logged_hours($month)]} {set logged_hours($month) 0}
        set logged_akk [format "%.2f" [expr $logged_akk + $logged_hours($month)]]
        if {![info exists invoices($month)]} {set invoices($month) 0}
        set invoices_akk [format "%.2f" [expr $invoices_akk + $invoices($month)]]
        if {![info exists bills($month)]} {set bills($month) 0}
        set bills_akk [format "%.2f" [expr $bills_akk + $bills($month)]]
        regsub -all {\.} $logged_akk {,} logged_akk_pretty
        CkCsv_SetCell $csv $row 0 $project_arr_nr($project_id)
        CkCsv_SetCell $csv $row 1 $project_arr_name($project_id)
        CkCsv_SetCell $csv $row 2 $project_arr_type($project_id)
        CkCsv_SetCell $csv $row 3 $company_arr($project_id)
        CkCsv_SetCell $csv $row 4 $project_arr_lead($project_id)
        CkCsv_SetCell $csv $row 5 $start_date_arr($project_id)
        CkCsv_SetCell $csv $row 6 $end_date_arr($project_id)
        CkCsv_SetCell $csv $row 7 $month
        CkCsv_SetCell $csv $row 8 [im_numeric $logged_hours($month) "" "de_DE"]
        CkCsv_SetCell $csv $row 9 [im_numeric $logged_akk "" "de_DE"]
        CkCsv_SetCell $csv $row 10 [im_numeric $invoices($month) "%.2f" "de_DE"]
        CkCsv_SetCell $csv $row 11 [im_numeric $invoices_akk "%.2f" "de_DE"]
        CkCsv_SetCell $csv $row 12 [im_numeric $bills($month) "%.2f" "de_DE"]
        CkCsv_SetCell $csv $row 13 [im_numeric $bills_akk "%.2f" "de_DE"]
        incr row
        set logged_hours($month) 0 
        set invoices($month) 0 
        set bills($month) 0
    }

}

set csv_file "[ad_tmpnam].csv"
set success [CkCsv_SaveFile $csv $csv_file]
if {$success != 1} then {
    cog_log Error [CkCsv_lastErrorText $csv]
} else {
	set outputheaders [ns_conn outputheaders]
	ns_set cput $outputheaders "Content-Disposition" "attachment; filename=projects.csv"
	ns_returnfile 200 application/csv $csv_file
}

file delete $csv_file


