 ad_page_contract {
    
    Purpose: Download a report for the freelancers who have gotten at least three projects
    Name, E-Mail, Anrede, Anzahl der Gutschriften 2022 und 2021 wäre derzeit ausreichend.

    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2021-11-16
} {
    {locale "de-DE"}
    {years ""}
}

if {$years eq ""} {
    set years [db_list years "SELECT DATE_PART('year', now()) from dual union select date_part('year', now() -interval '1 year')"]
}
	
set where_clause_list [list "date_part('year', co.effective_date) in ([template::util::tcl_to_sql_list $years])"]
set skill_id [im_category_from_category -category $locale]
if {$skill_id ne ""} {
    lappend where_clause_list "primary_contact_id in (select user_id from im_freelance_skills where skill_id = $skill_id)"
} else {
    lappend where_clause_list "0 = 1"
}

lappend where_clause_list "c.company_id = co.provider_id"
lappend where_clause_list "co.cost_type_id = [im_cost_type_bill]"

set freelancer_ids [db_list freelancers "select distinct primary_contact_id from im_companies c, im_costs co
    where [join $where_clause_list " and "]
    "]

set csv [new_CkCsv]
CkCsv_put_HasColumnNames $csv 1
CkCsv_SetColumnName $csv 0 "First Name"
CkCsv_SetColumnName $csv 1 "Last Name"
CkCsv_SetColumnName $csv 2 "E-Mail"
CkCsv_SetColumnName $csv 3 "Salutation"

set ctr 4
foreach year $years {
    CkCsv_SetColumnName $csv $ctr $year
    incr ctr
}

set row 0
foreach freelancer_id $freelancer_ids {
    db_1row freelancer_info "select first_names, last_name, email from cc_users where user_id = :freelancer_id"

	CkCsv_SetCell $csv $row 0 $first_names
	CkCsv_SetCell $csv $row 1 $last_name
	CkCsv_SetCell $csv $row 2 $email
	CkCsv_SetCell $csv $row 3 [cog_salutation -person_id $freelancer_id]
    set ctr 4
    foreach year $years {
        CkCsv_SetCell $csv $row $ctr [db_string costs "select count(cost_id) as count from im_companies c, im_costs co where c.company_id = co.provider_id and date_part('year', co.effective_date) = :year and primary_contact_id = :freelancer_id" -default ""]

        incr ctr
    }

	incr row
}

CkCsv_SortByColumnIndex $csv 0 1 0
set csv_file "[ad_tmpnam].csv"
set success [CkCsv_SaveFile $csv $csv_file]
if {$success != 1} then {
    cog_log Error [CkCsv_lastErrorText $csv]
} else {
	set outputheaders [ns_conn outputheaders]
	ns_set cput $outputheaders "Content-Disposition" "attachment; filename=findocs.csv"
	ns_returnfile 200 application/csv $csv_file
}

file delete $csv_file


