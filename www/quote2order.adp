<!DOCTYPE html>
<html lang="de-DE">
<head>
    <meta charset="UTF-8" />
    <link rel="shortcut icon" href="https://www.kolibri.online/wp-content/uploads/2020/03/favicon.png" />
	<script src="https://kit.fontawesome.com/d2254a7d82.js" crossorigin="anonymous"></script>
    
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="pingback" href="https://www.kolibri.online/xmlrpc.php" />

    <script type="text/javascript">
        document.documentElement.className = 'js';
    </script>

    <meta name='robots' content='noindex, nofollow' />
<link rel="alternate" href="https://www.kolibri.online/danke/" hreflang="x-default" />
<link rel="alternate" hreflang="de" href="https://www.kolibri.online/danke/" />

<!-- Google Tag Manager for WordPress by gtm4wp.com -->
<script data-cfasync="false" data-pagespeed-no-defer>//<![CDATA[
	var gtm4wp_datalayer_name = "dataLayer";
	var dataLayer = dataLayer || [];
//]]>
</script>
<!-- End Google Tag Manager for WordPress by gtm4wp.com -->
	<!-- This site is optimized with the Yoast SEO plugin v17.3 - https://yoast.com/wordpress/plugins/seo/ -->
	<title>Danke - Kolibri Online</title>
	<meta property="og:locale" content="de_DE" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="Danke - Kolibri Online" />
	<meta property="og:url" content="https://www.kolibri.online/danke/" />
	<meta property="og:site_name" content="Kolibri Online" />
	<meta property="article:modified_time" content="2021-08-24T05:55:48+00:00" />
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:label1" content="Geschätzte Lesezeit" />
	<meta name="twitter:data1" content="1 Minute" />
	<script type="application/ld+json" class="yoast-schema-graph">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://www.kolibri.online/#organization","name":"KOLIBRI ONLINE","url":"https://www.kolibri.online/","sameAs":["https://www.linkedin.com/company/kolibri-online/?originalSubdomain=de"],"logo":{"@type":"ImageObject","@id":"https://www.kolibri.online/#logo","inLanguage":"de-DE","url":"https://www.kolibri.online/wp-content/uploads/2020/03/kolibri_logo.png","contentUrl":"https://www.kolibri.online/wp-content/uploads/2020/03/kolibri_logo.png","width":283,"height":86,"caption":"KOLIBRI ONLINE"},"image":{"@id":"https://www.kolibri.online/#logo"}},{"@type":"WebSite","@id":"https://www.kolibri.online/#website","url":"https://www.kolibri.online/","name":"Kolibri Online","description":"\u00dcbersetzungsb\u00fcro und Content-Marketing-Agentur","publisher":{"@id":"https://www.kolibri.online/#organization"},"potentialAction":[{"@type":"SearchAction","target":{"@type":"EntryPoint","urlTemplate":"https://www.kolibri.online/?s={search_term_string}"},"query-input":"required name=search_term_string"}],"inLanguage":"de-DE"},{"@type":"WebPage","@id":"https://www.kolibri.online/danke/#webpage","url":"https://www.kolibri.online/danke/","name":"Danke - Kolibri Online","isPartOf":{"@id":"https://www.kolibri.online/#website"},"datePublished":"2021-08-23T14:04:42+00:00","dateModified":"2021-08-24T05:55:48+00:00","breadcrumb":{"@id":"https://www.kolibri.online/danke/#breadcrumb"},"inLanguage":"de-DE","potentialAction":[{"@type":"ReadAction","target":["https://www.kolibri.online/danke/"]}]},{"@type":"BreadcrumbList","@id":"https://www.kolibri.online/danke/#breadcrumb","itemListElement":[{"@type":"ListItem","position":1,"name":"Startseite","item":"https://www.kolibri.online/"},{"@type":"ListItem","position":2,"name":"Danke"}]}]}</script>
	<!-- / Yoast SEO plugin. -->


<link rel='dns-prefetch' href='//www.kolibri.online' />
<link rel='dns-prefetch' href='//www.google.com' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Kolibri Online &raquo; Feed" href="https://www.kolibri.online/feed/" />
<link rel="alternate" type="application/rss+xml" title="Kolibri Online &raquo; Kommentar-Feed" href="https://www.kolibri.online/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.kolibri.online\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.8.1"}};
			!function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode;p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0);e=i.toDataURL();return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([10084,65039,8205,55357,56613],[10084,65039,8203,55357,56613])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(n=t.source||{}).concatemoji?c(n.concatemoji):n.wpemoji&&n.twemoji&&(c(n.twemoji),c(n.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<meta content="Kolibri v.1.0.0" name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='structured-content-frontend-css'  href='https://www.kolibri.online/wp-content/plugins/structured-content/dist/blocks.style.build.css?ver=1.4.6' type='text/css' media='all' />
<link rel='stylesheet' id='mec-select2-style-css'  href='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/packages/select2/select2.min.css?ver=5.20.5' type='text/css' media='all' />
<link rel='stylesheet' id='mec-font-icons-css'  href='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/css/iconfonts.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='mec-frontend-style-css'  href='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/css/frontend.min.css?ver=5.20.5' type='text/css' media='all' />
<link rel='stylesheet' id='mec-tooltip-style-css'  href='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/packages/tooltip/tooltip.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='mec-tooltip-shadow-style-css'  href='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/packages/tooltip/tooltipster-sideTip-shadow.min.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='featherlight-css'  href='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/packages/featherlight/featherlight.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='mec-google-fonts-css'  href='//fonts.googleapis.com/css?family=Montserrat%3A400%2C700%7CRoboto%3A100%2C300%2C400%2C700&#038;ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='mec-lity-style-css'  href='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/packages/lity/lity.min.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'  href='https://www.kolibri.online/wp-includes/css/dist/block-library/style.min.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='cognovis-po-integration-css'  href='https://www.kolibri.online/wp-content/plugins/cognovis-po-integration/public/css/cognovis-po-integration-public.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='cognovis-multi-select-css-css'  href='https://www.kolibri.online/wp-content/plugins/cognovis-po-integration/public/css/multi-select.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='dnd-upload-cf7-css'  href='https://www.kolibri.online/wp-content/plugins/drag-and-drop-multiple-file-upload-contact-form-7/assets/css/dnd-upload-cf7.css?ver=1.3.6.1' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='https://www.kolibri.online/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.5.1' type='text/css' media='all' />
<style id='contact-form-7-inline-css' type='text/css'>
.wpcf7 .wpcf7-recaptcha iframe {margin-bottom: 0;}.wpcf7 .wpcf7-recaptcha[data-align="center"] > div {margin: 0 auto;}.wpcf7 .wpcf7-recaptcha[data-align="right"] > div {margin: 0 0 0 auto;}
</style>
<link rel='stylesheet' id='google_business_reviews_rating_wp_css-css'  href='https://www.kolibri.online/wp-content/plugins/g-business-reviews-rating/wp/css/css.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='ppress-frontend-css'  href='https://www.kolibri.online/wp-content/plugins/wp-user-avatar/assets/css/frontend.min.css?ver=3.1.19' type='text/css' media='all' />
<link rel='stylesheet' id='ppress-flatpickr-css'  href='https://www.kolibri.online/wp-content/plugins/wp-user-avatar/assets/flatpickr/flatpickr.min.css?ver=3.1.19' type='text/css' media='all' />
<link rel='stylesheet' id='ppress-select2-css'  href='https://www.kolibri.online/wp-content/plugins/wp-user-avatar/assets/select2/select2.min.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='wpcf7-redirect-script-frontend-css'  href='https://www.kolibri.online/wp-content/plugins/wpcf7-redirect/build/css/wpcf7-redirect-frontend.min.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='wpml-legacy-horizontal-list-0-css'  href='//www.kolibri.online/wp-content/plugins/sitepress-multilingual-cms/templates/language-switchers/legacy-list-horizontal/style.min.css?ver=1' type='text/css' media='all' />
<link rel='stylesheet' id='wpml-menu-item-0-css'  href='//www.kolibri.online/wp-content/plugins/sitepress-multilingual-cms/templates/language-switchers/menu-item/style.min.css?ver=1' type='text/css' media='all' />
<link rel='stylesheet' id='mc4wp-form-basic-css'  href='https://www.kolibri.online/wp-content/plugins/mailchimp-for-wp/assets/css/form-basic.css?ver=4.8.6' type='text/css' media='all' />
<link rel='stylesheet' id='pmfcf-sweetalert2-style-css'  href='https://www.kolibri.online/wp-content/plugins/popup-message-for-contact-form-7-pro/css/sweetalert2.min.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='pmfcf-style-css'  href='https://www.kolibri.online/wp-content/plugins/popup-message-for-contact-form-7-pro/css/style.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='divi-parent-css'  href='https://www.kolibri.online/wp-content/themes/Divi/style.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='divi-style-css'  href='https://www.kolibri.online/wp-content/themes/Divi-child/style.css?ver=4.8.0' type='text/css' media='all' />
<link rel='stylesheet' id='Divi-Blog-Extras-styles-css'  href='https://www.kolibri.online/wp-content/plugins/Divi-Blog-Extras/styles/style.min.css?ver=2.6.3' type='text/css' media='all' />
<link rel='stylesheet' id='divi-styles-css'  href='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/app/addons/divi/styles/style.min.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='et-builder-googlefonts-cached-css'  href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,regular,italic,600,600italic,700,700italic,800,800italic&#038;subset=latin,latin-ext&#038;display=swap' type='text/css' media='all' />
<link rel='stylesheet' id='ewd-ufaq-rrssb-css'  href='https://www.kolibri.online/wp-content/plugins/ultimate-faqs/assets/css/rrssb-min.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='ewd-ufaq-jquery-ui-css'  href='https://www.kolibri.online/wp-content/plugins/ultimate-faqs/assets/css/jquery-ui.min.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='ewd-ufaq-css-css'  href='https://www.kolibri.online/wp-content/plugins/ultimate-faqs/assets/css/ewd-ufaq.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='popup-maker-site-css'  href='//www.kolibri.online/wp-content/uploads/pum/pum-site-styles.css?generated=1630667791&#038;ver=1.16.2' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='https://www.kolibri.online/wp-includes/css/dashicons.min.css?ver=5.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='cf7cf-style-css'  href='https://www.kolibri.online/wp-content/plugins/cf7-conditional-fields/style.css?ver=2.0.4' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-ui-smoothness-css'  href='https://www.kolibri.online/wp-content/plugins/contact-form-7/includes/js/jquery-ui/themes/smoothness/jquery-ui.min.css?ver=1.12.1' type='text/css' media='screen' />
<script type='text/javascript' src='https://www.kolibri.online/wp-includes/js/jquery/jquery.min.js?ver=3.6.0' id='jquery-core-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
<script type='text/javascript' id='mec-frontend-script-js-extra'>
/* <![CDATA[ */
var mecdata = {"day":"Tag","days":"Tage","hour":"Stunde","hours":"Stunden","minute":"Minute","minutes":"Minuten","second":"Sekunde","seconds":"Sekunden","elementor_edit_mode":"no","recapcha_key":"","ajax_url":"https:\/\/www.kolibri.online\/wp-admin\/admin-ajax.php","fes_nonce":"5e3febb81e","current_year":"2021","current_month":"10","datepicker_format":"dd.mm.yy&d.m.Y"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/js/frontend.js?ver=5.20.5' id='mec-frontend-script-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/js/events.js?ver=5.20.5' id='mec-events-script-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/cognovis-po-integration/public/js/cognovis-po-integration-public.js?ver=1.0.0' id='cognovis-po-integration-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/cognovis-po-integration/public/js/jquery.multi-select.js?ver=1.0.0' id='jquery-multiselect-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/g-business-reviews-rating/wp/js/js.js?ver=5.8.1' id='google_business_reviews_rating_wp_js-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/wp-user-avatar/assets/flatpickr/flatpickr.min.js?ver=5.8.1' id='ppress-flatpickr-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/wp-user-avatar/assets/select2/select2.min.js?ver=5.8.1' id='ppress-select2-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/duracelltomi-google-tag-manager/js/gtm4wp-contact-form-7-tracker.js?ver=1.13.1' id='gtm4wp-contact-form-7-tracker-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/popup-message-for-contact-form-7-pro/js/popupscript.js?ver=5.8.1' id='pmfcf-script-popupscript-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/popup-message-for-contact-form-7-pro/js/sweetalert2.all.min.js?ver=5.8.1' id='pmfcf-script-sweetalert2-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/popup-message-for-contact-form-7-pro/js/jscolor.js?ver=5.8.1' id='pmfcf-jscolor-js'></script>
<link rel="https://api.w.org/" href="https://www.kolibri.online/wp-json/" /><link rel="alternate" type="application/json" href="https://www.kolibri.online/wp-json/wp/v2/pages/18974" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.kolibri.online/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.kolibri.online/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.8.1" />
<link rel='shortlink' href='https://www.kolibri.online/?p=18974' />
<link rel="alternate" type="application/json+oembed" href="https://www.kolibri.online/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.kolibri.online%2Fdanke%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://www.kolibri.online/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.kolibri.online%2Fdanke%2F&#038;format=xml" />
<meta name="generator" content="WPML ver:4.4.12 stt:1,3;" />
<script type="text/javascript" src="https://secure.leadforensics.com/js/187017.js"></script>
<noscript><img src="https://secure.leadforensics.com/187017.png" alt="" style="display:none;" /></noscript>    
    <script type="text/javascript">
        var ajaxurl = 'https://www.kolibri.online/wp-admin/admin-ajax.php';
    </script>

<!-- Google Tag Manager for WordPress by gtm4wp.com -->
<script data-cfasync="false" data-pagespeed-no-defer>//<![CDATA[
	var dataLayer_content = {"visitorDoNotTrack":0,"visitorLoginState":"logged-out","pagePostType":"page","pagePostType2":"single-page","pagePostAuthor":"Marco Lupo"};
	dataLayer.push( dataLayer_content );//]]>
</script>
<script data-cfasync="false">//<![CDATA[
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.'+'js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KD8K6LB');//]]>
</script>
<!-- End Google Tag Manager -->
<!-- End Google Tag Manager for WordPress by gtm4wp.com --><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" /><link rel="preload" href="https://www.kolibri.online/wp-content/themes/Divi/core/admin/fonts/modules.ttf" as="font" crossorigin="anonymous"><link rel="shortcut icon" href="" /><!-- proconnects -->
<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin><link rel="preconnect" href="https://fonts.googleapis.com/" crossorigin><!-- fonts --><link rel="preload" as="font" href="https://www.kolibri.online/wp-content/themes/Divi/core/admin/fonts/modules.ttf" type="font/ttf" crossorigin><link rel="preload" as="font" href="https://www.kolibri.online/wp-content/themes/Divi-child/webfonts/ps/PlutoSansCondLight.woff2" type="font/woff2" crossorigin><link rel="preload" as="font" href="https://www.kolibri.online/wp-content/themes/Divi-child/webfonts/ps/PlutoSansCondBold.woff2" type="font/woff2" crossorigin><!-- scripts --><link rel="preload" as="script" href="https://www.kolibri.online/wp-content/plugins/wp-rocket/assets/js/lazyload/12.0/lazyload.min.js"><meta name="ahrefs-site-verification" content="472bab37903ef7f8a5472078ba994b7c8cb45c499758cf969f058f7517ed22fa"><!-- css --><link rel="preload" as="style" href="https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&amp;ver=5.3.2" crossorigin><!--MOBILE MENU CODE -->
<script type="text/javascript">
(function($) { 
    function setup_collapsible_submenus() {
        // mobile menu
        $('#mobile_menu .menu-item-has-children > a').after('<span class="menu-closed"></span>');
        $('#mobile_menu .menu-item-has-children > a').each(function() {
            $(this).next().next('.sub-menu').toggleClass('hide',1000);
        });
        $('#mobile_menu .menu-item-has-children > a + span').on('click', function(event) {
            event.preventDefault();
            $(this).toggleClass('menu-open');
            $(this).next('.sub-menu').toggleClass('hide',1000);
        });
    }
       
    $(window).load(function() {
        setTimeout(function() {
            setup_collapsible_submenus();
        }, 700);
    });
  
})(jQuery);
</script><style id="et-divi-customizer-global-cached-inline-styles">body,.et_pb_column_1_2 .et_quote_content blockquote cite,.et_pb_column_1_2 .et_link_content a.et_link_main_url,.et_pb_column_1_3 .et_quote_content blockquote cite,.et_pb_column_3_8 .et_quote_content blockquote cite,.et_pb_column_1_4 .et_quote_content blockquote cite,.et_pb_blog_grid .et_quote_content blockquote cite,.et_pb_column_1_3 .et_link_content a.et_link_main_url,.et_pb_column_3_8 .et_link_content a.et_link_main_url,.et_pb_column_1_4 .et_link_content a.et_link_main_url,.et_pb_blog_grid .et_link_content a.et_link_main_url,body .et_pb_bg_layout_light .et_pb_post p,body .et_pb_bg_layout_dark .et_pb_post p{font-size:18px}.et_pb_slide_content,.et_pb_best_value{font-size:20px}body{color:#000000}h1,h2,h3,h4,h5,h6{color:#000000}body{line-height:1.5em}.woocommerce #respond input#submit,.woocommerce-page #respond input#submit,.woocommerce #content input.button,.woocommerce-page #content input.button,.woocommerce-message,.woocommerce-error,.woocommerce-info{background:#c10830!important}#et_search_icon:hover,.mobile_menu_bar:before,.mobile_menu_bar:after,.et_toggle_slide_menu:after,.et-social-icon a:hover,.et_pb_sum,.et_pb_pricing li a,.et_pb_pricing_table_button,.et_overlay:before,.entry-summary p.price ins,.woocommerce div.product span.price,.woocommerce-page div.product span.price,.woocommerce #content div.product span.price,.woocommerce-page #content div.product span.price,.woocommerce div.product p.price,.woocommerce-page div.product p.price,.woocommerce #content div.product p.price,.woocommerce-page #content div.product p.price,.et_pb_member_social_links a:hover,.woocommerce .star-rating span:before,.woocommerce-page .star-rating span:before,.et_pb_widget li a:hover,.et_pb_filterable_portfolio .et_pb_portfolio_filters li a.active,.et_pb_filterable_portfolio .et_pb_portofolio_pagination ul li a.active,.et_pb_gallery .et_pb_gallery_pagination ul li a.active,.wp-pagenavi span.current,.wp-pagenavi a:hover,.nav-single a,.tagged_as a,.posted_in a{color:#c10830}.et_pb_contact_submit,.et_password_protected_form .et_submit_button,.et_pb_bg_layout_light .et_pb_newsletter_button,.comment-reply-link,.form-submit .et_pb_button,.et_pb_bg_layout_light .et_pb_promo_button,.et_pb_bg_layout_light .et_pb_more_button,.woocommerce a.button.alt,.woocommerce-page a.button.alt,.woocommerce button.button.alt,.woocommerce button.button.alt.disabled,.woocommerce-page button.button.alt,.woocommerce-page button.button.alt.disabled,.woocommerce input.button.alt,.woocommerce-page input.button.alt,.woocommerce #respond input#submit.alt,.woocommerce-page #respond input#submit.alt,.woocommerce #content input.button.alt,.woocommerce-page #content input.button.alt,.woocommerce a.button,.woocommerce-page a.button,.woocommerce button.button,.woocommerce-page button.button,.woocommerce input.button,.woocommerce-page input.button,.et_pb_contact p input[type="checkbox"]:checked+label i:before,.et_pb_bg_layout_light.et_pb_module.et_pb_button{color:#c10830}.footer-widget h4{color:#c10830}.et-search-form,.nav li ul,.et_mobile_menu,.footer-widget li:before,.et_pb_pricing li:before,blockquote{border-color:#c10830}.et_pb_counter_amount,.et_pb_featured_table .et_pb_pricing_heading,.et_quote_content,.et_link_content,.et_audio_content,.et_pb_post_slider.et_pb_bg_layout_dark,.et_slide_in_menu_container,.et_pb_contact p input[type="radio"]:checked+label i:before{background-color:#c10830}a{color:#c10830}.nav li ul{border-color:#abbdc9}#top-header,#et-secondary-nav li ul{background-color:#c10830}#top-menu li.current-menu-ancestor>a,#top-menu li.current-menu-item>a,#top-menu li.current_page_item>a,.et_color_scheme_red #top-menu li.current-menu-ancestor>a,.et_color_scheme_red #top-menu li.current-menu-item>a,.et_color_scheme_red #top-menu li.current_page_item>a,.et_color_scheme_pink #top-menu li.current-menu-ancestor>a,.et_color_scheme_pink #top-menu li.current-menu-item>a,.et_color_scheme_pink #top-menu li.current_page_item>a,.et_color_scheme_orange #top-menu li.current-menu-ancestor>a,.et_color_scheme_orange #top-menu li.current-menu-item>a,.et_color_scheme_orange #top-menu li.current_page_item>a,.et_color_scheme_green #top-menu li.current-menu-ancestor>a,.et_color_scheme_green #top-menu li.current-menu-item>a,.et_color_scheme_green #top-menu li.current_page_item>a{color:#333333}#main-footer{background-color:#c1072f}#footer-widgets .footer-widget a,#footer-widgets .footer-widget li a,#footer-widgets .footer-widget li a:hover{color:#ffffff}.footer-widget{color:#ffffff}#main-footer .footer-widget h4{color:#c10830}.footer-widget li:before{border-color:#c10830}#footer-widgets .footer-widget li:before{top:12.3px}.bottom-nav,.bottom-nav a,.bottom-nav li.current-menu-item a{color:#ffffff}#et-footer-nav .bottom-nav li.current-menu-item a{color:#ffffff}.bottom-nav,.bottom-nav a{font-size:10px}h1,h2,h3,h4,h5,h6,.et_quote_content blockquote p,.et_pb_slide_description .et_pb_slide_title{line-height:1.5em}.et_slide_in_menu_container,.et_slide_in_menu_container .et-search-field{letter-spacing:px}.et_slide_in_menu_container .et-search-field::-moz-placeholder{letter-spacing:px}.et_slide_in_menu_container .et-search-field::-webkit-input-placeholder{letter-spacing:px}.et_slide_in_menu_container .et-search-field:-ms-input-placeholder{letter-spacing:px}@media only screen and (min-width:981px){.et_pb_section{padding:0% 0}.et_pb_fullwidth_section{padding:0}.et_pb_row{padding:1% 0}.et-fixed-header#top-header,.et-fixed-header#top-header #et-secondary-nav li ul{background-color:#c10830}.et-fixed-header #top-menu li.current-menu-ancestor>a,.et-fixed-header #top-menu li.current-menu-item>a,.et-fixed-header #top-menu li.current_page_item>a{color:#333333!important}}@media only screen and (min-width:1350px){.et_pb_row{padding:13px 0}.et_pb_section{padding:0px 0}.single.et_pb_pagebuilder_layout.et_full_width_page .et_post_meta_wrapper{padding-top:40px}.et_pb_fullwidth_section{padding:0}}h1,h1.et_pb_contact_main_title,.et_pb_title_container h1{font-size:45px}h2,.product .related h2,.et_pb_column_1_2 .et_quote_content blockquote p{font-size:38px}h3{font-size:32px}h4,.et_pb_circle_counter h3,.et_pb_number_counter h3,.et_pb_column_1_3 .et_pb_post h2,.et_pb_column_1_4 .et_pb_post h2,.et_pb_blog_grid h2,.et_pb_column_1_3 .et_quote_content blockquote p,.et_pb_column_3_8 .et_quote_content blockquote p,.et_pb_column_1_4 .et_quote_content blockquote p,.et_pb_blog_grid .et_quote_content blockquote p,.et_pb_column_1_3 .et_link_content h2,.et_pb_column_3_8 .et_link_content h2,.et_pb_column_1_4 .et_link_content h2,.et_pb_blog_grid .et_link_content h2,.et_pb_column_1_3 .et_audio_content h2,.et_pb_column_3_8 .et_audio_content h2,.et_pb_column_1_4 .et_audio_content h2,.et_pb_blog_grid .et_audio_content h2,.et_pb_column_3_8 .et_pb_audio_module_content h2,.et_pb_column_1_3 .et_pb_audio_module_content h2,.et_pb_gallery_grid .et_pb_gallery_item h3,.et_pb_portfolio_grid .et_pb_portfolio_item h2,.et_pb_filterable_portfolio_grid .et_pb_portfolio_item h2{font-size:27px}h5{font-size:23px}h6{font-size:21px}.et_pb_slide_description .et_pb_slide_title{font-size:68px}.woocommerce ul.products li.product h3,.woocommerce-page ul.products li.product h3,.et_pb_gallery_grid .et_pb_gallery_item h3,.et_pb_portfolio_grid .et_pb_portfolio_item h2,.et_pb_filterable_portfolio_grid .et_pb_portfolio_item h2,.et_pb_column_1_4 .et_pb_audio_module_content h2{font-size:23px}	h1,h2,h3,h4,h5,h6{font-family:'Open Sans',Helvetica,Arial,Lucida,sans-serif}body,input,textarea,select{font-family:'Open Sans',Helvetica,Arial,Lucida,sans-serif}#top-menu a,#top-menu ul.sub-menu{-webkit-transition:none!important;-moz-transition:none!important;-o-transition:color 0 ease-in;transition:none}</style><style type="text/css">.mec-wrap, .mec-wrap div:not([class^="elementor-"]), .lity-container, .mec-wrap h1, .mec-wrap h2, .mec-wrap h3, .mec-wrap h4, .mec-wrap h5, .mec-wrap h6, .entry-content .mec-wrap h1, .entry-content .mec-wrap h2, .entry-content .mec-wrap h3, .entry-content .mec-wrap h4, .entry-content .mec-wrap h5, .entry-content .mec-wrap h6, .mec-wrap .mec-totalcal-box input[type="submit"], .mec-wrap .mec-totalcal-box .mec-totalcal-view span, .mec-agenda-event-title a, .lity-content .mec-events-meta-group-booking select, .lity-content .mec-book-ticket-variation h5, .lity-content .mec-events-meta-group-booking input[type="number"], .lity-content .mec-events-meta-group-booking input[type="text"], .lity-content .mec-events-meta-group-booking input[type="email"],.mec-organizer-item a { font-family: "Montserrat", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, sans-serif;}.mec-event-grid-minimal .mec-modal-booking-button:hover, .mec-events-timeline-wrap .mec-organizer-item a, .mec-events-timeline-wrap .mec-organizer-item:after, .mec-events-timeline-wrap .mec-shortcode-organizers i, .mec-timeline-event .mec-modal-booking-button, .mec-wrap .mec-map-lightbox-wp.mec-event-list-classic .mec-event-date, .mec-timetable-t2-col .mec-modal-booking-button:hover, .mec-event-container-classic .mec-modal-booking-button:hover, .mec-calendar-events-side .mec-modal-booking-button:hover, .mec-event-grid-yearly  .mec-modal-booking-button, .mec-events-agenda .mec-modal-booking-button, .mec-event-grid-simple .mec-modal-booking-button, .mec-event-list-minimal  .mec-modal-booking-button:hover, .mec-timeline-month-divider,  .mec-wrap.colorskin-custom .mec-totalcal-box .mec-totalcal-view span:hover,.mec-wrap.colorskin-custom .mec-calendar.mec-event-calendar-classic .mec-selected-day,.mec-wrap.colorskin-custom .mec-color, .mec-wrap.colorskin-custom .mec-event-sharing-wrap .mec-event-sharing > li:hover a, .mec-wrap.colorskin-custom .mec-color-hover:hover, .mec-wrap.colorskin-custom .mec-color-before *:before ,.mec-wrap.colorskin-custom .mec-widget .mec-event-grid-classic.owl-carousel .owl-nav i,.mec-wrap.colorskin-custom .mec-event-list-classic a.magicmore:hover,.mec-wrap.colorskin-custom .mec-event-grid-simple:hover .mec-event-title,.mec-wrap.colorskin-custom .mec-single-event .mec-event-meta dd.mec-events-event-categories:before,.mec-wrap.colorskin-custom .mec-single-event-date:before,.mec-wrap.colorskin-custom .mec-single-event-time:before,.mec-wrap.colorskin-custom .mec-events-meta-group.mec-events-meta-group-venue:before,.mec-wrap.colorskin-custom .mec-calendar .mec-calendar-side .mec-previous-month i,.mec-wrap.colorskin-custom .mec-calendar .mec-calendar-side .mec-next-month:hover,.mec-wrap.colorskin-custom .mec-calendar .mec-calendar-side .mec-previous-month:hover,.mec-wrap.colorskin-custom .mec-calendar .mec-calendar-side .mec-next-month:hover,.mec-wrap.colorskin-custom .mec-calendar.mec-event-calendar-classic dt.mec-selected-day:hover,.mec-wrap.colorskin-custom .mec-infowindow-wp h5 a:hover, .colorskin-custom .mec-events-meta-group-countdown .mec-end-counts h3,.mec-calendar .mec-calendar-side .mec-next-month i,.mec-wrap .mec-totalcal-box i,.mec-calendar .mec-event-article .mec-event-title a:hover,.mec-attendees-list-details .mec-attendee-profile-link a:hover,.mec-wrap.colorskin-custom .mec-next-event-details li i, .mec-next-event-details i:before, .mec-marker-infowindow-wp .mec-marker-infowindow-count, .mec-next-event-details a,.mec-wrap.colorskin-custom .mec-events-masonry-cats a.mec-masonry-cat-selected,.lity .mec-color,.lity .mec-color-before :before,.lity .mec-color-hover:hover,.lity .mec-wrap .mec-color,.lity .mec-wrap .mec-color-before :before,.lity .mec-wrap .mec-color-hover:hover,.leaflet-popup-content .mec-color,.leaflet-popup-content .mec-color-before :before,.leaflet-popup-content .mec-color-hover:hover,.leaflet-popup-content .mec-wrap .mec-color,.leaflet-popup-content .mec-wrap .mec-color-before :before,.leaflet-popup-content .mec-wrap .mec-color-hover:hover, .mec-calendar.mec-calendar-daily .mec-calendar-d-table .mec-daily-view-day.mec-daily-view-day-active.mec-color, .mec-map-boxshow div .mec-map-view-event-detail.mec-event-detail i,.mec-map-boxshow div .mec-map-view-event-detail.mec-event-detail:hover,.mec-map-boxshow .mec-color,.mec-map-boxshow .mec-color-before :before,.mec-map-boxshow .mec-color-hover:hover,.mec-map-boxshow .mec-wrap .mec-color,.mec-map-boxshow .mec-wrap .mec-color-before :before,.mec-map-boxshow .mec-wrap .mec-color-hover:hover, .mec-choosen-time-message, .mec-booking-calendar-month-navigation .mec-next-month:hover, .mec-booking-calendar-month-navigation .mec-previous-month:hover, .mec-yearly-view-wrap .mec-agenda-event-title a:hover, .mec-yearly-view-wrap .mec-yearly-title-sec .mec-next-year i, .mec-yearly-view-wrap .mec-yearly-title-sec .mec-previous-year i, .mec-yearly-view-wrap .mec-yearly-title-sec .mec-next-year:hover, .mec-yearly-view-wrap .mec-yearly-title-sec .mec-previous-year:hover, .mec-av-spot .mec-av-spot-head .mec-av-spot-box span, .mec-wrap.colorskin-custom .mec-calendar .mec-calendar-side .mec-previous-month:hover .mec-load-month-link, .mec-wrap.colorskin-custom .mec-calendar .mec-calendar-side .mec-next-month:hover .mec-load-month-link, .mec-yearly-view-wrap .mec-yearly-title-sec .mec-previous-year:hover .mec-load-month-link, .mec-yearly-view-wrap .mec-yearly-title-sec .mec-next-year:hover .mec-load-month-link, .mec-skin-list-events-container .mec-data-fields-tooltip .mec-data-fields-tooltip-box ul .mec-event-data-field-item a{color: #c10830}.mec-skin-carousel-container .mec-event-footer-carousel-type3 .mec-modal-booking-button:hover, .mec-wrap.colorskin-custom .mec-event-sharing .mec-event-share:hover .event-sharing-icon,.mec-wrap.colorskin-custom .mec-event-grid-clean .mec-event-date,.mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing > li:hover a i,.mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing .mec-event-share:hover .mec-event-sharing-icon,.mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing li:hover a i,.mec-wrap.colorskin-custom .mec-calendar:not(.mec-event-calendar-classic) .mec-selected-day,.mec-wrap.colorskin-custom .mec-calendar .mec-selected-day:hover,.mec-wrap.colorskin-custom .mec-calendar .mec-calendar-row  dt.mec-has-event:hover,.mec-wrap.colorskin-custom .mec-calendar .mec-has-event:after, .mec-wrap.colorskin-custom .mec-bg-color, .mec-wrap.colorskin-custom .mec-bg-color-hover:hover, .colorskin-custom .mec-event-sharing-wrap:hover > li, .mec-wrap.colorskin-custom .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected,.mec-wrap .flip-clock-wrapper ul li a div div.inn,.mec-wrap .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected,.event-carousel-type1-head .mec-event-date-carousel,.mec-event-countdown-style3 .mec-event-date,#wrap .mec-wrap article.mec-event-countdown-style1,.mec-event-countdown-style1 .mec-event-countdown-part3 a.mec-event-button,.mec-wrap .mec-event-countdown-style2,.mec-map-get-direction-btn-cnt input[type="submit"],.mec-booking button,span.mec-marker-wrap,.mec-wrap.colorskin-custom .mec-timeline-events-container .mec-timeline-event-date:before, .mec-has-event-for-booking.mec-active .mec-calendar-novel-selected-day, .mec-booking-tooltip.multiple-time .mec-booking-calendar-date.mec-active, .mec-booking-tooltip.multiple-time .mec-booking-calendar-date:hover, .mec-ongoing-normal-label, .mec-calendar .mec-has-event:after{background-color: #c10830;}.mec-booking-tooltip.multiple-time .mec-booking-calendar-date:hover, .mec-calendar-day.mec-active .mec-booking-tooltip.multiple-time .mec-booking-calendar-date.mec-active{ background-color: #c10830;}.mec-skin-carousel-container .mec-event-footer-carousel-type3 .mec-modal-booking-button:hover, .mec-timeline-month-divider, .mec-wrap.colorskin-custom .mec-single-event .mec-speakers-details ul li .mec-speaker-avatar a:hover img,.mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing > li:hover a i,.mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing .mec-event-share:hover .mec-event-sharing-icon,.mec-wrap.colorskin-custom .mec-event-list-standard .mec-month-divider span:before,.mec-wrap.colorskin-custom .mec-single-event .mec-social-single:before,.mec-wrap.colorskin-custom .mec-single-event .mec-frontbox-title:before,.mec-wrap.colorskin-custom .mec-calendar .mec-calendar-events-side .mec-table-side-day, .mec-wrap.colorskin-custom .mec-border-color, .mec-wrap.colorskin-custom .mec-border-color-hover:hover, .colorskin-custom .mec-single-event .mec-frontbox-title:before, .colorskin-custom .mec-single-event .mec-wrap-checkout h4:before, .colorskin-custom .mec-single-event .mec-events-meta-group-booking form > h4:before, .mec-wrap.colorskin-custom .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected,.mec-wrap .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected,.event-carousel-type1-head .mec-event-date-carousel:after,.mec-wrap.colorskin-custom .mec-events-masonry-cats a.mec-masonry-cat-selected, .mec-marker-infowindow-wp .mec-marker-infowindow-count, .mec-wrap.colorskin-custom .mec-events-masonry-cats a:hover, .mec-has-event-for-booking .mec-calendar-novel-selected-day, .mec-booking-tooltip.multiple-time .mec-booking-calendar-date.mec-active, .mec-booking-tooltip.multiple-time .mec-booking-calendar-date:hover, .mec-virtual-event-history h3:before, .mec-booking-tooltip.multiple-time .mec-booking-calendar-date:hover, .mec-calendar-day.mec-active .mec-booking-tooltip.multiple-time .mec-booking-calendar-date.mec-active{border-color: #c10830;}.mec-wrap.colorskin-custom .mec-event-countdown-style3 .mec-event-date:after,.mec-wrap.colorskin-custom .mec-month-divider span:before, .mec-calendar.mec-event-container-simple dl dt.mec-selected-day, .mec-calendar.mec-event-container-simple dl dt.mec-selected-day:hover{border-bottom-color:#c10830;}.mec-wrap.colorskin-custom  article.mec-event-countdown-style1 .mec-event-countdown-part2:after{border-color: transparent transparent transparent #c10830;}.mec-wrap.colorskin-custom .mec-box-shadow-color { box-shadow: 0 4px 22px -7px #c10830;}.mec-events-timeline-wrap .mec-shortcode-organizers, .mec-timeline-event .mec-modal-booking-button, .mec-events-timeline-wrap:before, .mec-wrap.colorskin-custom .mec-timeline-event-local-time, .mec-wrap.colorskin-custom .mec-timeline-event-time ,.mec-wrap.colorskin-custom .mec-timeline-event-location,.mec-choosen-time-message { background: rgba(193,8,48,.11);}.mec-wrap.colorskin-custom .mec-timeline-events-container .mec-timeline-event-date:after { background: rgba(193,8,48,.3);}</style><style type="text/css">mec-single-event .mec-events-meta-group-booking, .mec-single-event .mec-frontbox {
    margin-bottom: 30px;
    padding: 20px 30px;
    background: #fff;
    border: 1px solid #d7d7d7;
    border-radius: 5px !important;
}

.mec-single-event .mec-event-meta .mec-events-event-cost {
    font-size: 1em;
}

.mec-skin-carousel-container .mec-event-footer-carousel-type3 .mec-modal-booking-button:hover, .mec-wrap .mec-map-lightbox-wp.mec-event-list-classic .mec-event-date, .mec-wrap.colorskin-custom .mec-event-sharing .mec-event-share:hover .event-sharing-icon, .mec-wrap.colorskin-custom .mec-event-grid-clean .mec-event-date, .mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing > li:hover a i, .mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing .mec-event-share:hover .mec-event-sharing-icon, .mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing li:hover a i, .mec-wrap.colorskin-custom .mec-calendar:not(.mec-event-calendar-classic) .mec-selected-day, .mec-wrap.colorskin-custom .mec-calendar .mec-selected-day:hover, .mec-wrap.colorskin-custom .mec-calendar .mec-calendar-row dt.mec-has-event:hover, .mec-wrap.colorskin-custom .mec-calendar .mec-has-event:after, .mec-wrap.colorskin-custom .mec-bg-color, .mec-wrap.colorskin-custom .mec-bg-color-hover:hover, .colorskin-custom .mec-event-sharing-wrap:hover > li, .mec-wrap.colorskin-custom .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected, .mec-wrap .flip-clock-wrapper ul li a div div.inn, .mec-wrap .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected, .event-carousel-type1-head .mec-event-date-carousel, .mec-event-countdown-style3 .mec-event-date, #wrap .mec-wrap article.mec-event-countdown-style1, .mec-event-countdown-style1 .mec-event-countdown-part3 a.mec-event-button, .mec-wrap .mec-event-countdown-style2, .mec-map-get-direction-btn-cnt input[type="submit"], .mec-booking button, span.mec-marker-wrap, .mec-wrap.colorskin-custom .mec-timeline-events-container .mec-timeline-event-date:before {
    background-color: #c10830;
border-radius:5px !important;
}

.mec-event-cost, .mec-event-more-info, .mec-event-website, .mec-events-meta-date, .mec-single-event-additional-organizers, .mec-single-event-category, .mec-single-event-date, .mec-single-event-label, .mec-single-event-location, .mec-single-event-organizer, .mec-single-event-time {
    background: #f7f7f7;
    padding: 12px 14px 8px;
    margin-bottom: 12px;
    vertical-align: baseline;
    position: relative;
}

.widget_search input#s, .widget_search input#searchsubmit {
    display: none !important;
padding: .7em;
    height: 40px!important;
    margin: 0;
    font-size: 1em;
    line-height: normal!important;
    border: 1px solid #ddd;
    color: #666;
}

.mec-skin-carousel-container .mec-event-footer-carousel-type3 .mec-modal-booking-button:hover, .mec-timeline-month-divider, .mec-wrap.colorskin-custom .mec-single-event .mec-speakers-details ul li .mec-speaker-avatar a:hover img, .mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing > li:hover a i, .mec-wrap.colorskin-custom .mec-event-list-modern .mec-event-sharing .mec-event-share:hover .mec-event-sharing-icon, .mec-wrap.colorskin-custom .mec-event-list-standard .mec-month-divider span:before, .mec-wrap.colorskin-custom .mec-single-event .mec-social-single:before, .mec-wrap.colorskin-custom .mec-single-event .mec-frontbox-title:before, .mec-wrap.colorskin-custom .mec-calendar .mec-calendar-events-side .mec-table-side-day, .mec-wrap.colorskin-custom .mec-border-color, .mec-wrap.colorskin-custom .mec-border-color-hover:hover, .colorskin-custom .mec-single-event .mec-frontbox-title:before, .colorskin-custom .mec-single-event .mec-events-meta-group-booking form > h4:before, .mec-wrap.colorskin-custom .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected, .mec-wrap .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected, .event-carousel-type1-head .mec-event-date-carousel:after, .mec-wrap.colorskin-custom .mec-events-masonry-cats a.mec-masonry-cat-selected, .mec-marker-infowindow-wp .mec-marker-infowindow-count, .mec-wrap.colorskin-custom .mec-events-masonry-cats a:hover {
    border-color: #c10830;
border-radius:5px!important;
}

.mec-single-event .mec-events-meta-group-booking form>h4, .mec-single-event .mec-frontbox-title {
    text-transform: uppercase;
    font-size: 1em;
    font-weight: 700;
    color: #313131;
    border-bottom: 1px solid #c10830 !important;
    width: 100%;
    display: block;
    padding-bottom: 10px;
    position: relative;
    text-align: center;
}

h3 {
    font-size: 1em !important;
    color: #000 !important;
    font-family: "PlutoSansCondBold","Open Sans" !important;
}

p {
font-family: "PlutoSansCond","Open Sans" !important;
font-size: 1em !important;
}

.mec-single-event .mec-events-meta-group-booking form>h4:before, .mec-single-event .mec-frontbox-title:before {
    display: none;
    border-bottom: 4px solid #40d9f1;
    font-size: 6px;
    content: "";
    text-align: center;
    position: absolute;
    bottom: -4px;
    margin-left: -35px;
    left: 50%;
}

.hinweis-highlight {
    text-decoration: none;
    box-shadow: inset 0 -.5em 0 rgba(193,8,48,0.25);
    color: inherit;
}

.mec-single-event .mec-events-meta-group-booking .mec-event-ticket-available {
    display: none;
    margin-bottom: 20px;
    margin-top: -17px;
    font-size: 1em;
    color: #8a8a8a;
}

.mec-container [class*=col-] img {
    max-width: 100%;
border-radius: 5px !important;
}

.mec-single-event .mec-events-meta-group-booking, .mec-single-event .mec-frontbox {
    margin-bottom: 30px;
    padding: 20px 30px;
    background: #fff;
    border: 1px solid #d7d7d7;
border-radius: 5px !important;
    box-shadow: 0 2px 0 0 rgba(0,0,0,.016);
}

.mec-ticket-available-spots .mec-event-ticket-description, .mec-ticket-available-spots .mec-event-ticket-price {
    display: none;
}

.mec-events-meta-group-booking ul.mec-book-price-details li {
    font-size: 1em;
    color: #a9a9a9;
    list-style: none;
    padding: 13px 18px;
    margin: 0;
    float: left;
    border-right: 1px solid #eee;
    display: none;
}

</style>		
</head>
<body class="page-template-default page page-id-18974 et_button_icon_visible et_pb_button_helper_class et_fixed_nav et_show_nav et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns4 et_cover_background et_pb_gutter osx et_pb_gutters3 et_pb_pagebuilder_layout et_smooth_scroll et_no_sidebar et_divi_theme et-db et_minified_css">
    <div id="page-container">

    
    
            <header id="main-header" data-height-onload="66">
            <div class="container clearfix et_menu_container">
                            <div class="logo_container">
                    <span class="logo_helper"></span>
                    <a href="https://www.kolibri.online/">
                        <img src="https://www.kolibri.online/wp-content/uploads/2020/03/kolibri_logo.png" alt="Kolibri Online" id="logo" data-height-percentage="54" />
                    </a>
                </div>
                            
                <div id="et-top-navigation" data-height="66" data-fixed-height="40">
                                            <nav id="top-menu-nav">
                            
                <div class="contact-nav">
	                <a href="tel:+49 40 5247774-0">+49 40 5247774-0</a> | <a href="/kontakt/">Kontakt</a>                    
<div class="wpml-ls-statics-shortcode_actions wpml-ls wpml-ls-legacy-list-horizontal">
	<ul><li class="wpml-ls-slot-shortcode_actions wpml-ls-item wpml-ls-item-de wpml-ls-current-language wpml-ls-first-item wpml-ls-last-item wpml-ls-item-legacy-list-horizontal">
				<a href="https://www.kolibri.online/danke/" class="wpml-ls-link">
                    <span class="wpml-ls-native">DE</span></a>
			</li></ul>
</div>
                </div>
                            
                        <ul id="top-menu" class="nav"><li id="menu-item-1090" class="mega-menu triangle menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1090"><a href="https://www.kolibri.online/content-marketing/">Content Marketing</a>
<ul class="sub-menu">
	<li id="menu-item-1776" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1776"><a href="https://www.kolibri.online/content-marketing/strategie-ziele/">Strategie &#038; Ziele</a></li>
	<li id="menu-item-1777" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1777"><a href="https://www.kolibri.online/content-marketing/seo-texte/">SEO-Texte</a></li>
	<li id="menu-item-7420" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7420"><a href="https://www.kolibri.online/content-marketing/schulungen/">SEO-Schulungen</a></li>
	<li id="menu-item-8473" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8473"><a href="https://www.kolibri.online/content-marketing/blogartikel-schreiben-lassen/">Blog-Content</a></li>
	<li id="menu-item-1779" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1779"><a href="https://www.kolibri.online/content-marketing/video-content/">Video-Content</a></li>
</ul>
</li>
<li id="menu-item-982" class="mega-menu triangle menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-982"><a href="https://www.kolibri.online/fachuebersetzungen/">Fachübersetzungen</a>
<ul class="sub-menu">
	<li id="menu-item-1780" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1780"><a href="https://www.kolibri.online/fachuebersetzungen/seo-uebersetzung/">SEO-Übersetzung</a></li>
	<li id="menu-item-1781" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1781"><a href="https://www.kolibri.online/fachuebersetzungen/website-uebersetzung/">Website-Übersetzung</a></li>
	<li id="menu-item-1782" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1782"><a href="https://www.kolibri.online/fachuebersetzungen/marketing-uebersetzung/">Marketing-Übersetzung</a></li>
	<li id="menu-item-1783" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1783"><a href="https://www.kolibri.online/fachuebersetzungen/juristische-uebersetzung/">Juristische Übersetzung</a></li>
	<li id="menu-item-1784" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1784"><a href="https://www.kolibri.online/fachuebersetzungen/technische-uebersetzung/">Technische Übersetzung</a></li>
	<li id="menu-item-7866" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7866"><a href="https://www.kolibri.online/fachuebersetzungen/uebersetzung-und-lokalisierung-von-apps/">App-Übersetzung</a></li>
</ul>
</li>
<li id="menu-item-10610" class="triangle black-header not-mega-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-10610"><a href="https://www.kolibri.online/textkorrektur/">Textkorrektur</a>
<ul class="sub-menu">
	<li id="menu-item-10611" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10611"><a href="https://www.kolibri.online/textkorrektur/texte-lektorieren/">Lektorat</a></li>
	<li id="menu-item-10612" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10612"><a href="https://www.kolibri.online/textkorrektur/korrektorat-textkorrektur/">Korrektorat</a></li>
</ul>
</li>
<li id="menu-item-989" class="triangle black-header not-mega-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-989"><a href="https://www.kolibri.online/fachgebiete/">Fachgebiete</a>
<ul class="sub-menu">
	<li id="menu-item-2367" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2367"><a href="https://www.kolibri.online/fachgebiete/mode-und-textilbranche/">Mode- und Textilbranche</a></li>
	<li id="menu-item-2366" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2366"><a href="https://www.kolibri.online/fachgebiete/reise-und-tourismus/">Reise und Tourismus</a></li>
	<li id="menu-item-2365" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2365"><a href="https://www.kolibri.online/fachgebiete/metallindustrie-und-verarbeitung/">Metallindustrie und -verarbeitung</a></li>
	<li id="menu-item-2364" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2364"><a href="https://www.kolibri.online/fachgebiete/finanzen-und-versicherungen/">Finanzen und Versicherungen</a></li>
	<li id="menu-item-2363" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2363"><a href="https://www.kolibri.online/fachgebiete/food-und-rezepte/">Food und Rezepte</a></li>
	<li id="menu-item-2362" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2362"><a href="https://www.kolibri.online/fachgebiete/sport-und-fitness/">Sport und Fitness</a></li>
	<li id="menu-item-2361" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2361"><a href="https://www.kolibri.online/fachgebiete/kosmetik-und-koerperpflege/">Kosmetik und Körperpflege</a></li>
	<li id="menu-item-2360" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2360"><a href="https://www.kolibri.online/fachgebiete/automobilindustrie/">Automobilindustrie</a></li>
	<li id="menu-item-7619" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7619"><a href="https://www.kolibri.online/fachgebiete/gesundheitswesen-und-pflege/">Gesundheitswesen und Pflege</a></li>
	<li id="menu-item-8016" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8016"><a href="https://www.kolibri.online/fachgebiete/kanzleien-legal-techs/">Kanzleien und Legal Techs</a></li>
	<li id="menu-item-19819" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19819"><a href="https://www.kolibri.online/fachgebiete/content-marketing-und-fachuebersetzungen-fuer-wohnen-einrichten-und-deko/">Wohnen, Einrichten und Deko</a></li>
</ul>
</li>
<li id="menu-item-15" class="mega-menu triangle black-header menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-15"><a href="https://www.kolibri.online/sprachen/">Sprachen</a>
<ul class="sub-menu">
	<li id="menu-item-915" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-915"><a>Westeuropa</a>
	<ul class="sub-menu">
		<li id="menu-item-908" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-908"><a href="https://www.kolibri.online/sprachen/deutsch/">Deutsch</a></li>
		<li id="menu-item-907" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-907"><a href="https://www.kolibri.online/sprachen/englisch/">Englisch</a></li>
		<li id="menu-item-906" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-906"><a href="https://www.kolibri.online/sprachen/franzoesisch/">Französisch</a></li>
		<li id="menu-item-905" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-905"><a href="https://www.kolibri.online/sprachen/niederlaendisch/">Niederländisch</a></li>
	</ul>
</li>
	<li id="menu-item-914" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-914"><a>Nordeuropa</a>
	<ul class="sub-menu">
		<li id="menu-item-14" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14"><a href="https://www.kolibri.online/sprachen/daenisch/">Dänisch</a></li>
		<li id="menu-item-909" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-909"><a href="https://www.kolibri.online/sprachen/norwegisch/">Norwegisch</a></li>
		<li id="menu-item-910" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-910"><a href="https://www.kolibri.online/sprachen/schwedisch/">Schwedisch</a></li>
		<li id="menu-item-912" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-912"><a href="https://www.kolibri.online/sprachen/finnisch/">Finnisch</a></li>
		<li id="menu-item-911" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-911"><a href="https://www.kolibri.online/sprachen/islaendisch/">Isländisch</a></li>
	</ul>
</li>
	<li id="menu-item-928" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-928"><a>Südeuropa</a>
	<ul class="sub-menu">
		<li id="menu-item-934" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-934"><a href="https://www.kolibri.online/sprachen/spanisch/">Spanisch</a></li>
		<li id="menu-item-933" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-933"><a href="https://www.kolibri.online/sprachen/italienisch/">Italienisch</a></li>
		<li id="menu-item-932" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-932"><a href="https://www.kolibri.online/sprachen/griechisch/">Griechisch</a></li>
		<li id="menu-item-929" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-929"><a href="https://www.kolibri.online/sprachen/kroatisch/">Kroatisch</a></li>
		<li id="menu-item-930" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-930"><a href="https://www.kolibri.online/sprachen/slowenisch/">Slowenisch</a></li>
		<li id="menu-item-931" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-931"><a href="https://www.kolibri.online/sprachen/portugiesisch/">Portugiesisch</a></li>
	</ul>
</li>
	<li id="menu-item-951" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-951"><a>Osteuropa</a>
	<ul class="sub-menu">
		<li id="menu-item-959" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-959"><a href="https://www.kolibri.online/sprachen/polnisch/">Polnisch</a></li>
		<li id="menu-item-958" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-958"><a href="https://www.kolibri.online/sprachen/ungarisch/">Ungarisch</a></li>
		<li id="menu-item-957" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-957"><a href="https://www.kolibri.online/sprachen/tschechisch/">Tschechisch</a></li>
		<li id="menu-item-956" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-956"><a href="https://www.kolibri.online/sprachen/rumaenisch/">Rumänisch</a></li>
		<li id="menu-item-955" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-955"><a href="https://www.kolibri.online/sprachen/russisch/">Russisch</a></li>
		<li id="menu-item-954" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-954"><a href="https://www.kolibri.online/sprachen/ukrainisch/">Ukrainisch</a></li>
		<li id="menu-item-953" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-953"><a href="https://www.kolibri.online/sprachen/slowakisch/">Slowakisch</a></li>
		<li id="menu-item-952" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-952"><a href="https://www.kolibri.online/sprachen/bulgarisch/">Bulgarisch</a></li>
	</ul>
</li>
	<li id="menu-item-972" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-972"><a>Weitere Sprachen</a>
	<ul class="sub-menu">
		<li id="menu-item-977" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-977"><a href="https://www.kolibri.online/sprachen/arabisch/">Arabisch</a></li>
		<li id="menu-item-976" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-976"><a href="https://www.kolibri.online/sprachen/chinesisch/">Chinesisch</a></li>
		<li id="menu-item-975" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-975"><a href="https://www.kolibri.online/sprachen/japanisch/">Japanisch</a></li>
		<li id="menu-item-974" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-974"><a href="https://www.kolibri.online/sprachen/tuerkisch/">Türkisch</a></li>
		<li id="menu-item-973" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-973"><a href="https://www.kolibri.online/sprachen/koreanisch/">Koreanisch</a></li>
	</ul>
</li>
</ul>
</li>
<li id="menu-item-992" class="triangle not-mega-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-992"><a href="https://www.kolibri.online/ueber-uns/">Über uns</a>
<ul class="sub-menu">
	<li id="menu-item-11141" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11141"><a href="https://www.kolibri.online/ueber-uns/karriere/">Karriere</a></li>
	<li id="menu-item-3938" class="freelancer-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-3938"><a href="https://www.kolibri.online/freelancer/">Freelancer*innen</a></li>
</ul>
</li>
<li id="menu-item-8257" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8257"><a href="https://www.kolibri.online/cases/">Cases</a></li>
<li id="menu-item-995" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-995"><a href="https://www.kolibri.online/blog/">Blog</a></li>
<li id="menu-item-wpml-ls-2-de" class="menu-item wpml-ls-slot-2 wpml-ls-item wpml-ls-item-de wpml-ls-current-language wpml-ls-menu-item wpml-ls-first-item wpml-ls-last-item menu-item-type-wpml_ls_menu_item menu-item-object-wpml_ls_menu_item menu-item-wpml-ls-2-de"><a title="DE" href="https://www.kolibri.online/danke/"><span class="wpml-ls-display">DE</span></a></li>
</ul>                        </nav>
                    
                    
                    
                    
                    <div id="et_mobile_nav_menu">
				<div class="mobile_nav closed">
					<span class="select_page">Seite auswählen</span>
					<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
				</div>
			</div>                </div> <!-- #et-top-navigation -->
            </div> <!-- .container -->
            <div class="et_search_outer">
                <div class="container et_search_form_container">
                    <form role="search" method="get" class="et-search-form" action="https://www.kolibri.online/">
                    <input type="search" class="et-search-field" placeholder="Suchen &hellip;" value="" name="s" title="Suchen nach:" /><input type='hidden' name='lang' value='de' />                    </form>
                    <span class="et_close_search_field"></span>
                </div>
            </div>
        </header> <!-- #main-header -->
            <div id="et-main-area">
    
<div id="main-content">


			
				<article id="post-18974" class="post-18974 page type-page status-publish hentry">

				
					<div class="entry-content">
					<div id="et-boc" class="et-boc">
			
		<div class="et-l et-l--post">
			<div class="et_builder_inner_content et_pb_gutters3"><div class="et_pb_section et_pb_section_0 et_section_regular" >
				
				
				
				
					<div class="et_pb_row et_pb_row_0">
				<div class="et_pb_column et_pb_column_4_4 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough et-last-child">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_0  et_pb_text_align_left et_pb_bg_layout_light">
				
				
				<div class="et_pb_text_inner"><h1>Vielen Dank für Ihre Auftragsbestätigung</h1>
<p>leider gab es ein Problem bei der Bestätigung des Angebots:</p>
<p>@complaint;noquote@</p>
<p>Melden Sie sich doch bitte bei Ihrem/Ihrer Projektmanager*in @pm_name;noquote@.</p>
<p>Liebe Grüße<br />
<i>Ihr Kolibri-Team</i></div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row -->
				
				
			</div> <!-- .et_pb_section -->		</div><!-- .et_builder_inner_content -->
	</div><!-- .et-l -->
	
			
		</div><!-- #et-boc -->
							</div> <!-- .entry-content -->

				
				</article> <!-- .et_pb_post -->

			

</div> <!-- #main-content -->


	<span class="et_pb_scroll_top et-pb-icon"></span>


			<footer id="main-footer">
				
				
				<div class="socialbox-footer">
<a href="https://www.facebook.com/KolibriOnlineHH/?fref=ts" target="_blank" rel="noopener noreferrer"><div class="social-circle">
		<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" class="svg-inline--fa fa-facebook-f fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path></svg>
	</div></a>
<a href="https://www.instagram.com/kolibrionline/" target="_blank" rel="noopener noreferrer"><div class="social-circle">
		<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" class="svg-inline--fa fa-instagram fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path></svg>
	</div></a>
<a href="https://www.linkedin.com/company/8836740/" target="_blank" rel="noopener noreferrer"><div class="social-circle">
		<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin-in" class="svg-inline--fa fa-linkedin-in fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"></path></svg>
	</div></a>
</div>
		
				
<div class="container">
    <div id="footer-widgets" class="clearfix">
		<div class="footer-widget"><div id="custom_html-14" class="widget_text fwidget et_pb_widget widget_custom_html"><div class="textwidget custom-html-widget"><div class="widget-title"><a href="/content-marketing/">Content Marketing</a></div>
<div class="textwidget custom-html-widget">
	<a href="/content-marketing/strategie-ziele/">Strategie &amp; Ziele</a><br>
	<a href="/content-marketing/seo-texte/">SEO-Texte</a><br>
	<a href="/content-marketing/schulungen/">SEO-Schulungen</a><br>
<a href="/content-marketing/blogartikel-schreiben-lassen/">Blog-Content</a><br>
<a href="/content-marketing/video-content/">Video-Content</a>
	</div>
</div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-15" class="widget_text fwidget et_pb_widget widget_custom_html"><div class="textwidget custom-html-widget"><div class="widget-title"><a href="/fachuebersetzungen/">Fachübersetzungen</a></div>
<div class="textwidget custom-html-widget">
	<a href="/fachuebersetzungen/seo-uebersetzung/">SEO-Übersetzung</a><br>
	<a href="/fachuebersetzungen/website-uebersetzung/">Website-Übersetzung</a><br>
	<a href="/fachuebersetzungen/marketing-uebersetzung/">Marketingübersetzung</a><br>
<a href="/fachuebersetzungen/juristische-uebersetzung/">Juristische Übersetzung</a><br>
<a href="/fachuebersetzungen/technische-uebersetzung/">Technische Übersetzung</a><br>
	<a href="/fachuebersetzungen/uebersetzung-und-lokalisierung-von-apps/">App-Übersetzung</a></div>
</div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-17" class="widget_text fwidget et_pb_widget widget_custom_html"><div class="textwidget custom-html-widget"><div class="widget-title">
	<a href="/textkorrektur">Textkorrektur</a>
</div>
<div class="textwidget custom-html-widget">
<a href="/textkorrektur/texte-lektorieren">Lektorat</a><br>
<a href="/textkorrektur/korrektorat-textkorrektur">Korrektorat</a></div>
</div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-16" class="widget_text fwidget et_pb_widget widget_custom_html"><div class="textwidget custom-html-widget"><div class="widget-title">KOLIBRI ONLINE GMBH</div>
<div class="textwidget custom-html-widget"><span>Bernstorffstraße 128<br> D-22767 Hamburg<br> <a href="tel:+49 40 5247774-0">Tel.: +49 40 5247774-0</a><br> E-Mail:<a href="mailto:info@kolibri.online">info@kolibri.online</a></span> <p></p>
<p> <span> <a href="/impressum/">Impressum</a> </span> <span> <a href="/datenschutzerklaerung/">Datenschutz</a> </span> <span> <a href="/ueber-uns/">About us</a> </span> <span> <a href="/blog/">Blog</a> </span></p>
</div>
</div></div> <!-- end .fwidget --></div> <!-- end .footer-widget -->    </div> <!-- #footer-widgets -->
</div>    <!-- .container -->

		
				<div id="footer-bottom">
					<div class="container clearfix">
									</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->


	</div> <!-- #page-container -->

	<div id="pum-15792" class="pum pum-overlay pum-theme-10953 pum-theme-standard-theme popmake-overlay click_open" data-popmake="{&quot;id&quot;:15792,&quot;slug&quot;:&quot;neues-faq-hinzufuegen&quot;,&quot;theme_id&quot;:10953,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;medium&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}" role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_15792">

	<div id="popmake-15792" class="pum-container popmake theme-10953 pum-responsive pum-responsive-medium responsive size-medium">

				

				            <div id="pum_popup_title_15792" class="pum-title popmake-title">
				Neues FAQ hinzufügen			</div>
		

		

				<div class="pum-content popmake-content" tabindex="0">
			<p>Du findest die Antwort auf deine Frage nicht oder hast interessante Inhalte mit deinen Kollegen zu teilen?<br />
Erstelle einfach eine Frage. Solltest du eine Antwort benötigen, schicke es ohne Antwort ab.<br />
Solltest du Inhalte mit deinen Kollegen teilen wollen, gebe auch gleich die Antwort mit an.</p>
<style>.ewd-ufaq-post-margin-symbol { color: #c10830 !important; }.ewd-ufaq-post-margin-symbol { border-radius: 5px !important; }.ewd-ufaq-faq-header-title a { color: #000000 !important; }div.ewd-ufaq-faq-title h3 { color: #c10830 !important; }div.ewd-ufaq-faq-post p { color: #000000 !important; }div.ewd-ufaq-author-date { font-size: 12px !important; }div.ewd-ufaq-author-date { margin: 0px !important; }div.ewd-ufaq-faq-categories, div.ewd-ufaq-faq-tags { font-size: 12px !important; }/*Etoile Ultimate FAQ*/

.ewd-ufaq-field-label{
	text-transform:uppercase;
font-weight: bold;
color: #4b4b4b !important;
display: none;
}

.ewd-ufaq-text-input input.text, input.title, input[type=email], input[type=password], input[type=tel], input[type=text], select, textarea{
    border: 1px solid #4b4b4b;
    border-radius: 5px !important;
    background: #fff;
    padding: 15px 30px;
    width: 100%;
    font-size: 1em;
    color: #C1072F;
    transition: all 300ms ease 0ms;
    display: block;
margin:0px!important;
}

.ui-widget{
background-color: #fff;
}

.ui-widget-content{
background-color: #fff;
}

.ui-menu{
border-left: 1px solid #c10830 !important;
border-top: none !important;
border-right: 1px solid #c10830 !important;
border-bottom: 1px solid #c10830 !important;
border-radius: 0px 0px 5px 5px;
padding-top: 10px !important;
padding-bottom: 10px !important;
background-color: #fff;
}

.ui-menu-item-wrapper{
padding: 0px !important;
margin: 0px !important;
background-color: #fff;
}

.ui-menu-item-wrapper:hover{
background-color: #fff;
border: none !important;
padding: 0px !important;
margin: 0px !important;
}

.ui-menu-item{
font-size: 0.8em;
color: #4b4b4b;
padding: 0px !important;
margin: 0px !important;
background-color: #fff;
}

.ui-menu-item:hover{
background-color: #fff;
border: none !important;
font-size: 0.8em;
color: #c10830;
padding: 0px !important;
margin: 0px !important;
}

.ui-autocomplete{
background-color: #fff;
}

.ewd-ufaq-field-explanation{
color: #4b4b4b !important;
font-size: 0.7em;
}

label{
text-transform: uppercase;
font-weight: bold;
}

.button-primary{
background-color: #c10830;
color: #fff;
text-transform: uppercase;
text-align: center;
font-weight: bold;
border-radius: 5px !important;
font-size: 1em;
padding: 15px 30px 15px 30px;
border: none;
width: 100%;
}

.ui-menu ui-widget ui-widget-content ui-autocomplete ui-front {
margin-top: 20px !important;
}

.et_pb_with_background.et_section_regular {
    z-index: 10;
}

.ewd-ufaq-rating-button img{
width:24px!important;
height:24px!important;
}

.ewd-ufaq-rating-button span {
line-height: 1.5em;
padding-right: 15px;
}

.ewd-ufaq-faq-title .ewd-ufaq-post-margin-symbol span {
font-size: 1em;
}

div.ewd-ufaq-faq-title div.ewd-ufaq-post-margin-symbol {
padding: 5px;
}

.ewd-ufaq-ratings-label{
border-top: 1px dashed #4b4b4b;
padding-top:10px;
}

.ewd-ufaq-faq-post{
margin-bottom: 25px;
}

div.ewd-ufaq-faq-title h3{
margin: 0px!important;
font-family:'PlutoSansCondLight', 'Open Sans'!important;
}</style><div class='ewd-ufaq-question-form'>

	
	<form id='question_form' method='post' action='#'>

		<input type="hidden" id="_wp_nonce" name="_wp_nonce" value="3745c7a97a" /><input type="hidden" name="_wp_http_referer" value="/danke/" />
		<input type="hidden" name="_wp_http_referer" value="/danke/" />
		<div class='form-field'>

	<label id='ewd-ufaq-submit-faq-question' class='ewd-ufaq-submit-faq-label'>
		Frage:
	</label>

	<input type='text' name='faq_question' id='faq_question' value='' />

	<div id='ewd-ufaq-faq-question' class='ewd-ufaq-field-explanation'>
		
		<label for='explanation'></label>
		
		<span>
			Auf welche Frage möchtest du eine Antwort bekommen oder geben?		</span>

	</div>

</div>
		<div class='ewd-ufaq-meta-field'>

	<label for='faq_answer'>
		Vorgeschlagene Antwort:
	</label>

	<textarea name='faq_answer' class='ewd-ufaq-faq-textarea' required>
			</textarea>

	<div id='ewd-ufaq-answer-explanation' class='ewd-ufaq-field-explanation'>
		
		<label for='explanation'></label>
		
		<span>
			Optional: Wie ist die Antwort auf deine gestellte Frage?		</span>

	</div>

</div>
		
		<div class='form-field'>

	<label id='ewd-ufaq-faq-author' class='ewd-ufaq-faq-label'>
		Dein Name:
	</label>

	<input type='text' name='post_author' id='post_author' value='' />
		
	<div id='ewd-ufaq-author-explanation' class='ewd-ufaq-field-explanation'>
		
		<label for='explanation'></label>
		
		<span>
			(Vorname Nachname)		</span>

	</div>

</div>
		
		<div class='ewd-ufaq-captcha-div'>

	<label for='captcha_image'></label>
	<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAYCAIAAAA3ajm2AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAsklEQVRIie2V0QmAMAxEqziDbqbgfgqO5hb+ldJc0l5B7UfuS0pyviQ1DvN6hP40/g2A5ViMHIuRYzGa4Ol97fF52U55mCoGxBh5YmdJDXKdptbyNVpkUIopZkHlQ8xy7GSYmNbTrPLdqmkVLAaS1bQqaHcrNYUWle7NWbhb97Ub43ibScUyxvEBk4rFumf08AulKiEWRNHd2HZsd0G3IgTr2LBWNIFu9aBO/4mOxcixGD2axXjZEkoEDgAAAABJRU5ErkJggg==' alt='captcha' />
	<input type='hidden' name='ewd_ufaq_modified_captcha' value='20136' />

</div>

<div class='ewd-ufaq-captcha-response'><label for='captcha_text'>Bitte gebe die obenstehende Zahl ein: </label>
	<input type='text' name='ewd_ufaq_captcha' value='' />
</div>
		<p class='submit'>

			<input type='submit' name='submit_question' id='submit' class='button-primary' value='Abschicken'  />

		</p>

	</form>

</div>
		</div>


				

				            <button type="button" class="pum-close popmake-close" aria-label="Schließen">
			⨯            </button>
		
	</div>

</div>
<div id="pum-11723" class="pum pum-overlay pum-theme-10953 pum-theme-standard-theme popmake-overlay click_open" data-popmake="{&quot;id&quot;:11723,&quot;slug&quot;:&quot;angebot&quot;,&quot;theme_id&quot;:10953,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;cookie_name&quot;:&quot;&quot;,&quot;extra_selectors&quot;:&quot;.angebot-anfordern&quot;}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;medium&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}" role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_11723">

	<div id="popmake-11723" class="pum-container popmake theme-10953 pum-responsive pum-responsive-medium responsive size-medium">

				

				            <div id="pum_popup_title_11723" class="pum-title popmake-title">
				Fordern Sie ein unverbindliches Angebot an			</div>
		

		

				<div class="pum-content popmake-content" tabindex="0">
			<div role="form" class="wpcf7" id="wpcf7-f11722-o1" lang="de-DE" dir="ltr">
<div class="screen-reader-response"><p role="status" aria-live="polite" aria-atomic="true"></p> <ul></ul></div>
<form action="/danke/#wpcf7-f11722-o1" method="post" class="wpcf7-form init" novalidate="novalidate" data-status="init">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="11722" />
<input type="hidden" name="_wpcf7_version" value="5.5.1" />
<input type="hidden" name="_wpcf7_locale" value="de_DE" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f11722-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
<input type="hidden" name="_wpcf7_posted_data_hash" value="" />
<input type="hidden" name="_wpcf7cf_hidden_group_fields" value="" />
<input type="hidden" name="_wpcf7cf_hidden_groups" value="" />
<input type="hidden" name="_wpcf7cf_visible_groups" value="" />
<input type="hidden" name="_wpcf7cf_repeaters" value="[]" />
<input type="hidden" name="_wpcf7cf_steps" value="{}" />
<input type="hidden" name="_wpcf7cf_options" value="{&quot;form_id&quot;:11722,&quot;conditions&quot;:[{&quot;then_field&quot;:&quot;auswahl-seo-texte-deutsch-und-lektorat&quot;,&quot;and_rules&quot;:[{&quot;if_field&quot;:&quot;dienstleistung&quot;,&quot;operator&quot;:&quot;equals&quot;,&quot;if_value&quot;:&quot;SEO-Texte Deutsch&quot;}]},{&quot;then_field&quot;:&quot;auswahl-seo-texte-deutsch-und-lektorat&quot;,&quot;and_rules&quot;:[{&quot;if_field&quot;:&quot;dienstleistung&quot;,&quot;operator&quot;:&quot;equals&quot;,&quot;if_value&quot;:&quot;Lektorat\/Korrektorat&quot;}]},{&quot;then_field&quot;:&quot;auswahl-fachuebersetzung-seo-international-fremdsprachenlektorat-sonstiges&quot;,&quot;and_rules&quot;:[{&quot;if_field&quot;:&quot;dienstleistung&quot;,&quot;operator&quot;:&quot;equals&quot;,&quot;if_value&quot;:&quot;Fach\u00fcbersetzung&quot;}]},{&quot;then_field&quot;:&quot;auswahl-fachuebersetzung-seo-international-fremdsprachenlektorat-sonstiges&quot;,&quot;and_rules&quot;:[{&quot;if_field&quot;:&quot;dienstleistung&quot;,&quot;operator&quot;:&quot;equals&quot;,&quot;if_value&quot;:&quot;SEO-Texte International&quot;}]},{&quot;then_field&quot;:&quot;auswahl-fachuebersetzung-seo-international-fremdsprachenlektorat-sonstiges&quot;,&quot;and_rules&quot;:[{&quot;if_field&quot;:&quot;dienstleistung&quot;,&quot;operator&quot;:&quot;equals&quot;,&quot;if_value&quot;:&quot;Fremdsprachenlektorat&quot;}]},{&quot;then_field&quot;:&quot;auswahl-fachuebersetzung-seo-international-fremdsprachenlektorat-sonstiges&quot;,&quot;and_rules&quot;:[{&quot;if_field&quot;:&quot;dienstleistung&quot;,&quot;operator&quot;:&quot;equals&quot;,&quot;if_value&quot;:&quot;Sonstiges&quot;}]},{&quot;then_field&quot;:&quot;auswahl-fachuebersetzung-seo-international-fremdsprachenlektorat-sonstiges&quot;,&quot;and_rules&quot;:[{&quot;if_field&quot;:&quot;dienstleistung&quot;,&quot;operator&quot;:&quot;equals&quot;,&quot;if_value&quot;:&quot;&quot;}]}],&quot;settings&quot;:{&quot;animation&quot;:&quot;yes&quot;,&quot;animation_intime&quot;:200,&quot;animation_outtime&quot;:200,&quot;conditions_ui&quot;:&quot;normal&quot;,&quot;notice_dismissed&quot;:false}}" />
<input type="hidden" name="_wpcf7_recaptcha_response" value="" />
</div>
<h2><span style="font-weight:1.5em; color:inherit">1.</span> Projektangaben</h2>
<div class="form-box2-1"><label>Dienstleistung*<span class="contact-field select"><span class="wpcf7-form-control-wrap dienstleistung"><select name="dienstleistung" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false"><option value="">---</option><option value="Fachübersetzung">Fachübersetzung</option><option value="SEO-Texte Deutsch">SEO-Texte Deutsch</option><option value="SEO-Texte International">SEO-Texte International</option><option value="Lektorat/Korrektorat">Lektorat/Korrektorat</option><option value="Fremdsprachenlektorat">Fremdsprachenlektorat</option><option value="Sonstiges">Sonstiges</option></select></span></span></label></div>
<div data-id="auswahl-seo-texte-deutsch-und-lektorat" data-orig_data_id="auswahl-seo-texte-deutsch-und-lektorat"  data-class="wpcf7cf_group">
<div class="form-box2-2"><label>Ausgangssprache*<span class="contact-field"><span class="wpcf7-form-control-wrap ausgangssprache"><input type="text" name="ausgangssprache" value="Deutsch" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" readonly="readonly" aria-required="true" aria-invalid="false" /></span></span></label></div>
<p><label>Zielsprache(n)*: Mehrauswahl durch Halten der Strg-Taste möglich*<span class="contact-field input"><span class="wpcf7-form-control-wrap zielsprachen"><select name="zielsprachen[]" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false" multiple="multiple"><option value="Deutsch (Deutschland)">Deutsch (Deutschland)</option><option value="Deutsch (Österreich)">Deutsch (Österreich)</option><option value="Deutsch (Schweiz)">Deutsch (Schweiz)</option></select></span></span></label></p>
</div>
<div data-id="auswahl-fachuebersetzung-seo-international-fremdsprachenlektorat-sonstiges" data-orig_data_id="auswahl-fachuebersetzung-seo-international-fremdsprachenlektorat-sonstiges"  data-class="wpcf7cf_group">
<div class="form-box2-2"><label>Ausgangssprache*<span class="contact-field select"><span class="wpcf7-form-control-wrap ausgangssprache"><select name="ausgangssprache" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false"><option value="">---</option><option value="Arabisch">Arabisch</option><option value="Bulgarisch">Bulgarisch</option><option value="Chinesisch">Chinesisch</option><option value="Dänisch">Dänisch</option><option value="Deutsch">Deutsch</option><option value="Englisch">Englisch</option><option value="Estnisch">Estnisch</option><option value="Finnisch">Finnisch</option><option value="Französisch">Französisch</option><option value="Griechisch">Griechisch</option><option value="Italienisch">Italienisch</option><option value="Japanisch">Japanisch</option><option value="Koreanisch">Koreanisch</option><option value="Kroatisch">Kroatisch</option><option value="Litauisch">Litauisch</option><option value="Mazedonisch">Mazedonisch</option><option value="Niederländisch">Niederländisch</option><option value="Norwegisch">Norwegisch</option><option value="Polnisch">Polnisch</option><option value="Portugiesisch">Portugiesisch</option><option value="Rumänisch">Rumänisch</option><option value="Russisch">Russisch</option><option value="Schwedisch">Schwedisch</option><option value="Serbisch">Serbisch</option><option value="Slowenisch">Slowenisch</option><option value="Slowakisch">Slowakisch</option><option value="Spanisch">Spanisch</option><option value="Tschechisch">Tschechisch</option><option value="Türkisch">Türkisch</option><option value="Ukrainisch">Ukrainisch</option><option value="Ungarisch">Ungarisch</option></select></span></span></label></div>
<p><label>Zielsprache(n)*: Mehrauswahl durch Halten der Strg-Taste möglich<span class="contact-field input"><span class="wpcf7-form-control-wrap zielsprachen"><select name="zielsprachen[]" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false" multiple="multiple"><option value="Arabisch">Arabisch</option><option value="Bulgarisch">Bulgarisch</option><option value="Chinesisch (Festland)">Chinesisch (Festland)</option><option value="Chinesisch (Taiwan)">Chinesisch (Taiwan)</option><option value="Dänisch">Dänisch</option><option value="Deutsch (Deutschland)">Deutsch (Deutschland)</option><option value="Deutsch (Österreich)">Deutsch (Österreich)</option><option value="Deutsch (Schweiz)">Deutsch (Schweiz)</option><option value="Englisch (Britisch)">Englisch (Britisch)</option><option value="Englisch (USA)">Englisch (USA)</option><option value="Estnisch">Estnisch</option><option value="Finnisch">Finnisch</option><option value="Französisch (Frankreich)">Französisch (Frankreich)</option><option value="Französisch (Belgien)">Französisch (Belgien)</option><option value="Französisch (Schweiz)">Französisch (Schweiz)</option><option value="Griechisch">Griechisch</option><option value="Italienisch (Italien)">Italienisch (Italien)</option><option value="Italienisch (Schweiz)">Italienisch (Schweiz)</option><option value="Japanisch">Japanisch</option><option value="Koreanisch">Koreanisch</option><option value="Kroatisch">Kroatisch</option><option value="Litauisch">Litauisch</option><option value="Mazedonisch">Mazedonisch</option><option value="Niederländisch (Belgien)">Niederländisch (Belgien)</option><option value="Niederländisch (Niederlande)">Niederländisch (Niederlande)</option><option value="Norwegisch">Norwegisch</option><option value="Polnisch">Polnisch</option><option value="Portugiesisch (Brasilien)">Portugiesisch (Brasilien)</option><option value="Portugiesisch (Portugal)">Portugiesisch (Portugal)</option><option value="Rumänisch">Rumänisch</option><option value="Russisch">Russisch</option><option value="Schwedisch">Schwedisch</option><option value="Serbisch">Serbisch</option><option value="Slowenisch">Slowenisch</option><option value="Slowakisch">Slowakisch</option><option value="Spanisch (Argentinien)">Spanisch (Argentinien)</option><option value="Spanisch (Mexiko)">Spanisch (Mexiko)</option><option value="Spanisch (Spanien)">Spanisch (Spanien)</option><option value="Tschechisch">Tschechisch</option><option value="Türkisch">Türkisch</option><option value="Ukrainisch">Ukrainisch</option><option value="Ungarisch">Ungarisch</option><option value="Andere Sprachen">Andere Sprachen</option></select></span></span></label></p>
</div>
<h2><span style="font-weight:1.5em; color:inherit">2.</span> Dateienupload</h2>
<p><label>Übersetzungsdateien, Briefings und Referenzdateien<span class="contact-field submit" style="margin-left:20px"><span class="wpcf7-form-control-wrap upload_file"><input type="file" size="40" class="wpcf7-form-control wpcf7-drag-n-drop-file d-none" aria-invalid="false" multiple="multiple" data-name="upload_file" data-type="jpeg|jpg|xls|doc|xlsx|docx|pdf|png|idml|indd|zip" data-id="11722" accept=".jpeg, .jpg, .xls, .doc, .xlsx, .docx, .pdf, .png, .idml, .indd, .zip" /></span></label></span></p>
<h2><span style="font-weight:1.5em; color:inherit">3.</span> Wunschlieferdatum</h2>
<p><label>Wann soll das Projekt idealerweise abgeschlossen werden?<span class="contact-field kol-datetimepicker"><span class="wpcf7-form-control-wrap wunschliefertermin"><input type="date" name="wunschliefertermin" value="" class="wpcf7-form-control wpcf7-date wpcf7-validates-as-date" aria-invalid="false" /></span></span></label></p>
<h2><span style="font-weight:1.5em; color:inherit">4.</span> Kontaktdaten<span style="color:inherit">*</span></h2>
<div class="form-box2-1"><label>Firma<span class="contact-field input"><span class="wpcf7-form-control-wrap firma"><input type="text" name="firma" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" /></span></span></label></div>
<div class="form-box2-2"><label>Land*<span class="contact-field select"><span class="wpcf7-form-control-wrap land"><select name="land" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false"><option value="">---</option><option value="Dänemark">Dänemark</option><option value="Deutschland">Deutschland</option><option value="Frankreich">Frankreich</option><option value="Großbritannien">Großbritannien</option><option value="Italien">Italien</option><option value="Niederlande">Niederlande</option><option value="Österreich">Österreich</option><option value="Portugal">Portugal</option><option value="Schweiz">Schweiz</option><option value="Spanien">Spanien</option><option value="Vereinigte Staaten von Amerika">Vereinigte Staaten von Amerika</option></select></span></span></label></div>
<div class="form-box2-1"><label>Vorname*<span class="contact-field input"><span class="wpcf7-form-control-wrap vorname"><input type="text" name="vorname" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span></span></label></div>
<div class="form-box2-2"><label>Nachname*<span class="contact-field input"><span class="wpcf7-form-control-wrap nachname"><input type="text" name="nachname" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span></span></label></div>
<div class="form-box2-1"><label>E-Mail*<span class="contact-field input"><span class="wpcf7-form-control-wrap e-mail"><input type="email" name="e-mail" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span></span></label></div>
<div class="form-box2-2"><label>Telefon*<span class="contact-field input"><span class="wpcf7-form-control-wrap telefon"><input type="tel" name="telefon" value="" size="40" minlength="6" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" /></span></span></label></div>
<p><label>Anmerkungen<span class="wpcf7-form-control-wrap anmerkungen"><textarea name="anmerkungen" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea contact-field" aria-invalid="false"></textarea></span></label></p>
<p><span class="wpcf7-checkbox input" style="display:inline-block"><span class="wpcf7-form-control-wrap einverstaendnis"><span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item"><label><input type="checkbox" name="einverstaendnis" value="1" aria-invalid="false" /><span class="wpcf7-list-item-label"><span style="font-weight:normal; display:inline-block; margin-top:-1.5em;font-size: 70%;    line-height: 1.25;     text-transform: initial;">Ich habe die <a href="https://www.kolibri.online/datenschutzerklaerung/" target="_blank">Datenschutzerklärung</a> zur Kenntnis genommen. Ich stimme zu, dass meine Angaben und Daten zur Beantwortung meiner Anfrage elektronisch erhoben und gespeichert werden.<b>Hinweis:</b> Sie können jederzeit für die Zukunft Ihre Einwilligung widerrufen. Schreiben Sie uns dazu gerne eine E-Mail.</span></span></label></span></span></span></span></p>
<p style="margin-bottom: 1em;">
<span class="wpcf7-form-control-wrap recaptcha"><span data-sitekey="6LeLJlUcAAAAABLvWtwT_viLH1-rNBp9WT9uWlmd" class="wpcf7-form-control g-recaptcha wpcf7-recaptcha"></span>
<noscript>
	<div class="grecaptcha-noscript">
		<iframe src="https://www.google.com/recaptcha/api/fallback?k=6LeLJlUcAAAAABLvWtwT_viLH1-rNBp9WT9uWlmd" frameborder="0" scrolling="no" width="310" height="430">
		</iframe>
		<textarea name="g-recaptcha-response" rows="3" cols="40" placeholder="reCaptcha Response Here">
		</textarea>
	</div>
</noscript>
</span>
<p><span class="contact-field submit"><input type="submit" value="Angebot jetzt anfordern" class="wpcf7-form-control has-spinner wpcf7-submit" /></span></p>
<input type='hidden' class='wpcf7-pum' value='{"closepopup":false,"closedelay":5,"openpopup":false,"openpopup_id":11723}' /><div class="wpcf7-response-output" aria-hidden="true"></div></form></div>
		</div>


				

				            <button type="button" class="pum-close popmake-close" aria-label="Schließen">
			⨯            </button>
		
	</div>

</div>
<script type='text/javascript' src='https://www.kolibri.online/wp-includes/js/jquery/ui/core.min.js?ver=1.12.1' id='jquery-ui-core-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.12.1' id='jquery-ui-datepicker-js'></script>
<script type='text/javascript' id='jquery-ui-datepicker-js-after'>
jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Schlie\u00dfen","currentText":"Heute","monthNames":["Januar","Februar","M\u00e4rz","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember"],"monthNamesShort":["Jan","Feb","Mrz","Apr","Mai","Jun","Jul","Aug","Sep","Okt","Nov","Dez"],"nextText":"Weiter","prevText":"Vorherige","dayNames":["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"],"dayNamesShort":["So","Mo","Di","Mi","Do","Fr","Sa"],"dayNamesMin":["S","M","D","M","D","F","S"],"dateFormat":"d. MM yy","firstDay":1,"isRTL":false});});
</script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/js/isotope.pkgd.min.js?ver=5.20.5' id='mec-isotope-script-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/js/imagesload.js?ver=5.20.5' id='mec-imagesload-script-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/js/jquery.typewatch.js?ver=5.20.5' id='mec-typekit-script-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/packages/featherlight/featherlight.js?ver=5.20.5' id='featherlight-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/packages/select2/select2.full.min.js?ver=5.20.5' id='mec-select2-script-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/packages/tooltip/tooltip.js?ver=5.20.5' id='mec-tooltip-script-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/packages/lity/lity.min.js?ver=5.20.5' id='mec-lity-script-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/packages/colorbrightness/colorbrightness.min.js?ver=5.20.5' id='mec-colorbrightness-script-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/assets/packages/owl-carousel/owl.carousel.min.js?ver=5.20.5' id='mec-owl-carousel-script-js'></script>
<script type='text/javascript' id='et-builder-modules-global-functions-script-js-extra'>
/* <![CDATA[ */
var et_builder_utils_params = {"condition":{"diviTheme":true,"extraTheme":false},"scrollLocations":["app","top"],"builderScrollLocations":{"desktop":"app","tablet":"app","phone":"app"},"onloadScrollLocation":"app","builderType":"fe"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/themes/Divi/includes/builder/frontend-builder/build/frontend-builder-global-functions.js?ver=4.8.0' id='et-builder-modules-global-functions-script-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-includes/js/dist/vendor/regenerator-runtime.min.js?ver=0.13.7' id='regenerator-runtime-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=3.15.0' id='wp-polyfill-js'></script>
<script type='text/javascript' id='contact-form-7-js-extra'>
/* <![CDATA[ */
var wpcf7 = {"api":{"root":"https:\/\/www.kolibri.online\/wp-json\/","namespace":"contact-form-7\/v1"}};
var wpcf7 = {"api":{"root":"https:\/\/www.kolibri.online\/wp-json\/","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/contact-form-7/includes/js/index.js?ver=5.5.1' id='contact-form-7-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/drag-and-drop-multiple-file-upload-contact-form-7/assets/js/codedropz-uploader-min.js?ver=1.3.6.1' id='codedropz-uploader-js'></script>
<script type='text/javascript' id='dnd-upload-cf7-js-extra'>
/* <![CDATA[ */
var dnd_cf7_uploader = {"ajax_url":"https:\/\/www.kolibri.online\/wp-admin\/admin-ajax.php","ajax_nonce":"bcce8f4932","drag_n_drop_upload":{"tag":"h3","text":"Drag & Drop","or_separator":"oder","browse":"Dateien ausw\u00e4hlen","server_max_error":"Die Datei ist leider zu gro\u00df! Bitte maximal 10 MB pro Datei.","large_file":"Die Datei ist leider zu gro\u00df! Bitte maximal 10 MB pro Datei.","inavalid_type":"Dies ist ein nicht erlaubter Dateityp. Erlaubt sind: .pdf, .doc, .docx, .txt, .odt und .pages","max_file_limit":"10485760","required":"This field is required.","delete":{"text":"deleting","title":"Remove"}},"dnd_text_counter":"von","disable_btn":""};
var dnd_cf7_uploader = {"ajax_url":"https:\/\/www.kolibri.online\/wp-admin\/admin-ajax.php","ajax_nonce":"bcce8f4932","drag_n_drop_upload":{"tag":"h3","text":"Drag & Drop","or_separator":"oder","browse":"Dateien ausw\u00e4hlen","server_max_error":"Die Datei ist leider zu gro\u00df! Bitte maximal 10 MB pro Datei.","large_file":"Die Datei ist leider zu gro\u00df! Bitte maximal 10 MB pro Datei.","inavalid_type":"Dies ist ein nicht erlaubter Dateityp. Erlaubt sind: .pdf, .doc, .docx, .txt, .odt und .pages","max_file_limit":"10485760","required":"This field is required.","delete":{"text":"deleting","title":"Remove"}},"dnd_text_counter":"von","disable_btn":""};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/drag-and-drop-multiple-file-upload-contact-form-7/assets/js/dnd-upload-cf7.js?ver=1.3.6.1' id='dnd-upload-cf7-js'></script>
<script type='text/javascript' id='ppress-frontend-script-js-extra'>
/* <![CDATA[ */
var pp_ajax_form = {"ajaxurl":"https:\/\/www.kolibri.online\/wp-admin\/admin-ajax.php","confirm_delete":"Are you sure?","deleting_text":"Deleting...","deleting_error":"An error occurred. Please try again.","nonce":"cef242d2f3","disable_ajax_form":"false"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/wp-user-avatar/assets/js/frontend.min.js?ver=3.1.19' id='ppress-frontend-script-js'></script>
<script type='text/javascript' id='wpcf7-redirect-script-js-extra'>
/* <![CDATA[ */
var wpcf7r = {"ajax_url":"https:\/\/www.kolibri.online\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/wpcf7-redirect/build/js/wpcf7-redirect-frontend-script.js?ver=1.1' id='wpcf7-redirect-script-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/duracelltomi-google-tag-manager/js/gtm4wp-users.js?ver=1.13.1' id='gtm4wp-user-reg-login-script-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/themes/Divi-child/script.js?ver=5.8.1' id='divi-parentscript-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/themes/Divi/includes/builder/scripts/ext/jquery.mobile.custom.min.js?ver=4.8.0' id='et-jquery-touch-mobile-js'></script>
<script type='text/javascript' id='divi-custom-script-js-extra'>
/* <![CDATA[ */
var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/themes/Divi/js/custom.js?ver=4.8.0' id='divi-custom-script-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/themes/Divi/js/smoothscroll.js?ver=4.8.0' id='smooth-scroll-js'></script>
<script type='text/javascript' id='et-builder-modules-script-js-extra'>
/* <![CDATA[ */
var et_frontend_scripts = {"builderCssContainerPrefix":"#et-boc","builderCssLayoutPrefix":"#et-boc .et-l"};
var et_pb_custom = {"ajaxurl":"https:\/\/www.kolibri.online\/wp-admin\/admin-ajax.php","images_uri":"https:\/\/www.kolibri.online\/wp-content\/themes\/Divi\/images","builder_images_uri":"https:\/\/www.kolibri.online\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"8e503f4da4","subscription_failed":"Bitte \u00fcberpr\u00fcfen Sie die Felder unten aus, um sicherzustellen, dass Sie die richtigen Informationen eingegeben.","et_ab_log_nonce":"2d3f9cd086","fill_message":"Bitte f\u00fcllen Sie die folgenden Felder aus:","contact_error_message":"Bitte folgende Fehler beheben:","invalid":"Ung\u00fcltige E-Mail","captcha":"Captcha","prev":"Vorherige","previous":"Vorherige","next":"Weiter","wrong_captcha":"Sie haben die falsche Zahl im Captcha eingegeben.","wrong_checkbox":"Checkbox","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","ab_tests":[],"is_ab_testing_active":"","page_id":"18974","unique_test_id":"","ab_bounce_rate":"5","is_cache_plugin_active":"no","is_shortcode_tracking":"","tinymce_uri":""};
var et_pb_box_shadow_elements = [];
var et_pb_motion_elements = {"desktop":[],"tablet":[],"phone":[]};
var et_pb_sticky_elements = [];
/* ]]> */
</script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/themes/Divi/includes/builder/frontend-builder/build/frontend-builder-scripts.js?ver=4.8.0' id='et-builder-modules-script-js'></script>
<script type='text/javascript' id='Divi-Blog-Extras-frontend-bundle-js-extra'>
/* <![CDATA[ */
var DiviBlogExtrasFrontendData = {"ajaxurl":"https:\/\/www.kolibri.online\/wp-admin\/admin-ajax.php","ajax_nonce":"ae3ef07880","et_theme_accent_color":"#c10830"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/Divi-Blog-Extras/scripts/frontend-bundle.min.js?ver=2.6.3' id='Divi-Blog-Extras-frontend-bundle-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/modern-events-calendar/app/addons/divi/scripts/frontend-bundle.min.js?ver=1.0.0' id='divi-frontend-bundle-js'></script>
<script type='text/javascript' id='popup-maker-site-js-extra'>
/* <![CDATA[ */
var pum_vars = {"version":"1.16.2","pm_dir_url":"https:\/\/www.kolibri.online\/wp-content\/plugins\/popup-maker\/","ajaxurl":"https:\/\/www.kolibri.online\/wp-admin\/admin-ajax.php","restapi":"https:\/\/www.kolibri.online\/wp-json\/pum\/v1","rest_nonce":null,"default_theme":"10953","debug_mode":"","disable_tracking":"","home_url":"\/","message_position":"top","core_sub_forms_enabled":"1","popups":[],"analytics_route":"analytics","analytics_api":"https:\/\/www.kolibri.online\/wp-json\/pum\/v1"};
var pum_sub_vars = {"ajaxurl":"https:\/\/www.kolibri.online\/wp-admin\/admin-ajax.php","message_position":"top"};
var pum_popups = {"pum-15792":{"triggers":[],"cookies":[],"disable_on_mobile":false,"disable_on_tablet":false,"atc_promotion":null,"explain":null,"type_section":null,"theme_id":"10953","size":"medium","responsive_min_width":"0%","responsive_max_width":"100%","custom_width":"640px","custom_height_auto":false,"custom_height":"380px","scrollable_content":false,"animation_type":"fade","animation_speed":"350","animation_origin":"center top","open_sound":"none","custom_sound":"","location":"center","position_top":"100","position_bottom":"0","position_left":"0","position_right":"0","position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"zindex":"1999999999","close_button_delay":"0","fi_promotion":null,"close_on_form_submission":false,"close_on_form_submission_delay":"0","close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"standard-theme","id":15792,"slug":"neues-faq-hinzufuegen"},"pum-11723":{"triggers":[{"type":"click_open","settings":{"cookie_name":"","extra_selectors":".angebot-anfordern"}}],"cookies":[],"disable_on_mobile":false,"disable_on_tablet":false,"atc_promotion":null,"explain":null,"type_section":null,"theme_id":"10953","size":"medium","responsive_min_width":"0%","responsive_max_width":"100%","custom_width":"640px","custom_height_auto":false,"custom_height":"380px","scrollable_content":false,"animation_type":"fade","animation_speed":"350","animation_origin":"center top","open_sound":"none","custom_sound":"","location":"center top","position_top":"100","position_bottom":"0","position_left":"0","position_right":"0","position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"zindex":"1999999999","close_button_delay":"0","fi_promotion":null,"close_on_form_submission":true,"close_on_form_submission_delay":"0","close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"standard-theme","id":11723,"slug":"angebot"}};
/* ]]> */
</script>
<script type='text/javascript' src='//www.kolibri.online/wp-content/uploads/pum/pum-site-scripts.js?defer&#038;generated=1630667791&#038;ver=1.16.2' id='popup-maker-site-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/wpcf7-recaptcha/assets/js/wpcf7-recaptcha-controls.js?ver=1.2' id='wpcf7-recaptcha-controls-js'></script>
<script type='text/javascript' id='google-recaptcha-js-extra'>
/* <![CDATA[ */
var wpcf7iqfix = {"recaptcha_empty":"Bitte best\u00e4tige, dass du kein Roboter bist.","response_err":"wpcf7-recaptcha: reCaptcha-Antwort konnte nicht verifiziert werden."};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.google.com/recaptcha/api.js?hl=de_DE&#038;onload=recaptchaCallback&#038;render=explicit&#038;ver=2.0' id='google-recaptcha-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/themes/Divi/includes/builder/scripts/ext/jquery.fitvids.js?ver=4.8.0' id='divi-fitvids-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/themes/Divi/includes/builder/scripts/ext/waypoints.min.js?ver=4.8.0' id='waypoints-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/themes/Divi/includes/builder/scripts/ext/jquery.magnific-popup.js?ver=4.8.0' id='magnific-popup-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/themes/Divi/core/admin/js/common.js?ver=4.8.0' id='et-core-common-js'></script>
<script type='text/javascript' id='wpcf7cf-scripts-js-extra'>
/* <![CDATA[ */
var wpcf7cf_global_settings = {"ajaxurl":"https:\/\/www.kolibri.online\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/cf7-conditional-fields/js/scripts.js?ver=2.0.4' id='wpcf7cf-scripts-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-content/plugins/contact-form-7/includes/js/html5-fallback.js?ver=5.5.1' id='contact-form-7-html5-fallback-js'></script>
<script type='text/javascript' src='https://www.kolibri.online/wp-includes/js/wp-embed.min.js?ver=5.8.1' id='wp-embed-js'></script>

	<script>
	(function($) {
       
function setup_collapsible_submenus() {
     
var FirstLevel = $('.et_mobile_menu .first-level > a');
   
FirstLevel.off('click').click(function() {
$(this).attr('href', '#');  
$(this).parent().children().children().toggleClass('reveal-items');
$(this).toggleClass('icon-switch');
});
   
 
}
       
$(window).load(function() {
setTimeout(function() {
setup_collapsible_submenus();
}, 700);
});

/*Multiselct without ctrl --> start*/
})(jQuery);
		
		jQuery('option').mousedown(function(e) {
    e.preventDefault();
    jQuery(this).toggleClass('selected');
  
    jQuery(this).prop('selected', !jQuery(this).prop('selected'));
    return false;
});
/*Multiselct without ctrl --> end*/
		
	</script>

</body>
</html>