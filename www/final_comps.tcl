set final_companies [db_list final_company "select distinct final_company_id from im_projects"]

foreach final_company_id $final_companies {
    set tms [list]
    db_foreach final "select project_id, company_path from im_projects, im_companies c
where final_company_id = :final_company_id and final_company_id = c.company_id order by end_date desc" {
    set project_path [cog::project::path -project_id $project_id]
    set files [glob -nocomplain $project_path/Trados/TM/*${company_path}*]
    set trados_folder "[im_trans_trados_folder -company_id $final_company_id]/TM"
    if {![file isdirectory $trados_folder]} {
#        file delete $trados_folder
#        file mkdir $trados_folder
	#        file mkdir [im_trans_trados_folder -company_id $final_company_id]/TB
#        ds_comment "Not a directroy $trados_folder"
    } else {
        foreach file $files {
            set filename "[file tail $file]"
            if {[string match "Finalized*" $filename]} {
                regsub -all "Finalized_" $filename "" filename
	    } 
	    if {[file exists  $trados_folder/$filename]} {
		if { [file mtime $file] > [file mtime $trados_folder/$filename] } {
		    ds_comment "should copy $filename [file mtime $file] :: [file mtime $trados_folder/$filename]"
                    file copy -force $file $trados_folder/$filename
		} 
	    } else {
                    file copy $file $trados_folder/$filename
                    ds_comment "File not found $filename"
	    }
            
            if {[lsearch $tms $filename]<0} {
                lappend tms $filename
            }
        }
    }
}
}

set final_companies [db_list final_company "select distinct final_company_id from im_projects"]

foreach final_company_id $final_companies {
    set tms [list]
    db_foreach final "select project_id, company_path from im_projects, im_companies c
where final_company_id = :final_company_id and final_company_id = c.company_id order by end_date desc" {
    set project_path [cog::project::path -project_id $project_id]
    set files [glob -nocomplain $project_path/Trados/TB/*${company_path}*]
    set trados_folder "[im_trans_trados_folder -company_id $final_company_id]/TB"
    if {![file isdirectory $trados_folder]} {
#        file delete $trados_folder
#        file mkdir $trados_folder
	#        file mkdir [im_trans_trados_folder -company_id $final_company_id]/TB
#        ds_comment "Not a directroy $trados_folder"
    } else {
        foreach file $files {
            set filename "[file tail $file]"
            if {[string match "Finalized*" $filename]} {
                regsub -all "Finalized_" $filename "" filename
	    } 
	    if {[file exists  $trados_folder/$filename]} {
		if { [file mtime $file] > [file mtime $trados_folder/$filename] } {
		    ds_comment "should copy $filename [file mtime $file] :: [file mtime $trados_folder/$filename]"
                    file copy -force $file $trados_folder/$filename
		} 
	    } else {
                    file copy $file $trados_folder/$filename
                    ds_comment "File not found $filename"
	    }
            
            if {[lsearch $tms $filename]<0} {
                lappend tms $filename
            }
        }
    }
}
}


f {0} {
db_foreach test "select company_path,  project_id, project_nr, im_name_from_id(p.final_company_id) as company_name from im_projects p,  (select max(end_date) as end_date,final_company_id from im_projects group by final_company_id) s, im_companies c where c.company_id = p.final_company_id and s.final_company_id=p.final_company_id and s.end_date = p.end_date" {
#ds_comment "$company_name, $company_path, $project_nr"
# Check if we have a TM for the company
    set project_path [cog::project::path -project_id $project_id]
    set file [glob -nocomplain  $project_path/Trados/TM/*${company_path}*]
    if {$file ne ""} {
ds_comment $file
    }
}
}
