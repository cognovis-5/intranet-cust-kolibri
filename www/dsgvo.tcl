#
# This program is free software. You can redistribute it
# and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation;
# either version 2 of the License, or (at your option)
# any later version. This program is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.



# ---------------------------------------------------------------
# Disable triggers
# ---------------------------------------------------------------
db_dml disable_trigger "alter table im_projects disable trigger im_projects_update_tr"
db_dml disable_trigger "alter table im_projects disable trigger im_project_project_cost_center_update_tr"
db_dml disable_trigger "alter table im_projects disable trigger im_projects_calendar_update_tr"
db_dml disable_trigger "alter table im_projects disable trigger im_projects_project_cache_up_tr"
db_dml disable_trigger "alter table im_projects disable trigger im_projects_tsearch_tr"
db_dml disable_trigger "alter table im_trans_tasks disable trigger im_trans_tasks_calendar_update_tr"
db_dml disable_trigger "alter table im_trans_tasks disable trigger im_trans_tasks_calendar_update_tr"
db_dml disable_trigger "alter table im_companies disable trigger im_companies_tsearch_tr"
db_dml disable_trigger "alter table persons disable trigger persons_tsearch_tr"

# ---------------------------------------------------------------
# Alter tables constraints
# ---------------------------------------------------------------
db_dml on_delete_cascade "alter table users_contact drop CONSTRAINT users_contact_user_id_fk"
db_dml on_delete_cascade "alter table users_contact add constraint users_contact_user_id_fk foreign key (user_id) references persons(person_id) on delete cascade"
db_dml on_delete_cascade "alter table im_freelance_skills drop constraint im_fl_skills_user_fk"
db_dml on_delete_cascade "alter table im_freelance_skills add constraint im_fl_skills_user_fk foreign key (user_id) references users(user_id) on delete cascade"
db_dml on_delete_cascade "alter table im_freelancers drop constraint im_freelancers_user_fk"
db_dml on_delete_cascade "alter table im_freelancers add constraint im_freelancers_user_fk foreign key (user_id) references users(user_id) on delete cascade"
db_dml on_delete_cascade "alter table im_component_plugin_user_map drop constraint im_comp_plugin_user_map_user_fk"
db_dml on_delete_cascade "alter table im_component_plugin_user_map add constraint im_comp_plugin_user_map_user_fk foreign key (user_id) REFERENCES users(user_id) on delete cascade"
db_dml on_delete_cascade "alter table im_freelance_assignments drop constraint im_freelance_assignment_assignee_fk"
db_dml on_delete_cascade "alter table im_freelance_assignments add constraint im_freelance_assignment_assignee_fk FOREIGN KEY (assignee_id) REFERENCES users(user_id) on delete cascade"
db_dml on_delete_cascade "alter table im_trans_freelancer_quality drop constraint im_trans_fl_quality_user_fk"
db_dml on_delete_cascade "alter table im_trans_freelancer_quality add constraint im_trans_fl_quality_user_fk FOREIGN KEY (user_id) REFERENCES users(user_id) on delete cascade"
db_dml on_delete_cascade "alter table acs_mail_log_recipient_map drop constraint acs_mail_log_recipient_id_fk"
db_dml on_delete_cascade "alter table acs_mail_log_recipient_map add constraint acs_mail_log_recipient_id_fk FOREIGN KEY (recipient_id) REFERENCES parties(party_id) on delete cascade"
db_dml on_delete_cascade "alter table im_timesheet_prices drop constraint im_timesheet_prices_company_id"
db_dml on_delete_cascade "alter table im_timesheet_prices add constraint im_timesheet_prices_company_id FOREIGN KEY (company_id) REFERENCES im_companies(company_id) on delete cascade"
db_dml on_delete_cascade "alter table im_trans_price_history drop CONSTRAINT im_trans_price_history_company_fk"
db_dml on_delete_cascade "alter table im_trans_price_history add constraint im_trans_price_history_company_fk FOREIGN KEY (company_id) REFERENCES im_companies(company_id) on delete cascade"
db_dml on_delete_cascade "alter table im_trans_prices drop constraint im_trans_prices_company_id"
db_dml on_delete_cascade "alter table im_trans_prices add constraint im_trans_prices_company_id FOREIGN KEY (company_id) REFERENCES im_companies(company_id) on delete cascade"
db_dml on_delete_cascade "alter table im_trans_trados_matrix drop CONSTRAINT im_trans_matrix_cid_fk"
db_dml on_delete_cascade "alter table im_trans_trados_matrix add CONSTRAINT im_trans_matrix_cid_fk FOREIGN KEY (object_id) REFERENCES acs_objects(object_id) on delete cascade"
db_dml on_delete_cascade "alter table im_task_actions drop constraint im_task_action_user_fk"
db_dml on_delete_cascade "alter table im_task_actions add constraint im_task_action_user_fk FOREIGN KEY (user_id) REFERENCES users(user_id) on delete cascade"
db_dml on_delete_cascade "alter table im_forum_topic_user_map drop constraint im_forum_topics_um_topic_fk"
db_dml on_delete_cascade "alter table im_forum_topic_user_map add constraint im_forum_topics_um_topic_fk  FOREIGN KEY (topic_id) REFERENCES im_forum_topics(topic_id) on delete cascade"
db_dml on_delete_cascade "alter table im_forum_topics drop constraint im_forum_topics_object_fk"
db_dml on_delete_cascade "alter table im_forum_topics add constraint im_forum_topics_object_fk FOREIGN KEY (object_id) REFERENCES acs_objects(object_id) on delete cascade"
#db_dml on_delete_cascade "alter table im_fs_actions drop constraint \"$2\""
#db_dml on_delete_cascade "alter table im_fs_actions add constraint im_fs_actions_user_id_fk (user_id) REFERENCES persons(person_id) on delete cascade"
db_dml on_delete_cascade "alter table im_trans_main_freelancer drop CONSTRAINT im_trans_main_fl_user_fk"
db_dml on_delete_cascade "alter table im_trans_main_freelancer add CONSTRAINT im_trans_main_fl_user_fk FOREIGN KEY (freelancer_id) REFERENCES users(user_id) on delete cascade"

db_dml fixed_data {create or replace function im_company__delete (integer) returns integer as '
DECLARE
v_company_id alias for $1;
BEGIN
-- make sure to remove links from all offices to this company.
update im_offices
set company_id = null
where company_id = v_company_id;

-- Erase the im_companies item associated with the id
delete from im_companies
where company_id = v_company_id;

-- Delete entry from parties
delete from parties where party_id = v_company_id;

-- Erase all the priviledges
delete from acs_permissions
where object_id = v_company_id;

PERFORM acs_object__delete(v_company_id);

return 0;
    end;' language 'plpgsql'}

# ---------------------------------------------------------------
# Delete users without company
# ---------------------------------------------------------------
set to_delete_ids [db_list free "select gdmm.member_id
  from group_distinct_member_map gdmm
  where gdmm.group_id = 465
  and member_id not in (select object_id_two from acs_rels, im_companies where object_id_one = company_id)
"]

# ---------------------------------------------------------------
# Delete users with companies without costs
# ---------------------------------------------------------------

set to_delete_ids2 [db_list old_free "select object_id_two
from acs_rels 
where object_id_one in (select company_id 
  from im_companies c, acs_objects o 
  where object_id = company_id and creation_date < '2016-01-01' 
  and company_id not in (select provider_id from im_costs union select customer_id from im_costs) 
  and company_type_id in (56,58,10000301)) 
and rel_type = 'im_company_employee_rel' 
and object_id_two in (select gdmm.member_id
  from group_distinct_member_map gdmm
  where gdmm.group_id = 465)
and object_id_two != 30919 order by object_id_two"]

set to_delete_ids [concat $to_delete_ids $to_delete_ids2]

# ---------------------------------------------------------------
# Delete users with companies with cost
# ---------------------------------------------------------------

set to_delete_ids2 [db_list old_free "select object_id_two
from acs_rels 
where object_id_one in (select company_id
  from im_companies c, acs_objects o
  where object_id = company_id
  and company_id not in (select provider_id from im_costs where effective_date > '2014-12-31' union select customer_id from im_costs where effective_date > '2014-12-31')
  and company_type_id in (56,58,10000301) and creation_date < '2016-01-01') 
and rel_type = 'im_company_employee_rel'
and object_id_two in (select gdmm.member_id
  from group_distinct_member_map gdmm
  where gdmm.group_id = 465)
and object_id_two != 30919 order by object_id_two"]

set to_delete_ids [concat $to_delete_ids $to_delete_ids2]

foreach user_id $to_delete_ids { 
    ns_log Notice "Deleting User [im_name_from_id $user_id] - $user_id"
    set rels [db_list rels "select rel_id from acs_rels where object_id_two = :user_id"]
    foreach rel_id $rels {
	db_string del_rel "select acs_rel__delete(:rel_id) from dual"
    }
    db_dml del_notes "delete from im_notes where object_id = :user_id"
    db_dml update_tasks "update im_trans_tasks set trans_id = null where trans_id = :user_id"
    db_dml update_tasks "update im_trans_tasks set edit_id = null where edit_id = :user_id"
    db_dml update_tasks "update im_trans_tasks set proof_id = null where proof_id = :user_id"
    db_dml update_folders "update im_fs_folders set object_id = NULL where object_id = :user_id"
    db_dml delete_context "delete from acs_object_context_index where object_id = :user_id"
    db_dml delete_context "delete from acs_object_context_index where ancestor_id = :user_id"
    db_dml delete_notification "delete from im_freelance_notifications where user_id = :user_id"
    db_dml update_lead "update im_projects set project_lead_id=null where project_lead_id = :user_id"
    db_dml update_lead "update im_projects set supervisor_id=null where supervisor_id = :user_id"  
    db_dml delete_hours "delete from im_hours where user_id = :user_id"
    db_dml update_costs "update im_costs set cause_object_id = [im_company_freelance] where cause_object_id = :user_id"
    db_dml update_mody "update acs_objects set modifying_user = NULL where modifying_user = :user_id"
    
    db_dml update_company "update im_companies set primary_contact_id = NULL where primary_contact_id = :user_id"
    db_dml delete_absence "delete from im_user_absences where owner_id = :user_id"
    set portrait_rels [db_list rels "select rel_id from acs_rels where object_id_one = :user_id and rel_type = 'user_portrait_rel'"]
    foreach rel_id $portrait_rels {
	db_string del_rel "select acs_rel__delete(:rel_id) from dual"
    }
    db_dml update_lead "update im_projects set supervisor_id=null where supervisor_id = :user_id"
    
    db_dml update_invoice "update im_invoices set company_contact_id = NULL where company_contact_id = :user_id"
    db_dml update_tasks "update im_trans_tasks set other_id = null where other_id = :user_id"
    db_dml update_survey "update survsimp_responses set related_object_id = null where related_object_id = :user_id"
    db_dml update_survey "update survsimp_responses set related_context_id = null where related_context_id = :user_id"
    db_dml update_context "update acs_objects set context_id = [im_company_freelance] where context_id = :user_id"
    db_dml update_context "update acs_objects set creation_user = NULL where creation_user = :user_id"
    db_dml update_lead "update im_projects set company_contact_id=null where company_contact_id = :user_id"  
    db_dml update_assignee "update im_forum_topics set asignee_id = null where asignee_id = :user_id"
    db_dml update_costs "update im_costs set provider_id = [im_company_freelance] where provider_id = :user_id"
    db_dml update_manager "update im_companies set manager_id = null where manager_id = :user_id"

    foreach content_item_id [db_list item "select item_id from cr_items where parent_id = :user_id"] {
	db_string delete_item "select content_item__del(:content_item_id) from dual"
    }
    
    db_string del_person "select person__delete(:user_id) from dual"
}


ns_log Notice "Deleted users, now starting with companies"

# ---------------------------------------------------------------
# Delete companies without cost
# ---------------------------------------------------------------
set to_delete_company_ids [db_list comp "select company_id
from im_companies c, acs_objects o
where object_id = company_id and creation_date < '2016-01-01'
and company_id not in (select provider_id from im_costs union select customer_id from im_costs)
and company_type_id in (56,58,10000301)
and company_id not in ([im_company_internal],[im_company_freelance])"]

foreach company_id $to_delete_company_ids {
    ns_log Notice "Deleting Company [im_name_from_id $company_id] - $company_id"
    set rels [db_list rels "select rel_id from acs_rels where object_id_one = :company_id"]
    foreach rel_id $rels {
	db_string del_rel "select acs_rel__delete(:rel_id) from dual"
    }
    db_dml del_notes "delete from im_notes where object_id = :company_id"
    
    db_dml delete_context "delete from acs_object_context_index where object_id = :company_id"
    db_dml delete_context "delete from acs_object_context_index where ancestor_id = :company_id"
    db_dml update_folders "update im_fs_folders set object_id = NULL where object_id = :company_id"
    
    db_string delete "select im_company__delete(:company_id) from dual"
}

set to_delete_company_ids [db_list comp "select company_id
from im_companies c, acs_objects o
where object_id = company_id
and company_id not in (select provider_id from im_costs where effective_date > '2014-12-31' union select customer_id from im_costs)
and company_type_id in (56,58,10000301) and creation_date < '2016-01-01'
and company_id not in ([im_company_internal],[im_company_freelance])"]


foreach company_id $to_delete_company_ids {
    ns_log Notice "Deleting Company [im_name_from_id $company_id] - $company_id"
    set company_name [im_name_from_id $company_id]
    set rels [db_list rels "select rel_id from acs_rels where object_id_one = :company_id"]
    foreach rel_id $rels {
	db_string del_rel "select acs_rel__delete(:rel_id) from dual"
    }
    db_dml del_notes "delete from im_notes where object_id = :company_id"
    
    db_dml delete_context "delete from acs_object_context_index where object_id = :company_id"
    db_dml delete_context "delete from acs_object_context_index where ancestor_id = :company_id"
    db_dml update_folders "update im_fs_folders set object_id = NULL where object_id = :company_id"
    
    db_dml update_costs "update im_costs set provider_id = [im_company_freelance], note=:company_name where provider_id=:company_id"

    db_string delete "select im_company__delete(:company_id) from dual"
}

# ---------------------------------------------------------------
# enable triggers
# ---------------------------------------------------------------
db_dml enable_trigger "alter table im_projects enable trigger im_projects_update_tr"
db_dml enable_trigger "alter table im_projects enable trigger im_project_project_cost_center_update_tr"
db_dml enable_trigger "alter table im_projects enable trigger im_projects_calendar_update_tr"
db_dml enable_trigger "alter table im_projects enable trigger im_projects_project_cache_up_tr"
db_dml enable_trigger "alter table im_projects enable trigger im_projects_tsearch_tr"
db_dml enable_trigger "alter table im_trans_tasks enable trigger im_trans_tasks_calendar_update_tr"
db_dml enable_trigger "alter table im_trans_tasks enable trigger im_trans_tasks_calendar_update_tr"
db_dml enable_trigger "alter table im_companies enable trigger im_companies_tsearch_tr"
db_dml disable_trigger "alter table persons enable trigger persons_tsearch_tr"


ad_returnredirect "/intranet"
