# /packages/intranet-trans-trados/trados/download-pdfs.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Purpose: Download all Source Files as PDFS along with all files in the references folder
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2016-06-15
} {
    {project_id ""}
}

# -----------------------------------------------------------
# Defaults
# -----------------------------------------------------------
set user_id [ad_maybe_redirect_for_registration]

    
set perm_p 0
# Check if the user has admin rights on the parent_id
# to allow freelancers to add sub-projects
im_project_permissions $user_id $project_id view read write admin
if {$admin} { set perm_p 1 }
    

# ---------------------------------------------------------------------
# Get some more information about the project
# ---------------------------------------------------------------------

set project_query "
    select
        p.project_nr,
        p.source_language_id,
        company_id,
        final_company_id,
        subject_area_id
    from
        im_projects p
    where
        p.project_id=:project_id
"

if { ![db_0or1row projects_info_query $project_query] } {
    ad_return_complaint 1 "[_ intranet-translation.lt_Cant_find_the_project]"
    return
}

# Create conversion dir if necessary
set pdf_dir "[cog::project::path -project_id $project_id]/PDF"
if {![file exists $pdf_dir]} {
    file mkdir $pdf_dir
}

set source_path "[cog::project::path -project_id $project_id]/Original"
set source_file_list [glob -nocomplain -types {f} -directory "${source_path}" "*"]
set reference_path "[cog::project::path -project_id $project_id]/Projektinfos"
set reference_file_list [glob -nocomplain -types {f} -directory "${reference_path}" "*"]

set file_list [concat $source_file_list $reference_file_list]

foreach source_file $file_list {
    set file_extension [file extension $source_file]
    set file_root [file rootname $source_file]
    set pdf_file "${pdf_dir}/${file_root}.pdf"
    if {[file exists $pdf_file]} {
	# File was converted, compare dates
	if {[file mtime $source_file]>[file mtime $pds_file]} {
	    set update_p 1
	} else {
	    # PDF is newer or the same time, so don't do anything
	    set update_p 0
	}
    } else {
	# No pdf, so update
	set update_p 1
    }

    if {$update_p} {
	switch $file_extension {
	    ".sdlxliff" {
		# Do nothing
	    }	    
	    ".pdf" {
		# Just copy
		file copy -force $source_file $pdf_dir
	    }
	    ".doc" - ".docx" {
		catch {set conversion_result [intranet_oo::convert_to -oo_file $source_file -outdir $pdf_dir]}
	    }
	    default {
		# If the conversion fails, we won't have a file.
		file copy -force $source_file $pdf_dir
	    }
        }
    }
}

if {$file_list ne ""} {
    # Zip the PDF folder and return it.
    set zipfile /tmp/PDF_${project_nr}.zip
    intranet_chilkat::create_zip -directories "$pdf_dir" -zipfile $zipfile
    
    # Return the file
    set outputheaders [ns_conn outputheaders]
    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"PDF_${project_nr}.zip\""
    ns_returnfile 200 application/zip $zipfile
    
    exec rm -rf $zipfile
} else {
    ad_return_error "No Source files" "We could not find any source files to generate PDFs for"
}
