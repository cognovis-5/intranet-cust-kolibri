ad_page_contract {
    @author malte.sussdorff@cognovis.de
} {
    project_id:integer
}

set current_user_id [ad_maybe_redirect_for_registration]

db_1row project_info "select to_char(start_date,'YYYY-MM-DD HH24:MI') as project_start_date, project_name, project_nr, project_status_id,company_id, subject_area_id,source_language_id from im_projects where project_id = :project_id"

set target_language_ids [list]
set task_type_ids [list]

db_foreach task_names {
	select aux_string1, target_language_id
	from im_trans_tasks, im_categories
	where project_id = :project_id
	and category_id = task_type_id
} {
	if {[lsearch $target_language_ids $target_language_id]<0} {lappend target_language_ids $target_language_id}

	# Add the task type ids
	foreach task_type $aux_string1 {
		set task_type_id [db_string type_id "select category_id from im_categories where lower(category) = :task_type and category_type = 'Intranet Trans Task Type'" -default ""]
		if {$task_type_id ne ""} {
			if {[lsearch $task_type_ids $task_type_id]<0} {lappend task_type_ids $task_type_id}
		}
	}
}

set project_dir [cog::project::path -project_id $project_id]

set error_list [list]

foreach target_language_id $target_language_ids {
    foreach task_type_id $task_type_ids {

        set package_file_type_id 606
        set task_type [im_category_from_id -translate_p 0 $task_type_id]

      	# Check if we have the package
      	set package_files [kolibri_trados_packages -project_id $project_id -target_language_id $target_language_id -task_type $task_type] 
      	set freelance_package_id ""

       	foreach package_file $package_files {
	      	set freelance_package_id ""
	
		    set file_path [lindex $package_file 0]
		    set file_task_ids [lindex $package_file 1]

	    	set freelance_package_name "${project_nr}"

	    	# Append the target language and type of task
	    	append freelance_package_name "_[im_category_from_id -translate_p 0 $target_language_id]_[im_category_from_id -translate_p 0 $task_type_id]"
	    	if {$file_task_ids ne ""} {
	    		# Find the correct freelance_package_id by looping through all the tasks
	    		set freelance_package_ids [db_list packages "select distinct fp.freelance_package_id from im_freelance_packages_trans_tasks fptt, im_freelance_packages fp, im_freelance_assignments fa
	    			where trans_task_id in ([template::util::tcl_to_sql_list $file_task_ids])
	    			and fp.freelance_package_id = fptt.freelance_package_id
	    			and fp.package_type_id = $task_type_id
	    			and fa.freelance_package_id = fp.freelance_package_id"]

	    		if {[llength $freelance_package_ids] eq 1} {
	    			set freelance_package_id [lindex $freelance_package_ids 0]
	    		} elseif {[llength $freelance_package_ids] eq 0} {
	    			lappend error_list "Can't import $file_path. Could not find a project-open package for the file. Maybe you should create the assignment first?"
	    			set freelance_package_id ""
	    		} else {
	    			lappend error_list "Can't import $file_path. The files seem to match multiple assignments. Please split the trados package according to the assignments."
	    			set freelance_package_id ""
	    		}
	    	} else {
				set freelance_package_id [db_string exists "select fp.freelance_package_id
					from im_freelance_packages fp, im_freelance_packages_trans_tasks fptt, im_trans_tasks tt
					where fp.freelance_package_id = fptt.freelance_package_id
						and fptt.trans_task_id = tt.task_id
						and fp.package_type_id = :task_type_id
						and fp.project_id = :project_id
						and tt.target_language_id = :target_language_id limit 1" -default ""]
				if {$freelance_package_id eq ""} {
					lappend error_list "Can't import $file_path. I could not find a project-open package for the file. Maybe you should create the assignment first?"
				}
			}
		}


		if {$freelance_package_id ne ""} {

			# ---------------------------------------------------------------
			# Set variables for mailings etc.
			# ---------------------------------------------------------------
			db_1row package_info "select p.project_id, p.project_nr, p.project_lead_id, freelance_package_name, package_type_id from im_freelance_packages fp, im_projects p
			 where freelance_package_id = :freelance_package_id
			 and fp.project_id = p.project_id"

			# Use the freelancer company name in case needed for communication
			set internal_company_name [im_name_from_id [im_company_freelance]]

			set assignee_id ""
			set assignment_status_id ""
			db_0or1row assignee_info "select assignee_id, assignment_id, assignment_status_id from im_freelance_assignments where freelance_package_id = :freelance_package_id and assignment_status_id in (4222,4224,4225,4226,4227,4229,4231)"
		    set location [util_current_location]
		    if {$assignee_id ne ""} {
		     	set salutation_pretty [im_invoice_salutation -person_id $assignee_id]
				set locale [lang::user::locale -user_id $assignee_id]

		    	set token [ns_sha1 someSeCrEt-${assignee_id}-${project_id}]
		        set assignments_link [export_vars -base "${location}/sencha-freelance-translation/project-assignments" -url {token project_id assignee_id}]

				# Assume the user has viewed the assignment, when uploading a file
				# Used for reminders	
			    im_freelance_record_view -viewer_id $current_user_id -object_id $assignment_id
			}


			# Copy the found file over (unless delivered)
			switch $assignment_status_id {
				4225 - 4226 - 4227 - 4231 {
					lappend error_list "Can't import $file_path as the corresponding assignment for [im_name_from_id $assignee_id] has already been delivered"
					continue
				}
				default {
				}
			}

			set freelance_package_file_name "${freelance_package_name}"
			append freelance_package_file_name [file extension $file_path]
			set folder [im_trans_task_folder -project_id $project_id -target_language [im_category_from_id -translate_p 0 $target_language_id] -folder_type $task_type]
			set new_path "${project_dir}/${folder}/$freelance_package_file_name"
			file rename -force $file_path $new_path


		    # Check if we have a file (Trados out) associated with it.
		    set freelance_package_file_id [db_string file_exists "select freelance_package_file_id from im_freelance_package_files where freelance_package_id = :freelance_package_id and package_file_type_id = :package_file_type_id" -default "" ]

		    if {$freelance_package_file_id ne ""} {

		    	# We need to upload a new version of the package and inform the freelancer if the assignment is already started
				db_foreach package_file "select freelance_package_file_id, file_path as old_file_path
					from im_freelance_package_files
					where freelance_package_id = :freelance_package_id and package_file_type_id = :package_file_type_id" {
						
					if {$old_file_path ne $new_path} {
						# delete the file
						file delete -force $old_file_path
						db_dml update_file "update im_freelance_package_files set file_path = :new_path,
								freelance_package_file_name = :freelance_package_file_name
								where freelance_package_file_id = :freelance_package_file_id"
					}
					db_dml update_object "update acs_objects set modifying_user = :current_user_id, last_modified = now() where object_id = :freelance_package_file_id"
				}

				# Inform the freelancer if they already worked on it.
				switch $assignment_status_id {
					4224 {
						# Inform about a new file
			           	set subject "[lang::message::lookup $locale intranet-cust-kolibri.lt_package_updated_message_subject]"
			           	set body "[lang::message::lookup $locale intranet-cust-kolibri.lt_package_updated_message_body]"

			            set signature [db_string signature "select signature from parties where party_id = :project_lead_id" -default ""]
			            if {$signature ne ""} {
			            		append body [template::util::richtext::get_property html_value $signature]
			            }

						acs_mail_lite::send \
							-send_immediately \
							-to_addr [party::email -party_id $assignee_id] \
							-from_addr [party::email -party_id $project_lead_id] \
							-subject $subject \
							-body $body \
							-mime_type "text/html" \
						    -use_sender \
					    -object_id $assignment_id
						
						im_freelance_notify \
							-object_id $assignment_id \
							-recipient_ids $assignee_id
					}
					4222 {
						db_dml update_ass_status "update im_freelance_assignments set assignment_status_id = 4229 where assignment_id = :assignment_id"
					}
					default {
						# Do nothing
					}
				}
			} else {
				set freelance_package_file_id [im_freelance_package_file_create \
							   -freelance_package_id $freelance_package_id \
							   -file_path $new_path \
							   -freelance_package_file_name $freelance_package_file_name \
							   -package_file_type_id $package_file_type_id]

				# ---------------------------------------------------------------
				#  Inform the assignee he can start the work
				# ---------------------------------------------------------------

				if {$assignee_id ne ""} {
					# Set the assignment status to work ready (4229)
					db_dml update_ass_status "update im_freelance_assignments set assignment_status_id = 4229 where assignment_id = :assignment_id"
					
		           # Getting and setting deadline
		           set assignment_deadline [db_string assignment_deadline "select end_date from im_freelance_assignments where assignment_id =:assignment_id" -default ""]
		           set assignment_deadline_formatted [lc_time_fmt $assignment_deadline "%q %X"]

                   set package_url [export_vars -base "${location}/sencha-freelance-translation/package-download" -url {freelance_package_file_id}]
		           
		           set subject "[lang::message::lookup $locale intranet-cust-kolibri.lt_package_ready_message_subject]"
		           set body "[lang::message::lookup $locale intranet-cust-kolibri.lt_package_ready_message_body]"

		            set signature [db_string signature "select signature from parties where party_id = :project_lead_id" -default ""]
		            if {$signature ne ""} {
		            		append body [template::util::richtext::get_property html_value $signature]
		            }


					acs_mail_lite::send \
						-send_immediately \
						-to_addr [party::email -party_id $assignee_id] \
						-from_addr [party::email -party_id $project_lead_id] \
						-subject $subject \
						-body $body \
						-mime_type "text/html" \
					    -use_sender \
					    -object_id $assignment_id
						
					im_freelance_notify \
						-object_id $assignment_id \
						-recipient_ids $assignee_id
				}
			}
	    }
	}
}

if {[llength $error_list] eq 0} {
	ad_returnredirect "[export_vars -base "/sencha-assignment/project-manager-app" -url {project_id}]"
} else {
	ad_return_error "Problems with importing packages" "<ul><li>[join $error_list "</li><li>"]</li></ul>"
}



