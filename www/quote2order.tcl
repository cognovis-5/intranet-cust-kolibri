#
#
# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Turns a quote into an order for a translation project.
    
    Primarily designed for a customer to accept the project
    
    @author <yourname> (<your email>)
    @creation-date 2012-03-11
    @cvs-id $Id$
} {
    {project_id ""}
    {invoice_id ""}
} -properties {
} -validate {
} -errors {
}

# -----------------------------------------------------------
# Defaults
# -----------------------------------------------------------
set user_id [im_require_login -no_redirect_p 1]

set complaint ""

if {$project_id eq ""} {
    # Get the project_id from the invoice
    set project_id [db_string project "select project_id from im_costs where cost_id = :invoice_id" -default ""]
}

if {$project_id eq ""} {
	set complaint "Can't find project"
}

# Get all information about the quote and customer and project

# Check if the user has admin rights on the parent_id
# to allow freelancers to add sub-projects
set perm_p 0
im_project_permissions $user_id $project_id view read write admin
if {$admin} { set perm_p 1 }
	
# Users with "add_projects" privilege can always create new projects...
if {[im_permission $user_id add_projects]} { set perm_p 1 }

# Quote recipient can also accept
set company_contact_id [db_string quote "select company_contact_id from im_invoices i, im_costs c, acs_objects o where cost_type_id = [im_cost_type_quote] and project_id = :project_id and i.invoice_id = c.cost_id order by o.last_modified desc limit 1" -default ""]

if {$company_contact_id eq $user_id} { set perm_p 1}

if {!$perm_p} {
	set complaint "Insufficient Privileges <li>You don't have sufficient privileges to see this page."
}

set project_lead_id [db_string lead "select project_lead_id
     from im_projects p  where p.project_id = :project_id" -default ""]

set pm_name [im_name_from_id $project_lead_id]

if {$complaint eq ""} {

    if {[catch {cog_rest::put::invoice_reply -invoice_id $invoice_id -cost_status_id [im_cost_status_accepted] -rest_user_id $user_id} error_message]} {
        set complaint $error_message
    } else {
        # Record the acceptance
        db_dml update_acceptance "update im_projects set acceptance_date = now(), acceptance_user_id = :user_id where project_id = :project_id"
        ns_returnredirect "https://www.kolibri.online/danke/"
        ad_script_abort
    }
}