<master>
<property name="context">@context;noquote@</property>
<property name="title">@page_title@</property>
<property name="admin_navbar_label">admin_templates</property>

<table cellpadding=1 cellspacing=1 border=0 width="100%">
<tr>
  <td colspan=2>
	<%= [im_component_bay top] %>
  </td>
</tr>
<tr>
  <td valign=top width="300px">
	<%= [im_box_header $page_title] %>
	<listtemplate name="templates"></listtemplate>
	<%= [im_box_footer] %>

	<p>&nbsp;</p>
	<%= [im_component_bay left] %>
  </td>
  <td valign="top">
	<%= [im_box_header [lang::message::lookup "" intranet-core.Template_Help "Help"]] %>
	#intranet-cust-kolibri.lt_In_this_screen_you_ca#<br>
	#intranet-cust-kolibri.lt_These_templates_are_a#<br>
	#intranet-cust-kolibri.lt_To_activate_templates# <a href="/intranet/admin/categories/index.tcl?select_category_type=Intranet Cost Template">#intranet-cust-kolibri.create_a_category#</a> #intranet-cust-kolibri.lt_for_themYou_have_the_#<br>
	#intranet-cust-kolibri.lt_To_download_a_templat#<br>
	#intranet-cust-kolibri.lt_For_details_on_templa#
	<a href="http://www.project-open.org/en/category_intranet_cost_template">#intranet-cust-kolibri.online_documentation#</a>.
	<%= [im_box_footer] %>

	<%= [im_component_bay right] %>
  </td>
</tr>
<tr>
  <td colspan=2>
	<%= [im_component_bay bottom] %>
  </td>
</tr>
</table>





