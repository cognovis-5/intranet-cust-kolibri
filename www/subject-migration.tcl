ad_page_contract {
    page to migrate categories
    
    @author malte.sussdorff@cognovis.de
    @creation-date 2015-11-12
    @cvs-id $Id$
} {
}

set user_id [ad_conn user_id]
set form_id "file-add"
ad_form -name $form_id -html { enctype multipart/form-data } -export { company_id project_id } -form {
    file_id:key
    {upload_file:file {label \#file-storage.Upload_a_file\#} {html "size 30"}}
}   
  

ad_form -extend -name $form_id -new_data {
    
	set tmp_filename [template::util::file::get_property tmp_filename $upload_file]
	
	set csv_files_content [fileutil::cat $tmp_filename]
	set csv_files [split $csv_files_content "\n"]
	set csv_files_len [llength $csv_files]

	set separator [im_csv_guess_separator $csv_files]

# Split the header into its fields
	set csv_header [string trim [lindex $csv_files 0]]
	set csv_header_fields [im_csv_split $csv_header $separator]
	set csv_header_len [llength $csv_header_fields]
	foreach line [im_csv_get_values $csv_files_content $separator] {
	    set old_value [lindex $line 2]
	    set new_value [lindex $line 1]
	    if {$old_value ne ""} {
		db_dml update_projects "update im_projects set subject_area_id = :new_value where subject_area_id = :old_value"
		db_foreach user_skill {select user_id, skill_id from im_freelance_skills where skill_id = :old_value} {
		    set exists_p [db_string exists "select 1 from im_freelance_skills where skill_id = :new_value and user_id = :user_id" -default 0]
		    if {$exists_p} {db_dml delete "delete from im_freelance_skills where user_id = :user_id and skill_id = :skill_id"}
		}
		db_dml update_skills "update im_freelance_skills set skill_id = :new_value where skill_id = :old_value"
		db_dml update_o_skill_map "update im_object_freelance_skill_map set skill_id = :new_value where skill_id = :old_value"
		db_dml update_child "update im_category_hierarchy set parent_id = :new_value where parent_id = :old_value"
		if {[catch {db_dml update_child "update im_category_hierarchy set child_id = :new_value where child_id = :old_value"}]} {
		    db_dml delete_child "delete from im_category_hierarchy where child_id = :old_value"
		}
		db_dml update_price "update im_trans_prices set subject_area_id = :new_value where subject_area_id = :old_value"
		ds_comment "[im_category_from_id $old_value] ($old_value) => [im_category_from_id $new_value] ($new_value)"
	    }
	    db_dml delete_category "delete from im_categories where category_id = :old_value and enabled_p = 'f'"
	}

} -after_submit {

    db_foreach freelance_skills "select user_id, skill_id, skill_type_id from im_freelance_skills where skill_id in (select category_id from im_categories where category_type = 'Intranet Skill Business Sector')" {
	catch {db_dml update "update im_freelance_skills set skill_type_id = 2024 where user_id = :user_id and skill_id = :skill_id and skill_type_id = :skill_type_id"}
    }

    db_foreach object_map "select object_id, skill_id, skill_type_id from im_object_freelance_skill_map where skill_id in (select category_id from im_categories where category_type = 'Intranet Skill Business Sector')" {
	catch {db_dml update "update im_object_freelance_skill_map set skill_type_id = 2024 where object_id = :object_id and skill_id = :skill_id and skill_type_id = :skill_type_id"}
    }
asdasdasd

    if {[exists_and_not_null return_url]} {
		ad_returnredirect $return_url
    } else {
		ad_returnredirect "./?company_id=$company_id"
    }
    ad_script_abort

}

ad_return_template
