#
#
# Copyright (c) 2022, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Migrate a T folder to OneDrive
    
} {
    {project_id ""}
} -properties {
} -validate {
} -errors {
}

if {$project_id eq ""} {
    ad_returnredirect "/intranet"
} else {

    kolibri::migrate::onedrive_project -project_id $project_id
    cog_log Warning "Migrated [im_name_from_id $project_id]"
    ad_returnredirect [export_vars -base "/intranet/projects/view" -url {project_id}]
}
