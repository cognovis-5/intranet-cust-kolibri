ad_page_contract {
    page to migrate categories
    
    @author malte.sussdorff@cognovis.de
    @creation-date 2015-11-12
    @cvs-id $Id$
} {
}

set user_id [ad_conn user_id]
set company_ids [list]

db_foreach prices {select distinct company_id, source_language_id,target_language_id from im_trans_prices} {
	if {[info exists company_lang_arr($company_id)]} {
		lappend company_lang_arr($company_id) "${source_language_id}-$target_language_id"
	} else {
		set company_lang_arr($company_id) "${source_language_id}-$target_language_id"
	}
}

set company_ids_with_price [db_list companyies "select distinct company_id from im_trans_prices"]
ds_comment "$company_ids_with_price"

db_multirow -extend {prices project_ids company_url} companies companies "select company_name, company_id 
	from im_companies 
	where company_status_id = [im_company_status_active] 
	and company_type_id in ([template::util::tcl_to_sql_list [im_sub_categories [im_company_type_customer]]])
	and company_path <> 'internal'" {

    set company_url [export_vars -base "/intranet/companies/view" -url {{company_id $company_id}}]
    set project_ids [db_list projects "select project_id from im_projects where company_id = :company_id and (start_date >= to_date('2017-01-01','YYYY-MM-DD') or end_date >= to_date('2017-01-01','YYYY-MM-DD'))"]    
    set prices ""

	if {$project_ids ne ""} {    
	    # Get source and target language_ids
	    set source_language_ids [db_list source "select distinct source_language_id from im_projects where project_id in ([template::util::tcl_to_sql_list $project_ids]) and source_language_id is not null"]
	    set target_language_ids [db_list target "select distinct language_id from im_target_languages where project_id in ([template::util::tcl_to_sql_list $project_ids])"]
	    if {[lsearch $company_ids_with_price $company_id]<0} {
		    if {$source_language_ids ne "" && $target_language_ids ne ""} {
		    	foreach source_language_id $source_language_ids {
		    		foreach target_language_id $target_language_ids {
		    			# Copy the price from the main pricelist
						db_dml insert "insert into im_trans_prices (price_id,uom_id,company_id,task_type_id,target_language_id,source_language_id,subject_area_id,valid_from,valid_through,currency,price,note,file_type_id,min_price) 
select nextval('im_trans_prices_seq'),uom_id,$company_id,task_type_id,target_language_id,source_language_id,subject_area_id,valid_from,valid_through,currency,price,note,file_type_id,min_price 
from im_trans_prices where
company_id = 8720 and source_language_id = $source_language_id and target_language_id = $target_language_id"
				   		append prices "$source_language_id => $target_language_id<br/>"			
		    		}
		    	}
			}
		} else {
			set prices "Price exissts"
		}
	} else {
		set prices "No Projects"
	}
}

template::multirow sort companies prices
ad_return_template
