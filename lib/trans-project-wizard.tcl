# /packages/intranet-trans-project-wizard/www/trans-project-wizard.tcl
#
# Copyright (c) 2003-2007 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

if {![info exists project_id]} {
    ad_page_contract {
	Show the status and a description of a number of steps to
	execute a translation project
	@author frank.bergmann@project-open.com
    } {
	project_id
    }
}

# ---------------------------------------------------------------------
# Permissions & Defaults
# ---------------------------------------------------------------------
set kolibri_package_url [apm_package_url_from_key "intranet-cust-kolibri"]

if {![info exists project_id]} {
    ad_return_complaint 1 "Trans-Project-Wizard: No project_id specified"
}

if {![info exists user_id]} {
    set user_id [ad_maybe_redirect_for_registration]
}

im_project_permissions $user_id $project_id view read write admin

if {!$read} {
    ad_return_complaint 1 "<li>[_ intranet-core.lt_You_have_insufficient_6]"
    ad_script_abort
}

# set return_url [export_vars -base "/sencha-assignment/project-manager-app" {project_id}]

set return_url [export_vars -base "https://pm.kolibri.online/" {project_id}]

set bgcolor(0) " class=roweven"
set bgcolor(1) " class=rowodd"

set table_width 500


set project_url "[ad_url]/intranet/projects/view"
set help_gif_url "[ad_url]/intranet/images/help.gif"
set progress_url "[ad_url]/intranet/images/progress_greygreen"

set status_display(0) "<img src=$progress_url.0.gif>"
set status_display(1) "<img src=$progress_url.1.gif>"
set status_display(2) "<img src=$progress_url.2.gif>"
set status_display(3) "<img src=$progress_url.3.gif>"
set status_display(4) "<img src=$progress_url.4.gif>"
set status_display(5) "<img src=$progress_url.5.gif>"
set status_display(6) "<img src=$progress_url.6.gif>"
set status_display(7) "<img src=$progress_url.7.gif>"
set status_display(8) "<img src=$progress_url.8.gif>"
set status_display(9) "<img src=$progress_url.9.gif>"
set status_display(10) "<img src=$progress_url.10.gif>"

set freelance_invoices_installed_p [util_memoize [list db_string freelance_inv_exists "select count(*) from apm_packages where package_key = 'intranet-freelance-invoices'"]]

set project_closed_p [db_string project_open_p "
	select	1
	from	im_projects
	where	project_id = :project_id
	and project_status_id in ([template::util::tcl_to_sql_list [im_sub_categories [im_project_status_closed]]])
" -default 0]

# ---------------------------------------------------------------------
# Setup Multirow to store data
# ---------------------------------------------------------------------

set multi_row_count 0
multirow create execution status value url name description class

# ---------------------------------------------------------------------
# Project Base Data
# ---------------------------------------------------------------------

# Show status "done" = 10, as the base data must have been entered to get
# to this page...

db_1row project_info "select
        p.project_nr,
        p.project_path,
        p.project_name,
        c.company_path,
        p.company_id,
        p.project_type_id
from
        im_projects p,
        im_companies c
where
        p.project_id=:project_id
        and p.company_id=c.company_id"


# Only do this for translation projects
set translation_type_ids [im_sub_categories [im_project_type_translation]]
if {[lsearch $translation_type_ids $project_type_id]>-1} {
	set workflow_steps [db_string workflow_steps "select aux_string1 from im_categories where category_id = :project_type_id" -default ""]
	if {[lsearch $workflow_steps "trans"] >-1} { 
    	# Check if we have a trados folder setup
    	set project_dir [cog::project::path -project_id $project_id]
    	if {![file exists ${project_dir}/Trados]} {
			# Seems it is not setup. Create it now
			catch {im_trans_trados_setup_directory -project_id $project_id}
    	}
	}
}


# Check if we have the new project path already there.
set query "
                        select
                                p.project_type_id,
                                p.project_nr,
                                p.company_id,
                p.final_company_id
                        from
                                im_projects p
                        where
                                p.project_id = :project_id
                        "

if { ![db_0or1row projects_info_query $query] } {
    return "Can't find the project with group id of $project_id"
}

if {$final_company_id eq ""} {
    set customer_project_path "[kolibri::customer_path -company_id $company_id]/Projekte"
} else {
    set customer_project_path "[kolibri::final_customer_path -final_company_id $final_company_id -company_id $company_id]/Projekte"
}

set project_dir "${customer_project_path}/$project_nr"
if {![file exists $project_dir]} {
    multirow append execution \
	"" "OneDrive" "[export_vars -base "/kolibri/migrate-t-to-onedrive" -url {project_id}]" \
	"MIGRATE FOLDER (from T: to OneDrive)" \
       	"MIGRATE FOLDER (from T: to OneDrive)" \
	$bgcolor([expr $multi_row_count % 2])
    
    incr multi_row_count
}    
    
# ---------------------------------------------------------------------
# Translation Tasks
# ---------------------------------------------------------------------

if {[im_trans_trados_project_p -project_id $project_id]} {

    multirow append execution \
	    "" "" "file:///T:/Projekte/${company_path}/${project_nr}/Trados/${project_nr}.sdlproj" \
	    [lang::message::lookup "" intranet-trans-project-wizard.Open_Trados_Project "Open Trados Projekt"] \
	    [lang::message::lookup "" intranet-trans-project-wizard.Open_Trados_Project_help "Open this project in Trados."] \
	    $bgcolor([expr $multi_row_count % 2])

    incr multi_row_count
		

	set trans_tasks [db_string trans_tasks_status "
	        select  count(*)
	        from    im_trans_tasks
	        where   project_id = :project_id
	"]
 
	if {$trans_tasks > 0} {
		set trans_tasks_status 10
		set trados_task_msg [lang::message::lookup "" intranet-cust-kolibri.Update_from_trados_folder "Update tasks from Trados analyisis"]
	} else {
		set trans_tasks_status 0
		set trados_task_msg [lang::message::lookup "" intranet-cust-kolibri.Create_from_trados_folder "Create tasks from Trados analysis"]
	}

    multirow append execution \
		$status_display($trans_tasks_status) \
		"$trans_tasks [lang::message::lookup "" intranet-trans-project-wizard.Trans_Tasks "Task(s)"]" \
		[export_vars -base "/intranet-trans-trados/update-tasks" {project_id return_url {quote_p "1"}}] \
		$trados_task_msg \
		[lang::message::lookup "" intranet-trans-project-wizard.Trans_Update_Tasks_desc "Update the Tasks with the latest analysis and recreate the quote"] \
		$bgcolor([expr $multi_row_count % 2])

    incr multi_row_count
    
    if {$trans_tasks > 0} {
	# Offer to delete existing tasks
	multirow append execution \
		$status_display($trans_tasks_status) \
		"$trans_tasks [lang::message::lookup "" intranet-trans-project-wizard.Trans_Tasks "Task(s)"]" \
	    [export_vars -base "/intranet-trans-trados/update-tasks" {project_id return_url {quote_p "1"} {delete_not_included_p 1}}] \
		[lang::message::lookup "" intranet-cust-kolibri.ReCreate_from_trados_folder "Use Trados analysis to recreate tasks"] \
		[lang::message::lookup "" intranet-trans-project-wizard.Trans_Update_Delete_Tasks_desc "Update the Tasks with the latest analysis and <b>delete</b> any task not in the analysis"] \
		$bgcolor([expr $multi_row_count % 2])

	incr multi_row_count
    }


}

	
# ---------------------------------------------------------------
# Do we need to create packages from trados
# ---------------------------------------------------------------
set package_count 0
set target_language_ids [im_target_language_ids $project_id]

foreach target_language_id $target_language_ids {
	foreach package_file [kolibri_trados_packages -project_id $project_id -target_language_id $target_language_id -task_type "trans"] {
		incr package_count
	}
	foreach package_file [kolibri_trados_packages -project_id $project_id -target_language_id $target_language_id -task_type "edit"] {
		incr package_count
	}
}

if {$package_count > 0} {
	
    # Count packages for trans
	set package_created_count [db_string package_created "select count(freelance_package_id)
		from im_freelance_packages
		where project_id = :project_id
                and package_type_id = 4210"]
	set package_status [expr 10 * $package_created_count / [llength $target_language_ids]]
	if {$package_status > 10} {set package_status 10}
	
	multirow append execution \
		$status_display($package_status) \
		"$package_count [lang::message::lookup "" sencha-freelance-translation.Package_file "Package File(s)"]" \
		[export_vars -base "$kolibri_package_url/packages-create" {project_id}] \
		[lang::message::lookup "" intranet-cust-kolibri.import_freelancer_pack "Import freelancer packages from Trados"] \
		[lang::message::lookup "" intranet-cust-kolibri.import_freelancer_pack_desc "
		Import and move the currently existing packages in the Korrektur or Übersetzung folder to the correct OUT location so freelancers can download them.."] \
		$bgcolor([expr $multi_row_count % 2])

	incr multi_row_count

} else {
	set missing_packages [db_string assignments "select count(*) from im_freelance_assignments fa, im_freelance_packages fp where fa.freelance_package_id = fp.freelance_package_id and fa.assignment_status_id = 4222 and project_id = :project_id" -default 0]
	
	if {$missing_packages >0} {
	
	    multirow append execution \
		$status_display(0) \
		"$missing_packages Assignments" \
		[export_vars -base "$kolibri_package_url/packages-create" {project_id}] \
		"$missing_packages Assignments warten auf [lang::message::lookup "" sencha-freelance-translation.Package_file "Package File(s)"]" \
		"" \
		$bgcolor([expr $multi_row_count % 2])

	    incr multi_row_count
	}
}

set project_open_p [db_string project_open_p "
	select	1
	from	im_projects
	where	project_id = :project_id
	and project_status_id in ([template::util::tcl_to_sql_list [im_sub_categories [im_project_status_open]]])
" -default 0]

set quote_id [db_string quote "select cost_id from im_costs c, acs_objects o where cost_type_id = [im_cost_type_quote] and project_id = :project_id order by o.last_modified desc limit 1" -default ""]

if {!$project_open_p && $quote_id ne ""} {

	incr multi_row_count
	multirow append execution \
		"" \
		"" \
		[export_vars -base "$kolibri_package_url/quote2order" {project_id return_url}] \
		[lang::message::lookup "" intranet-trans-project-wizard.Open_Project "Set the project to OPEN."] \
		[lang::message::lookup "" intranet-trans-project-wizard.Open_Project_descr "After the customer accepted the latest quote, set the project to status open. If a new TB / TM is available	you will be able to download the updated files."] \
		$bgcolor([expr $multi_row_count % 2])
}

set hours1 [db_string hours1 "
        select  sum(h.hours)
        from    im_hours h
        where   h.project_id = :project_id and
                h.user_id = :user_id
"]

if {"" == $hours1} { set hours1 0 }
if {$hours1 > 0} { set hours1_status 10} else { set hours1_status 0}

set hours1_url "/intranet-timesheet2/hours/new"

multirow append execution \
    $status_display($hours1_status) \
    "$hours1 [lang::message::lookup "" intranet-trans-project-wizard.Hours "Hours(s)"]" \
    [export_vars -base $hours1_url {project_id return_url}] \
    [lang::message::lookup "" intranet-trans-project-wizard.Hours_exec_name "Log Your Hours"] \
    [lang::message::lookup "" intranet-trans-project-wizard.Hours_exec_descr "Please log your hours during executions"] \
    $bgcolor([expr $multi_row_count % 2])
incr multi_row_count



# ---------------------------------------------------------------
# Close the project (if hours are logged)
# ---------------------------------------------------------------

if {!$project_closed_p} {

	
	multirow append execution \
		"" \
		"" \
		[export_vars -base "$kolibri_package_url/close-project" {project_id return_url}] \
		[lang::message::lookup "" intranet-cust-kolibri.Close_Project "Close Project"] \
		[lang::message::lookup "" intranet-cust-kolibri.Close_Project_descr "Close the project and run all final tasks."] \
		$bgcolor([expr $multi_row_count % 2])
	incr multi_row_count
}
