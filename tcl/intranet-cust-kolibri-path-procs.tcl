ad_proc -public is_trados_file {
    name
} {
    Returns if the filename is a trados file

    @param name Name of the file
} {
    return [string match *.sdl* $name]
}


namespace eval kolibri {

	ad_proc -public customer_path {
		-company_id:required
	} {
		Returns the unix path location for a customer. Returns nothing if not a customer
	} {
        return [util_memoize [list kolibri::customer_path_helper -company_id $company_id] 600]
	}

	ad_proc customer_path_helper {
		-company_id:required
	} {
		Returns the customer_path (and creates it if necessary)
		@param company_id ID of the company for which we want the path
	} {
		set base_path_unix "[acs_root_dir][parameter::get_from_package_key -package_key "intranet-cust-kolibri" -parameter "CustomerFolder"]"
		if {[regexp {.\/$} $base_path_unix]} {
			ad_return_complaint 1 "<br><blockquote>
				The '$base_path_unix' path for this filestorage contains a trailing slash ('/') at the end.
				Please notify your system administrator and ask him or her to remove any trailing
				slashes in the Admin -&gt; Parameters -&gt; 'intranet-filestorage' section.
			</blockquote><br>
			"
			return
		}

		set customer_type_ids [im_sub_categories [im_company_type_customer]]
		if {[db_0or1row customer_info "select company_path,company_type_id from im_companies 
			where company_id=:company_id and company_type_id in ([ns_dbquotelist $customer_type_ids])"]
		} {
	    	set customer_path "$base_path_unix/$company_path"
			if {![file exists $customer_path]} {

				set success [im_filestorage_mkdir -dir $customer_path]
				if {$success ne 1} {
					cog_log Error $success
					return ""
				}

				set success [im_filestorage_mkdir -dir "${customer_path}/Allgemein"]
				if {$success ne 1} {
					cog_log Error $success
					return ""
				} else {
					set success [im_filestorage_mkdir -dir "${customer_path}/Allgemein/Vertragliches & Preise"]
					set success [im_filestorage_mkdir -dir "${customer_path}/Allgemein/Corporate Infos"]
				}
			}
    		return $customer_path			
    	} else {
			return ""
		}
	}

	ad_proc -public final_customer_path {
		-company_id:required
		-final_company_id:required
	} {
		Returns the unix path location for a final customer. Returns nothing if not a customer & final company
	} {
        return [util_memoize [list kolibri::final_customer_path_helper -company_id $company_id -final_company_id $final_company_id] 600]
	}

	ad_proc final_customer_path_helper {
		-company_id:required
		-final_company_id:required
	} {
		Returns the final customer_path (and creates it if necessary)
		@param company_id ID of the company for which we want the path
		@param final_company_id ID of the final company
	} {
		set final_companies_path "[kolibri::customer_path -company_id $company_id]/Endkunden"

	    if {![file exists $final_companies_path]} {
			set success [im_filestorage_mkdir -dir $final_companies_path]
			if {$success ne 1} {
				cog_log Error $success
				return ""
			}
		}

		if {[db_0or1row customer_info "select company_path from im_companies 
			where company_id=:final_company_id and company_type_id = 10247"]
		} {

	    	set final_company_path "$final_companies_path/$company_path"

		if {![file exists $final_company_path]} {

				set success [im_filestorage_mkdir -dir $final_company_path]
				if {$success ne 1} {
					cog_log Error $success
					return ""
				}

				set success [im_filestorage_mkdir -dir "${final_company_path}/Allgemein"]
				if {$success ne 1} {
					cog_log Error $success
					return ""
				} else {
					set success [im_filestorage_mkdir -dir "${final_company_path}/Allgemein/Corporate Infos"]
				}
			}
    		return $final_company_path
    	} else {
			return ""
		}
	}

	ad_proc -public project_path {
		-project_id:required
	} {
		Returns the unix path location for a project.

		@param project_id Project for which we want to return the path
	} {
		return [util_memoize [list kolibri::project_path_helper -project_id $project_id] 600]
	}

	ad_proc project_path_helper {
		-project_id:required
	} {
		Returns and creates if it does not exist the unix path location for a project.
	} {
		# Localize the workflow stage directories
		set query "
			select
				p.project_type_id,
				p.project_nr,
				p.company_id,
				p.final_company_id
			from
				im_projects p
			where
				p.project_id = :project_id
			"
	
		if { ![db_0or1row projects_info_query $query] } {
			return "Can't find the project with id of $project_id"
		}

		if {$final_company_id eq ""} {
			set customer_project_path "[kolibri::customer_path -company_id $company_id]/Projekte"
		} else {
			set customer_project_path "[kolibri::final_customer_path -final_company_id $final_company_id -company_id $company_id]/Projekte"
		}

		if {![file exists $customer_project_path]} {
			set success [im_filestorage_mkdir -dir $customer_project_path]
			if {$success ne 1} {
				cog_log Error $success
				return ""
			}
		}

		set project_dir "${customer_project_path}/$project_nr"
		if {![file exists $project_dir]} {
			set success [im_filestorage_mkdir -dir $project_dir]
			if {$success ne 1} {
				cog_log Error $success
				return ""
			}
		}

		set common_directories [list "Angebote" "Originaldateien" "Projektinformationen" "Umsetzung"]
		foreach common_dir $common_directories {
			im_filestorage_mkdir -dir ${project_dir}/$common_dir
		}

		set umsetzung_directories [list]

		switch $project_type_id {
			10000391 - 10000392 {
				# Content
				set umsetzung_directories [list "v0 - Assignment-Dateien für Freelancer" \
					"v1 - Erstfassung des Textes" \
					"v2 - Korrektur durch 2. Redakteur" \
					"v3 - Erstlieferung an den Kunden" \
					"v4 - Feedback vom Kunden" \
					"v5 - Finalisierte Dateien"]
			}
			10000813 {
				# Trans Creation
				set umsetzung_directories [list "v0 - Assignment-Dateien für Freelancer" \
					"v2 - Korrektur durch 2. Redakteur" \
							       "v4 - Feedback vom Kunden" \
					"v5 - Finalisierte Dateien"]							       
			}
			default {
				set umsetzung_directories [list "Finale Dateien"]
			}
		}

		im_filestorage_mkdir -dir "${project_dir}/Umsetzung"
		set target_languages [im_target_languages $project_id]
		if {[llength $target_languages]>1} {
			# Create
			foreach target_language $target_languages {
				im_filestorage_mkdir -dir "${project_dir}/Umsetzung/$target_language"
				foreach umsetzung_dir $umsetzung_directories {
					im_filestorage_mkdir -dir "${project_dir}/Umsetzung/$target_language/$umsetzung_dir"
				}
			}
		} else {
			foreach umsetzung_dir $umsetzung_directories {
				im_filestorage_mkdir -dir "${project_dir}/Umsetzung/$umsetzung_dir"
			}
		}

		return $project_dir
	}

	ad_proc -public terminologie_path {
		-company_id:required
		{-final_company_id ""}
	} {
		Returns the Terminologiemanagement folder
	} {
		return [util_memoize [list kolibri::terminologie_path_helper -company_id $company_id -final_company_id $final_company_id] 600]
	}

	ad_proc terminologie_path_helper {
		-company_id:required
		{-final_company_id ""}
	} {
		Creates the terminologie_folder for a customer company

		@return Path to the Terminologiemanagement, empty if not created 
	} {
	    if {$final_company_id ne ""} {
		set customer_path [kolibri::final_customer_path -company_id $company_id -final_company_id $final_company_id]
	    } else {
		set customer_path [kolibri::customer_path -company_id $company_id]
	    }
		if {$customer_path ne ""} {
		    set terminologie_path  "${customer_path}/Terminologiemanagement"
		    set success [im_filestorage_mkdir -dir $terminologie_path]
			if {$success} {
				set success [im_filestorage_mkdir -dir "${terminologie_path}/TB"]
				set success [im_filestorage_mkdir -dir "${terminologie_path}/TM"]
			} else {
				set terminologie_path ""
			}
		} else {
			set terminologie_path ""
		}
		
		return $terminologie_path
	}


    ad_proc -public create_t_folders {
        -project_id:required
    } {
        Create the folder structure on the T: drive for kolibri projects
        
        @param project_id ID of the project for whhich we create the projects

    } {

        # Localize the workflow stage directories
        set query "
        select
            p.project_type_id,
            p.project_path,
            p.company_id,
            im_category_from_id(p.source_language_id) as source_language,
            c.company_path
        from
            im_projects p,
            im_companies c
        where
            p.project_id = :project_id
            and p.company_id = c.company_id
        "
        if { ![db_0or1row projects_info_query $query] } {
            return "Can't find the project with group id of $project_id"
        }
        
        # Make sure the directories exists:
        #	- Client directy
        #	- Project directory
        
        
        set base_path_unix [parameter::get -package_id [im_package_filestorage_id] -parameter "ProjectBasePathUnix" -default "/tmp/projects"]
        
        # Check if the base_path has a trailing "/" and produce an error:
        if {[regexp {.\/$} $base_path_unix]} {
        ad_return_complaint 1 "<br><blockquote>
                    The '$base_path_unix' path for this filestorage contains a trailing slash ('/') at the end.
                    Please notify your system administrator and ask him or her to remove any trailing
                    slashes in the Admin -&gt; Parameters -&gt; 'intranet-filestorage' section.
                </blockquote><br>
            "
            return
        }
        
        # Create a company directory if it doesn't already exist
        set company_dir "$base_path_unix/$company_path"
        cog_log Debug "im_filestorage_create_directories: company_dir=$company_dir"
        if { [catch {
            if {![file exists $company_dir]} {
                cog_log Debug "exec /bin/mkdir -p $company_dir"
                exec /bin/mkdir -p $company_dir
                cog_log Debug "exec /bin/chmod ug+w $company_dir"
                exec /bin/chmod ug+w $company_dir
            }
        } err_msg] } { return $err_msg }
        
        # Create the project dir if it doesn't already exist
        set project_dir [kolibri::project_path -project_id $project_id]
        cog_log Debug "im_filestorage_create_directories: project_dir=$project_dir"
        if { [catch {
            if {![file exists $project_dir]} {
                cog_log Debug "exec /bin/mkdir -p $project_dir"
                exec /bin/mkdir -p $project_dir
                cog_log Debug "exec /bin/chmod ug+w $project_dir"
                exec /bin/chmod ug+w $project_dir
            }
        } err_msg]} { return $err_msg }
        
        
        # Create the various subdirectories directory
        set content_type_ids [list 10000391 10000392]

        # Base directories, used in e.g. Trans Only (projet_type_id: 93)
        set kolibri_directories [list "Angebot" "Final" "Projektinfos intern"]
        foreach kolibri_dir $kolibri_directories {
            im_filestorage_mkdir -dir ${project_dir}/$kolibri_dir
        }

        set target_languages [im_target_languages $project_id]
        

        switch $project_type_id {
            10000391 - 10000392 {
                set umsetzung_directories [list "v0 Assignment-Dateien für Freelancer" \
                    "v1 Erstfassung des Textes" \
                    "v2 Korrektur durch 2. Redakteur" \
                    "v3 Erstlieferung an den Kunden" \
                    "v4 Feedback vom Kunden" \
                    "v5 Finalisierte Dateien"]

                im_filestorage_mkdir -dir "${project_dir}/$kolibri_dir/Umsetzung"
                if {[llength $target_languages]>1} {
                    # Create
                    foreach target_language $target_languages {
                        im_filestorage_mkdir -dir "${project_dir}/$kolibri_dir/Umsetzung/$target_language"
                        foreach umsetzung_dir $umsetzung_directories {
                            im_filestorage_mkdir -dir "${project_dir}/Umsetzung/$target_language/$umsetzung_dir"
                        }
                    }
                } else {
                    foreach umsetzung_dir $umsetzung_directories {
                        im_filestorage_mkdir -dir "${project_dir}/Umsetzung/$umsetzung_dir"
                    }
                }
            }
        }
    }

	ad_proc -public local_path {
		-project_id
		-type 
	} {
		Returns the windows path for the project

		@param project_id ID of the project we need the path for
		@param type Type of path we look for. Currently supported project, sdlproj

		@return URI for the file on the local machine
	} {
	    # set drive [parameter::get_from_package_key -package_key "intranet-cust-kolibri" -parameter "LocalDrive"]
		set email [party::email -party_id [auth::get_user_id]]
		regsub -all {\.} [lindex [split $email @] 0] {} username
#		set drive "C:/Users/${username}/Kolibri%20Online%20GmbH/Share%20-%20Firmen"
		# 		set drive "%OneDrive%/Share - Firmen"
		set drive "T:"
		
    	set customer_path_unix "[acs_root_dir][parameter::get_from_package_key -package_key "intranet-cust-kolibri" -parameter "CustomerFolder"]"
    	set project_path [kolibri::project_path -project_id $project_id]
    	if {[regexp "${customer_path_unix}/(.*)" $project_path match project_folder]} {
			switch $type {
				project {
					return "file:///${drive}/${project_folder}"
				}
				sdlproj {
					set project_nr [db_string project_nr "select project_nr from im_projects where project_id = :project_id" -default ""]
					if {$project_nr eq ""} {
						return ""
					} else {
						return "file:///${drive}/${project_folder}/Trados/${project_nr}.sdlproj" 
					}
				}
				company {
					set company_id [db_string company "select company_id from im_projects where project_id = :project_id" -default ""]
					set company_path [kolibri::customer_path -company_id $company_id]
					regexp "${customer_path_unix}(.*)" $company_path match company_folder

					return "file:///${drive}/${company_folder}"
				}
				final_company {
					if {[db_0or1row final_company "select final_company_id, company_id from im_projects where project_id = :project_id"]} {
						if {$final_company_id ne ""} {
							set final_company_path [kolibri::final_customer_path -company_id $company_id -final_company_id $final_company_id]
							regexp "${customer_path_unix}(.*)" $final_company_path match final_company_folder
							return "file:///${drive}/${final_company_folder}"
						} else {
							return [kolibri::local_path -project_id $project_id -type "company"]
						}
					} else {
						return ""
					}
				}
				default {
					return "file:///${drive}"
				}
			}
		} else {
			return ""
		}
	}

	ad_proc -public setup_trados_directory {
		-project_id
	} {
		Setup the trados directory for a project

		Will create company folders for trados as well if they don't exist

		@param project_id Project we want trados setup
		@return path UNIX path to the trados directory
	} {
		set locale "en_US"
	
		# Get some missing variables about the project and the company
		set query "
		select
			im_category_from_id(p.source_language_id) as source_language,
			project_nr,
			project_name
		from
			im_projects p
		where
			p.project_id = :project_id
		"
		if { ![db_0or1row projects_info_query $query] } {
			cog_log Error "Can't find the project $project_id"
			return ""
		}

		set trados_directories [list "Originals" "Reports" $source_language]		
			
		# Create the project dir if it doesn't already exist
		set project_dir [kolibri::project_path -project_id $project_id]
		im_filestorage_mkdir -dir ${project_dir}/Trados
		
		# Create the various subdirectories directory
		foreach target_language [im_target_languages $project_id] {
			lappend trados_directories $target_language
		}
		
		foreach trados_dir $trados_directories {
			cog_log Debug "im_trans_trados_setup_directory: trados_dir=${project_dir}/Trados/$trados_dir"
			im_filestorage_mkdir -dir ${project_dir}/Trados/$trados_dir
		}
		
		
		# ---------------------------------------------------------------
		# Create and download the sdlproj
		# ---------------------------------------------------------------
		set project_name_xml [ad_text_to_html $project_name]
		
		# Only use one sdltpl file for the time being
		set trados_sdlproj "[acs_package_root_dir intranet-trans-trados]/lib/PT.sdlproj"
			
		# Calculate the GUID for the Project and append missing 0
		set guid $project_id
		set len [string length $project_id]
		set missing_frac [expr 8-$len]
		while {$missing_frac > 0} {
			set guid "${guid}0"
			set missing_frac [expr $missing_frac-1]
		}
		
		# Create the language mappings
		set target_language_ids [im_target_language_ids $project_id]
		
		set language_mapping_xml ""
		set language_directions_xml "<LanguageDirections>"
		set language_settings_bundle_guid "454df948-fb6e-44d7-8d7c-0426fca7a774"
		
		foreach target_language_id $target_language_ids {
		
			# Calculate the GUID for the Language and append missing 0
			set language_guid $target_language_id
			set len [string length $target_language_id]
			set missing_frac [expr 8-$len]
			while {$missing_frac > 0} {
				set language_guid "${language_guid}0"
				set missing_frac [expr $missing_frac-1]
			}
		
			append language_directions_xml "
			<LanguageDirection Guid=\"${language_guid}-e496-4c9c-be52-afb515a59023\" SettingsBundleGuid=\"$language_settings_bundle_guid\" TargetLanguageCode=\"[im_category_from_id -translate_p 0 $target_language_id]\" SourceLanguageCode=\"$source_language\">
				<AutoSuggestDictionaries />
				<CascadeItem OverrideParent=\"false\" StopSearchingWhenResultsFound=\"false\" />
			</LanguageDirection>"
		}
		append language_directions_xml "</LanguageDirections>"
		
		# Create a "unique" start_date
		set start_date [db_string test "select to_char(now(),'YYYY-MM-DD HH24:MM:SS') from dual"]
		set start_date [join $start_date "T"]
		append start_date ".7767411Z"
		set termbase_xml ""
		set tm_xml ""
		
		# ---------------------------------------------------------------
		# Finalize and write the sdlproj
		# ---------------------------------------------------------------
		set file [open "$trados_sdlproj"]
		fconfigure $file -encoding "utf-8"
		set sdlproj_template [read $file]
		close $file
		
		eval [template::adp_compile -string $sdlproj_template]
		set sdlproj $__adp_output
		
		# write the file to disk
		set return_sdlproj "${project_dir}/Trados/${project_nr}.sdlproj"
		set file [open $return_sdlproj w]
		fconfigure $file -encoding "utf-8"
		puts $file $sdlproj
		flush $file
		close $file
	}

		ad_proc -public trados_project_p {
		    -project_id:required
		} {
		    Check if this is trados project

		    @param project_id project we look at
		    @return 1 if this a trados project
		} {
		    set project_dir [kolibri::project_path -project_id $project_id]
		    set trados_dir "${project_dir}/Trados"
		    set sdlproj_file [glob -nocomplain -join -dir ${trados_dir} *.sdlproj]
		    if {$sdlproj_file eq ""} {
			return 0
		    } else {
			return 1
		    }
		}
}

ad_proc -public kolibri_create_folders {
	-project_id:required
} {
	Create the CR Folders for the kolibri projects
} {
	set company_contact_id [db_string contact "select company_contact_id from im_projects where project_id = :project_id" -default ""]
	set assignee_ids [db_list assignees "select assignee_id from im_freelance_assignments a, im_freelance_packages p
		where a.freelance_package_id = p.freelance_package_id
		and a.assignment_status_id not in (4223,4228,4230,4231)
		and p.project_id = :project_id"]

	set project_type_id [db_string project_type "select project_type_id from im_projects where project_id = :project_id" -default ""]
	if {[lsearch [im_sub_categories [im_project_type_translation]] $project_type_id]<0} {
		set kolibri_directories [list]
	} else {
		# Translation project
		set kolibri_directories [list "Original" "Projektinfos" "Projektinfos extern" "Lieferung"]
	}
	
	foreach folder $kolibri_directories {
		set folder_id [cog::file::get_folder_id -folder $folder -context_id $project_id -break_inheritance_p 1 -create]
		switch $folder {
			"Original" {
				# Customer + Freelancer
				if {$company_contact_id ne ""} {
					permission::grant -party_id $company_contact_id -object_id $folder_id -privilege write
					permission::grant -party_id $company_contact_id -object_id $folder_id -privilege read
				}
				foreach assignee_id $assignee_ids {
					permission::grant -party_id $assignee_id -object_id $folder_id -privilege read
				}
			}
			"Projektinfos" {
				# Customer
				if {$company_contact_id ne ""} {
					permission::grant -party_id $company_contact_id -object_id $folder_id -privilege write
					permission::grant -party_id $company_contact_id -object_id $folder_id -privilege read
				}
				foreach assignee_id $assignee_ids {
					permission::grant -party_id $assignee_id -object_id $folder_id -privilege read
				}
			}
			"Lieferung" {
				if {$company_contact_id ne ""} {
					permission::grant -party_id $company_contact_id -object_id $folder_id -privilege read
				}
			}
			"Projektinfos extern" {
				# Freelancer
				foreach assignee_id $assignee_ids {
					permission::grant -party_id $assignee_id -object_id $folder_id -privilege read
				}
			}
		}
        # Grant permissions also to all employees
        permission::grant -party_id [im_profile_employees] -object_id $folder_id -privilege write
        permission::grant -party_id [im_profile_employees] -object_id $folder_id -privilege read
	}

}
