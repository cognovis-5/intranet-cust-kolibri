# packages/intranet-cust-kolibri/tcl/intranet-cust-kolibri-callback-procs.tcl

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#

ad_library {
    
    Kolibri Custom Callback Procs
    
    @author  malte.sussdorff@cognovis.de
    @creation-date 2017-05-29
    @cvs-id $Id$
}


# ---------------------------------------------------------------
# INVOICE callbacks
# ---------------------------------------------------------------

ad_proc -public -callback im_invoice_after_create -impl aa_kolibri_update_cost_center {
	{-object_id:required}
	{-status_id ""}
	{-type_id ""}
} {
	Update the cost center of the invoice with the one from the project
} {
	db_1row cost_center "select p.cost_center_id,p.project_id,p.project_type_id from im_projects p,im_costs c where p.project_id = c.project_id and c.cost_id = :object_id"

	if {"" == $cost_center_id} {
		# We need to first update the project cost center
		set cost_center_id [kolibri_update_project_cost_center -project_id $object_id -type_id $project_type_id]
	}

	if {"" != $cost_center_id} {
		# Update the cost center
		db_dml update "update im_costs set cost_center_id = :cost_center_id where cost_id = :object_id"
		return "Updated cost_center for $object_id to $cost_center_id"
	} else {
		return ""
	}
}


ad_proc -public -callback im_invoice_after_create -impl im_invoices_storno_corrected {
    -object_id
    -status_id
    -type_id
} {
    Callback to create a storno invoice in case of a correction
} {
    set invoice_id $object_id

    switch $type_id {
		3725 - 3735 {
			# This is a correction, we need to create a cancellation invoice first
			
			# What is the linked invoice
			set linked_invoice_ids [relation::get_objects -object_id_two $invoice_id -rel_type "im_invoice_invoice_rel"]
			set linked_invoice_ids [concat [relation::get_objects -object_id_one $invoice_id -rel_type "im_invoice_invoice_rel"] $linked_invoice_ids]
			if {[llength $linked_invoice_ids] ne 1} {
				# We don't know for which linked invoice this cancellation is, so do nothing
				cog_log Error "We could not create a cancellation for the created invoice [im_name_from_id $object_id]"
			} else {
				set linked_invoice_id [lindex $linked_invoice_ids 0]
				# Create the cancellation invoice
				switch $type_id {
					3725 { set target_cost_type_id [im_cost_type_cancellation_invoice] }
					3735 { set target_cost_type_id [im_cost_type_cancellation_bill] }
				}

				set new_invoice_id [cog::invoice::copy_new -source_invoice_ids $linked_invoice_id -target_cost_type_id $target_cost_type_id]
				if {$new_invoice_id >0} {
					return "Created $new_invoice_id [im_name_from_id $new_invoice_id]"
				} else {
					return ""
				}
			}
		}
		default {
			return ""
		}
    }
}

ad_proc -public -callback im_invoice_after_create -impl kolibri_copy_invoice {
	-object_id
	-status_id
	-type_id
} {
	Callback to set the purchase order to paid if we generate a provider bill out of it
} {
	set ignore_status_ids [list [im_cost_status_paid] [im_cost_status_deleted] [im_cost_status_rejected] [im_cost_status_cancelled] [im_cost_status_filed]]
	switch $type_id {
		3704 {
			set linked_invoice_ids [relation::get_objects -object_id_two $object_id -rel_type "im_invoice_invoice_rel"]
			set linked_invoice_ids [concat [relation::get_objects -object_id_one $object_id -rel_type "im_invoice_invoice_rel"] $linked_invoice_ids]
			foreach linked_invoice_id $linked_invoice_ids {
				set sql "select 1 from im_costs where cost_type_id = [im_cost_type_po]
					and cost_status_id not in ([template::util::tcl_to_sql_list $ignore_status_ids])
					and cost_id = :linked_invoice_id"
				set update_p [db_string created_po $sql -default 0]
				if {$update_p} {
					db_dml update_invoice "update im_costs set cost_status_id = [im_cost_status_filed] where cost_id = $linked_invoice_id"
					return "Set $linked_invoice_id to filed"
				}
			}
		}
		3700 {
			set linked_invoice_ids [relation::get_objects -object_id_two $object_id -rel_type "im_invoice_invoice_rel"]
			set linked_invoice_ids [concat [relation::get_objects -object_id_one $object_id -rel_type "im_invoice_invoice_rel"] $linked_invoice_ids]
			foreach linked_invoice_id $linked_invoice_ids {
				set sql "select 1 from im_costs where cost_type_id = [im_cost_type_quote]
					and cost_status_id not in ([template::util::tcl_to_sql_list $ignore_status_ids])
					and cost_id = :linked_invoice_id"
				set update_p [db_string created_quote $sql -default 0]
				if {$update_p} {
					db_dml update_invoice "update im_costs set cost_status_id = [im_cost_status_filed] where cost_id = $linked_invoice_id"
					return "Set $linked_invoice_id to filed"
				}
			}
		}
	}
}

ad_proc -public -callback im_invoice_after_create -impl kolibri_store_buchhaltung_pdf {
	{-object_id:required}
	{-status_id ""}
	{-type_id ""}
} {
	Generate a PDF of the invoice in the Buchhaltung folder
} {
	return [kolibri::store_buchhaltung_pdf -invoice_id $object_id]
}


ad_proc -public -callback im_invoice_after_update -impl kolibri_generate_pdf {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    This is the complex handle all types of invoice changes function for collmex
} {
    set relevant_status_ids [parameter::get_from_package_key -package_key "intranet-collmex" -parameter "RelevantCostStatus"]
    if {[lsearch $relevant_status_ids $status_id]>-1} {
		# Generate a revision
		return "Created invoice PDF revision [intranet_openoffice::invoice_pdf -invoice_id $object_id]"
    }
}

ad_proc -public -callback im_invoice_after_update -impl zz_kolibri_accepted_denied_quote {
    -object_id
    -status_id
    -type_id
} {
	notify the pm about the acceptance of a quote. 
} {
	set user_id [auth::get_user_id]
    if {$type_id eq [im_cost_type_quote]} {

		if {$status_id eq [im_cost_status_accepted]} {
			db_1row project "select p.project_id, p.project_lead_id, p.project_nr, 
					i.invoice_nr, i.company_contact_id
				from im_costs c, im_projects p, im_invoices i
				where c.cost_id = :object_id
					and c.cost_id = i.invoice_id
					and c.project_id = p.project_id"
			if {$user_id ne $project_lead_id} {
				set project_url [webix::trans::project_url -project_id $project_id]
				set notification_id [webix::notification::create \
					-creation_user_id $user_id \
					-context_id $object_id \
					-project_id $project_id \
					-recipient_id $project_lead_id \
					-message "[_ intranet-cust-kolibri.quote_accepted]" \
					-notification_status_id [webix_notification_status_default] \
					-notification_type_id [webix_notification_type_quote_accepted]]
				

				webix::notification::action_create \
					-notification_id $notification_id \
					-action_type_id [webix_notification_action_type_open_assignment_overview] \
					-action_object_id $project_id 
			
				webix::notification::action_create \
					-notification_id $notification_id \
					-action_type_id [webix_notification_action_type_send_email] \
					-action_object_id $company_contact_id
			}
		} elseif {$status_id eq [im_cost_status_rejected]} {
			db_1row project "select p.project_id, p.project_lead_id, p.project_nr, 
					i.invoice_nr, i.company_contact_id
				from im_costs c, im_projects p, im_invoices i
				where c.cost_id = :object_id
					and c.cost_id = i.invoice_id
					and c.project_id = p.project_id"

			if {$user_id ne $project_lead_id} {
				set project_url [webix::trans::project_url -project_id $project_id]
				set notification_id [webix::notification::create \
					-creation_user_id $user_id \
					-context_id $object_id \
					-project_id $project_id \
					-recipient_id $project_lead_id \
					-message "[_ intranet-cust-kolibri.quote_denied]" \
					-notification_status_id [webix_notification_status_important] \
					-notification_type_id [webix_notification_type_quote_denied]]

				webix::notification::action_create \
					-notification_id $notification_id \
					-action_type_id [webix_notification_action_type_send_email] \
					-action_object_id $company_contact_id
			}
		}
    }
}

ad_proc -public -callback im_invoice_after_update -impl kolibri_store_buchhaltung_pdf {
	{-object_id:required}
	{-status_id ""}
	{-type_id ""}
} {
	Generate a PDF of the invoice in the Buchhaltung folder
} {
	return [kolibri::store_buchhaltung_pdf -invoice_id $object_id]
}


#---------------------------------------------------------------
# Mail sending callbacks
#---------------------------------------------------------------

ad_proc -public -callback intranet-invoices::mail_before_send -impl aaa_kolibri_accept_quote_link {
	{-invoice_id:required}
	{-type_id:required}
} {
	Set the accept url for the quote
} {
	upvar accept_url accept_url
	set accept_url ""
	
	if {$type_id eq [im_cost_type_quote]} {
		upvar accept_url accept_url
		set user_id [db_string user "select company_contact_id from im_invoices where invoice_id = :invoice_id" -default ""]
		if {$user_id ne ""} {
			
			set auto_login [im_generate_auto_login -user_id $user_id]
			set project_id [db_string project "select project_id from im_costs where cost_id = :invoice_id"]
			set base_url "[ad_url][apm_package_url_from_key "intranet-cust-kolibri"]"
			set accept_url [export_vars -base "${base_url}quote2order" -url {user_id auto_login invoice_id}]
		}
	}
}

ad_proc -public -callback im_invoices::oo::before_render -impl kolibri_offer {
    {-invoice_id:required}
    {-cost_type_id ""}
	-current_user_id:required
} {
    If this is an offer, set the valid until date and get the version number of the offer
} {
    if {[im_category_is_a $cost_type_id [im_cost_type_quote]]} {

		# Deal with the valid until
		upvar valid_until_pretty valid_until_pretty
		
	
		set company_contact_id [db_string company_contact_id "select company_contact_id from im_invoices where invoice_id = :invoice_id" -default ""]
		if {$company_contact_id ne ""} {
			set locale [lang::user::locale -user_id $company_contact_id]
		} else {
			set locale [lang::system::locale]
		}
	
		set valid_until [db_string valid_until "select effective_date + interval '1 month' from im_costs where cost_id = :invoice_id" -default ""]
		set valid_until_pretty [lc_time_fmt $valid_until "%x" $locale]
		
		# Take a look at the version number
		upvar offer_nr offer_nr
		
		db_1row invoice_info "select invoice_nr from im_invoices where invoice_id = :invoice_id"
		set invoice_item_id [content::item::get_id_by_name -name "${invoice_nr}.pdf" -parent_id $invoice_id]
		
		set offer_nr [db_string offers "select count(*) from cr_revisions where item_id = :invoice_item_id" -default 0]
		
		# Set the title of the offer
		upvar offer_name offer_name
		
		set project_exists_p [db_0or1row project_info "select * from acs_rels r,
					im_projects p
				where
					r.object_id_one = p.project_id
						and r.object_id_two = :invoice_id limit 1"]
		
		upvar subject_area subject_area
		
		if {$project_exists_p} {
			set offer_name "${project_name}, [im_category_from_id -current_user_id $current_user_id $source_language_id] - "
			set target_langs [list]
			db_foreach target_lang "select im_category_from_id(language_id) as target_language from im_target_languages where project_id = :project_id order by im_category_from_id(language_id)" {
			lappend target_langs "$target_language"
			}
			append offer_name "[join $target_langs ", "]"
			
			# Dienstleistung (aus Material bzw. Projekt)
			set subject_area [im_category_from_id -current_user_id $current_user_id $subject_area_id]
		} else {
			set offer_name $invoice_nr
		}
    }

    upvar currency currency

    return 1
}

# ---------------------------------------------------------------
# PROJECT callbacks
# ---------------------------------------------------------------

ad_proc -public -callback webix::trans::project::after_update -impl "kolibri-potential-to-open-or-declined" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
} {
    After run if the status changed to open from potential

	Will accept the lastest open quote and decline all others
	Will notify the PM about the acceptance if he/she did not do it themselves
	Will update the company status to active.
	Will record the acceptance of the project (with date and who did it)
} {
    if {$old_project_status_id eq [im_project_status_potential]} {
		switch $project_status_id {
			76 {
				db_dml accept_quotes "update im_costs set cost_status_id = [im_cost_status_accepted] where cost_status_id in ([im_cost_status_created],[im_cost_status_outstanding]) and project_id = :project_id"

				set open_ids [im_sub_categories [im_project_status_open]]

				db_dml update_company "update im_companies set company_status_id = [im_company_status_active] where company_id = (select company_id from im_projects where project_id = :project_id)"

				# Record the acceptance
				db_dml update_acceptance "update im_projects set acceptance_date = now(), acceptance_user_id = :user_id where project_id = :project_id"
				
			}
			77 {
				db_dml decline_quotes "update im_costs set cost_status_id = [im_cost_status_rejected] where cost_status_id in ([im_cost_status_created],[im_cost_status_outstanding]) and project_id = :project_id"
			}
		}
	}
}

ad_proc -public -callback webix::trans::project::after_update -impl "kolibri-open-to-delivered" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
} {
    After run if the status changed to open from potential

    Will send out created assignments and setting them to requested
} {
    if {$old_project_status_id eq [im_project_status_open] && $project_status_id eq [im_project_status_delivered]} {
		# Mark notifications as read
		set notification_ids [db_list notifications "select notification_id from webix_notifications where project_id = :project_id"]
		foreach notification_id $notification_ids {
			webix::notification::record_view -notification_id $notification_id
		}

	    set accounting_contact_id [db_string accounting_contact "select accounting_contact_id from im_companies where company_id = [im_company_internal]"]
		set project_name [im_name_from_id $project_id]
		set notification_id [webix::notification::create \
			-creation_user_id $user_id \
	    	-context_id $project_id \
			-project_id $project_id \
			-recipient_id $accounting_contact_id \
	    	-message "[_ intranet-cust-kolibri.lt_Project_project_name_]" \
	    	-notification_status_id [webix_notification_status_important] \
			-notification_type_id [webix_notification_type_project_delivered]]
    }
}

ad_proc -public -callback im_project_on_submit -impl aaa_kolibri_check_for_logged_hours {
	{-object_id:required}
	{-form_id:required}

} {
	Check if the project has logged hours, otherwise do not allow to close the project
} {

	set status_id [template::element::get_value $form_id project_status_id]
	upvar error_field error_field
	upvar error_message error_message

	set closed_ids [im_sub_categories 81]
	if {[lsearch $closed_ids $status_id]>-1} {
		# Check that we have logged hours
		set logged_hours_p [db_string logged_hours "select 1 from im_hours where project_id in (select project_id from im_projects where parent_id = :object_id union select :object_id from dual) limit 1" -default 0]

		if {!$logged_hours_p} {
			set error_field "project_status_id"
			set error_message "[_ intranet-cust-kolibri.lt_Your_need_logged_hours_for_close]"
		}
	}
}


ad_proc -public -callback webix::trans::project::after_update -impl "kolibri-update-delivered-tms" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
} {
    After run if the status changed to open from delivered

	Will update the TMs in the company folder from the TMs found in the project trados folder
} {
    if {$old_project_status_id eq [im_project_status_open] && $project_status_id eq [im_project_status_delivered]} {
       	db_1row project_query {
			select project_id, project_nr, source_language_id,
				project_type_id, company_id, final_company_id, subject_area_id
			from im_projects
			where project_id=:project_id
		}
		im_trans_trados_project_info -project_id $project_id -array_name project_info
		
		set project_dir $project_info(project_dir)
		set trados_dir $project_info(trados_dir)
		
		set source_language [im_name_from_id $source_language_id]
		
		# ---------------------------------------------------------------
		# Update the TMs
		# ---------------------------------------------------------------
		
		foreach target_language_id [im_target_language_ids $project_id] {
			# Get the expected TMs for the target language
		
			set tm_paths [im_trans_trados_get_trans_memory_file \
					-company_id $company_id \
					-final_company_id $final_company_id \
					-source_language_id $source_language_id \
					-target_language_id $target_language_id \
					-subject_area_id $subject_area_id]
		
			foreach file_path $tm_paths {
				# Check if the file exists
				set filename "[lindex [split [file rootname $file_path] "/"] end].sdltm"
				if {[file exists "${trados_dir}/TM/$filename"]} {
					cog_log Debug "Updating TM $file_path from project $project_nr"
					# Upload the new TM
					im_trans_trados_update_tm \
						-current_tm $file_path \
						-new_tm ${trados_dir}/TM/$filename \
						-project_id $project_id
						
					# Rename the file so we won't update it again
					file rename "${trados_dir}/TM/$filename" "${trados_dir}/TM/Finalized_$filename"
				}
			}
		}
    }
}

ad_proc -public -callback webix::trans::project::after_update -impl "kolibri-mark-notifications-read" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
} {
    After run if the status changed to closed/deleted/charged

	Will update the TMs in the company folder from the TMs found in the project trados folder
} {
	set closed_status_ids [list [im_project_status_closed] [im_project_status_declined] [im_project_status_deleted] [im_project_status_invoiced]]
	
	if {[lsearch $closed_status_ids $project_status_id] >-1} {
		# Mark notifications as read
		set notification_ids [db_list notifications "select notification_id from webix_notifications where project_id = :project_id"]
		foreach notification_id $notification_ids {
			webix::notification::record_view -notification_id $notification_id
		}
	}
}

ad_proc -public -callback webix::trans::project::after_update -impl "kolibri-close-subtasks and projects" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
} {
    After run if the status changed to closed/deleted/charged

	Will close all subprojects (including tasks etc.)
} {
	set closed_status_ids [list [im_project_status_closed] [im_project_status_declined] [im_project_status_deleted] [im_project_status_invoiced]]
	
	if {[lsearch $closed_status_ids $project_status_id] >-1} {
		# Close subprojects
		db_dml update_subprojects "update im_projects set project_status_id = :project_status_id where parent_id = :project_id"
	}
}

# ---------------------------------------------------------------
#  COMPANY callbacks
# ---------------------------------------------------------------

ad_proc -public -callback im_company_after_create -impl kolibri_update_vat_and_tax {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    If a company is saved automatically determine the VAT and tax classification
} {
    set company_name [db_string company "select company_name from im_companies where company_id = :object_id"]
	if {[lsearch [im_sub_categories [im_company_type_provider]] $type_id]>-1} {
			# Send mail to accounting contact
			set to_addr "sourcing@kolibri.online"
	
			intranet_chilkat::send_mail \
				-from_party_id [auth::get_user_id] \
				-to_addr $to_addr \
				-subject "[_ intranet-cust-kolibri.lt_Provider_created_comp]" \
				-body "[_ intranet-cust-kolibri.lt_We_created_the_Provid] <a href=[export_vars -base "[ad_url]/intranet/companies/view" -url {company_id}]>$company_name</a>."
	}

	return "Created and [callback::im_company_after_update::impl::kolibri_update_vat_and_tax -object_id $object_id -status_id $status_id -type_id $type_id]"
}

ad_proc -public -callback im_company_after_create -impl kolibri_update_vat_after_create {
	{-object_id:required}
	{-status_id ""}
	{-type_id ""}
} {
	If a company is saved automatically determine the VAT and tax classification
} {
	set company_type_id $type_id
	set company_id $object_id
	set country_code [db_string address "select address_country_code from im_offices o, im_companies c where c.main_office_id=o.office_id and c.company_id = :company_id"]	
    set vat_type_id [kolibri::get_vat_type_based_on_country -type_id $type_id -country_code $country_code]

	# update the company
	db_dml update_company "update im_companies set vat_type_id = :vat_type_id where company_id = :object_id"
	
	# Update the modification date
	db_dml update "update acs_objects set last_modified = now() where object_id = :object_id"
	return "Updated company $object_id .. $company_type_id :: $country_code :: $vat :: $vat_type"
}


ad_proc -public -callback im_company_after_update -impl kolibri_update_vat_and_tax {
	{-object_id:required}
	{-status_id ""}
	{-type_id ""}
} {
	If a company is saved automatically determine the VAT and tax classification
} {
	set company_type_id $type_id
	set company_id $object_id
	set country_code [db_string address "select address_country_code from im_offices o, im_companies c where c.main_office_id=o.office_id and c.company_id = :company_id"]	
	if {[im_permission [auth::get_user_id] "fi_write_all"]} {
		db_1row vat_info "select vat_type_id as vat_type, default_vat as vat from im_companies where company_id = :company_id"
	} else {
		# Figure out the country
		set vat_type [kolibri::get_vat_type_based_on_country -type_id $type_id -country_code $country_code]
	}
    set vat_type [kolibri::get_vat_type_based_on_country -type_id $type_id -country_code $country_code]
	# Set default payment terms
	set payment_term_id [db_string payment_term "select payment_term_id from im_companies where company_id = :object_id" -default ""]
	if {"" == $payment_term_id} {
		set provider_p [im_category_is_a $type_id [im_company_type_provider]]
		if {$provider_p} {
			# Providers have 30 day payment terms
			set payment_term_id 80130
		} else {
			set payment_term_id 80114
		}
	}

	# update the company
	db_dml update_company "update im_companies set company_type_id = :company_type_id, default_vat = :vat , vat_type_id = :vat_type, payment_term_id = :payment_term_id where company_id = :object_id"
	
	# Update the modification date
	db_dml update "update acs_objects set last_modified = now() where object_id = :object_id"
	return "Updated company $object_id .. $company_type_id :: $country_code :: $vat :: $vat_type"
}

ad_proc -public -callback intranet-translation::task_status_package_file -impl kolibri_trados {
	{-type:required}
	{-user_id ""}
	{-target_language_id:required}
	{-project_id:required}
} {
	Check if a package file is created for that target_language_id
} {
	upvar package_file package_file
	set package_file [im_trans_trados_package_file -target_language_id $target_language_id -project_id $project_id -task_type $type]
	if {$package_file ne ""} {
		# Update the tasks in the project for the language.
		switch $type {
			trans {
				db_dml update_trans_tasks "update im_trans_tasks set task_status_id = 342 where task_status_id = 340 and target_language_id = :target_language_id and project_id = :project_id"
			}
			edit {
				db_dml update_trans_tasks "update im_trans_tasks set task_status_id = 346 where task_status_id = 345 and target_language_id = :target_language_id and project_id = :project_id"
			}
		}
	}
}

ad_proc -public -callback im_member_add_before_form_extend -impl kolibri_im_before_form_extend {
    {-user_id:required}
    {-object_id:required}
} {
	Check if a package file is created for that target_language_id
} {
	# Make sure that object type is company
	upvar notify_checked notify_checked
	set object_type [acs_object_type $object_id]
	if {$object_type eq "im_company"} {
		set notify_checked 0
	}
}

# ---------------------------------------------------------------
#  FREELANCE PACKAGES callbacks
# ---------------------------------------------------------------


#---------------------------------------------------------------
# Project default filters
#---------------------------------------------------------------

ad_proc -public -callback cog_rest::openapi::response_schema -impl kolibri_project_default_filters {
	-proc_name:required
	-object_name:required
} {
	Append role filter data along with logic

} {
	if {$proc_name eq "cog_rest::get::project_default_filters"} {
		upvar response_object_schema response_object_schema

		set return_element {skill_role category_array "Intranet Skill Role" Kolibri specific List of Roles the freelancer has experience in.}

		append response_object_schema [cog_rest::openapi::response::object_schema -return_element $return_element -object_name $object_name]
		return "Appended $return_element"
	}
}

ad_proc -public -callback webix::assignment::default_filters -impl kolibri_project_default_filters {
    -project_id:required
    -user_id:required
	-freelance_package_id:required
} {
    Append role filters (defaults)
} {
	set trans_task_ids [db_list tasks "select task_id from im_trans_tasks where project_id = :project_id"]
    set trans_task_type_ids [webix::assignments::trans_task_type_ids -trans_task_ids $trans_task_ids]

	# Get the workflow steps in the correct order
	upvar skill_role_ids skill_role_ids
	set skill_role_ids [list]

	if {$freelance_package_id ne ""} {
		set trans_task_type_ids [db_string package_type "select package_type_id from im_freelance_packages where freelance_package_id = :freelance_package_id" -default $trans_task_type_ids]
	}
	
	foreach package_type_id $trans_task_type_ids {
		set workflow_step [db_string workflow_step "select category from im_categories where category_id = :package_type_id"]
		set skill_role_id  [db_string role_skills "select category_id from im_categories where category_type = 'Intranet Skill Role' and aux_string1 = :workflow_step" -default ""]
		if {$skill_role_id ne ""} {
			lappend skill_role_ids $skill_role_id
		}
	}
	if {[llength $skill_role_ids]>0} {
		return "Appended $skill_role_ids"
	}
}

ad_proc -public -callback cog_rest::object_return_element -impl kolibri_project_default_filters {
    -proc_name
    -object_class
} {
    append role filters

} {
	if {$proc_name eq "cog_rest::get::project_default_filters"} {
		upvar return_elements return_elements
		lappend return_elements {skill_role category_array "Intranet Skill Role" Kolibri specific List of Roles the freelancer has experience in.}
		return "Added Skill Role for Kolibri"
	}
} 

ad_proc -public -callback webix::packages::after_deadline_calculation -impl kolibri_working_hours {
	-freelance_package_id:required
	-package_deadline:required
} {
	Reset the deadline if the hours are between 18:00 and 08:00

	Reset to the next working day as well
} {

	if {$package_deadline ne ""} {
		set deadline_hour [db_string hour "select date_part('hour', timestamp :package_deadline)" -default ""]
		set deadline_date [lindex [split $package_deadline " "] 0]
		if {$deadline_hour < 8} {
			upvar package_deadline new_package_deadline
			set new_package_deadline "$deadline_date 08:00"
		}
		if {$deadline_hour >17} {
			if {$deadline_hour eq 18} {
				set deadline_minute [db_string hour "select date_part('minute', timestamp :package_deadline)" -default ""]
				if {$deadline_minute eq 0} {
					# If the deadline is exactly 18:00, then it is fine
					return ""
				}
			}
			# Get the next working day
			upvar package_deadline new_package_deadline
			set new_package_deadline [im_translation_processing_deadline -start_timestamp "$deadline_date 08:00" -days 1]
			return "Set deadline for $freelance_package_id to $new_package_deadline"
		}
	}
}

ad_proc -public -callback webix::assignment::trans_assignment_after_create -impl z_kolibri_start_date {
    -assignment_id:required
    -assignment_status_id:required
    -assignment_type_id:required
    -user_id:required
} {
	sets the start_date of the assignment in case of previous steps or if steps are following.

	Also sets the start_date to now if the current assignment has no start date (for whatever the reason)
} {
	kolibri::update_start_dates -assignment_id $assignment_id -user_id $user_id
	set relevant_status_ids [list 4220 4221 4222 4224 4225 4226 4229 4231]

	# Get the next assignments and check if the start_date is less than two hours away from the end_date of this assignment
	set next_assignment_ids [webix::assignments::next_assignments -assignment_id $assignment_id -assignment_status_ids $relevant_status_ids]
	foreach next_assignment_id $next_assignment_ids {
		kolibri::update_start_dates -assignment_id $next_assignment_id -user_id $user_id
	}
#	return "Updated start dates for $assignment_id and $next_assignment_ids"
}


ad_proc -public -callback webix::assignment::trans_assignment_after_update -impl z_kolibri_deadline_change {
    -assignment_id:required
	-user_id:required
} {
	If the deadline has changed:
	* The freelancer will receive an e-mail informing him that the deadline of an assignment has been changed.
	* Additionally the freelancer receives a notification in the system.

} {
	upvar old_assignment old
	db_1row info "select fa.end_date, fa.assignee_id, fa.assignment_name,fa.assignment_status_id, fp.project_id, p.project_lead_id
		from im_freelance_assignments fa, im_freelance_packages fp, im_projects p
		where assignment_id = :assignment_id
		and fa.freelance_package_id = fp.freelance_package_id
		and p.project_id = fp.project_id"
	
	set relevant_status_ids [list 4220 4221 4222 4224 4225 4226 4229]
	switch $assignment_status_id {
		4222 - 4224 - 4225 - 4226 - 4229 {
			# Active assignment, notify the freelancer
			set old_end_date $old(end_date)
			if {$end_date ne $old_end_date} {
				cog_log Debug "Kolibri update deadline"
				set notification_id [webix::notification::create \
					-creation_user_id $user_id \
					-context_id $assignment_id \
					-project_id $project_id \
					-recipient_id $assignee_id \
					-message "[_ intranet-cust-kolibri.assignment_deadline_change]" \
					-notification_status_id [webix_notification_status_default] \
					-notification_type_id [webix_notification_type_assignment_deadline_change]]
				
				webix::notification::action_create \
					-notification_id $notification_id \
					-action_type_id [webix_notification_action_type_send_email] \
					-action_object_id $project_lead_id

				# Get the next assignments and check if the start_date is less than two hours away from the end_date of this assignment
				set next_assignment_ids [webix::assignments::next_assignments -assignment_id $assignment_id -assignment_status_ids $relevant_status_ids]
				foreach next_assignment_id $next_assignment_ids {
					kolibri::update_start_dates -assignment_id $next_assignment_id -user_id $user_id
				}
			}
		}
		4220 - 4221 {
			# Only update next assignments
			set next_assignment_ids [webix::assignments::next_assignments -assignment_id $assignment_id -assignment_status_ids $relevant_status_ids]
			foreach next_assignment_id $next_assignment_ids {
				kolibri::update_start_dates -assignment_id $next_assignment_id -user_id $user_id
			}
		}
	}
}

ad_proc -public -callback webix::assignment::trans_assignment_after_update -impl z_kolibri_start_date_change {
    -assignment_id:required
	-user_id:required
} {
	If the start_date has changed:
	* The freelancer will receive an e-mail informing him that the deadline of an assignment has been changed.
	* Additionally the freelancer receives a notification in the system.

} {
	upvar old_assignment old
	db_1row info "select start_date, assignment_name, assignee_id, end_date ,assignment_status_id from im_freelance_assignments where assignment_id = :assignment_id"
	
	set relevant_status_ids [list 4220 4221 4222 4224 4225 4226 4229]
	set old_start_date $old(start_date)
	if {$old_start_date eq ""} {
		set old_start_date $start_date
	}

	if {$start_date ne $old_start_date} {
		set old_end_date $old(end_date)
		if {$old_end_date eq $end_date} {
			# Need to update the end_date as well
			set new_end_date [db_string difference "select start_date + (:old_end_date::timestamp - :old_start_date::timestamp) from im_freelance_assignments where assignment_id = :assignment_id" -default $end_date]
			cog_rest::put::translation_assignments -assignment_id $assignment_id -rest_user_id $user_id -assignment_deadline $new_end_date
		}
	}
}

ad_proc -public -callback webix::assignment::trans_assignment_after_update -impl z_kolibri_fee_unit {
    -assignment_id:required
	-user_id:required
} {
	If the fee or units changed:
	* Add notification to freelancer fee was changed
	* Edit existing purchase order
	* Send out Purchase Order after fee change

} {
	upvar old_assignment old

	db_1row assignment_info "select fa.assignee_id, fa.assignment_name, fa.assignment_status_id, fa.end_date, 
		fa.rate, fa.assignment_units, fa.purchase_order_id,
		p.project_id, p.project_lead_id, fp.freelance_package_id
		from im_freelance_assignments fa, im_freelance_packages fp, im_projects p
		where p.project_id = fp.project_id
		and fa.freelance_package_id = fp.freelance_package_id
		and fa.assignment_id = :assignment_id"

	if {$assignment_units ne $old(assignment_units) || $rate ne $old(rate)} {

		db_dml update_invoice "update im_invoice_items set item_units = :assignment_units, price_per_unit = :rate where context_id = :assignment_id"
		if {$purchase_order_id ne ""} {
			set cost_status_id [db_string cost_status "select cost_status_id from im_costs where cost_id = :purchase_order_id" -default [im_cost_status_created]]
        	cog::callback::invoke -object_type "im_invoice" -object_id $purchase_order_id -action after_update -status_id $cost_status_id -type_id [im_cost_type_po]
			cog::invoice::send_email -invoice_id $purchase_order_id -recipient_id $assignee_id -from_addr [party::email -party_id $project_lead_id]
		}

		set notification_id [webix::notification::create \
			-creation_user_id $user_id \
			-context_id $assignment_id \
			-project_id $project_id \
			-recipient_id $assignee_id \
			-message "[_ intranet-cust-kolibri.assignment_fee_change]" \
			-notification_status_id [webix_notification_status_default] \
			-notification_type_id [webix_notification_type_assignment_deadline_change]]
		
		webix::notification::action_create \
			-notification_id $notification_id \
			-action_type_id [webix_notification_action_type_send_email] \
			-action_object_id $project_lead_id

		return "Created notification $notification_id"
	}
}

ad_proc -public -callback webix::assignment::trans_assignment_after_update -impl z_kolibri_status_change {
    -assignment_id:required
	-user_id:required
} {
	Status changes (e.g. set to denied or accepted)

	Will create a purchase order for the freelancer if accepted.
} {
	upvar old_assignment old


	db_1row assignment_info "select fa.assignee_id, fa.assignment_name, fa.assignment_status_id, fa.end_date, p.project_id, p.project_lead_id, fp.freelance_package_id
		from im_freelance_assignments fa, im_freelance_packages fp, im_projects p
		where p.project_id = fp.project_id
		and fa.freelance_package_id = fp.freelance_package_id
		and fa.assignment_id = :assignment_id"

	cog_log Debug "Kolibri callback switching assignment status from $old(assignment_status_id) to $assignment_status_id"
	set project_folder_id [intranet_fs::get_project_folder_id -project_id $project_id]
	set folder_ids [db_list folders "select item_id from cr_items where parent_id = :project_folder_id and name in ('original','projektinfos-extern')"]
	set package_name [im_name_from_id $freelance_package_id]
	set assignee_name [im_name_from_id $assignee_id]

	set notification_id ""

	switch $old(assignment_status_id) {
		4220 - 4221 {
			switch $assignment_status_id {
				4222 {
					# Accepted
					if {![im_user_is_employee_p $assignee_id]} {
						set invoice_id [webix::assignments::create_purchase_order -assignment_id $assignment_id]
						set message_id [cog::invoice::send_email -invoice_id $invoice_id -recipient_id $assignee_id -from_addr [party::email -party_id $project_lead_id]]
						if {$message_id ne ""} {
							db_dml update_po_to_accepted "update im_costs set cost_status_id = [im_cost_status_accepted] where cost_id = :invoice_id"
						}
					}
										
					set notification_id [webix::notification::create \
						-creation_user_id $user_id \
	    				-context_id $assignment_id \
						-project_id $project_id \
	    				-recipient_id $project_lead_id \
	    				-message "[_ intranet-cust-kolibri.lt_accepted_assignment]" \
						-notification_type_id [webix_notification_type_assignment_accepted]]

					webix::notification::action_create \
						-notification_id $notification_id \
						-action_type_id [webix_notification_action_type_open_assignment_overview] \
						-action_object_id $assignment_id 

					webix::notification::action_create \
						-notification_id $notification_id \
						-action_type_id [webix_notification_action_type_send_email] \
						-action_object_id $assignee_id
										
					set file_uploaded_p [db_string uploaded "select 1 from cr_items where parent_id = :freelance_package_id limit 1" -default 0]
					if {!$file_uploaded_p} {
						db_dml update_severity "update webix_notifications set notification_status_id = [webix_notification_status_important] where notification_id = :notification_id"
						webix::notification::action_create \
							-notification_id $notification_id \
							-action_type_id [webix_notification_action_type_upload_files] \
							-action_object_id $freelance_package_id \
							-action_help "[_ intranet-cust-kolibri.lt_Please_upload_a_packa]"
					}
				}
				4223 {
					# Denied

					set notification_id [webix::notification::create \
						-creation_user_id $user_id \
	    				-context_id $assignment_id \
						-project_id $project_id \
	    				-recipient_id $project_lead_id \
	    				-message "[_ intranet-cust-kolibri.Denied_batch]" \
	    				-notification_status_id [webix_notification_status_important] \
						-notification_type_id [webix_notification_type_assignment_denied]]

					webix::notification::action_create \
						-notification_id $notification_id \
						-action_type_id [webix_notification_action_type_freelancer_selection] \
						-action_object_id $freelance_package_id \
						-action_help "[_ intranet-cust-kolibri.lt_Select_a_new_freelanc]"
					
					webix::notification::action_create \
						-notification_id $notification_id \
						-action_type_id [webix_notification_action_type_send_email] \
						-action_object_id $assignee_id
				}
			}
		}
		4222 - 4224 {
			switch $assignment_status_id {
				4225 {
					# Assignment delivered
					set notification_id [webix::notification::create \
						-creation_user_id $user_id \
	    				-context_id $assignment_id \
						-project_id $project_id \
	    				-recipient_id $project_lead_id \
	    				-message "[_ intranet-cust-kolibri.assignee_deliverd]" \
	    				-notification_status_id [webix_notification_status_default] \
						-notification_type_id [webix_notification_type_assignment_delivered]]

					# Check if we have a file uploaded and attach an action
					set file_uploaded_p [db_string uploaded "select 1 from cr_items where parent_id = :assignment_id limit 1" -default 0]
					if {$file_uploaded_p} {
						webix::notification::action_create \
							-notification_id $notification_id \
							-action_type_id [webix_notification_action_type_upload_files] \
							-action_object_id $assignment_id \
							-action_help "[_ intranet-cust-kolibri.lt_Access_uploaded_files]"
					}
					
					webix::notification::action_create \
						-notification_id $notification_id \
						-action_type_id [webix_notification_action_type_send_email] \
						-action_object_id $assignee_id
				}
			}
		}
	}

	set returns [list]
	if {$notification_id ne ""} {
		lappend returns "Created notification $notification_id"
	}
	# Handle Permissions
	switch $assignment_status_id {
		4221 - 4222 {
			foreach folder_id $folder_ids {
				permission::grant -party_id $assignee_id -object_id $folder_id -privilege read
				lappend returns "Read granted for $assignee_id on folder $folder_id"
			}
		}
		4223 - 4228 - 4230 - 4231 {
			foreach folder_id $folder_ids {
				permission::revoke -party_id $assignee_id -object_id $project_id -privilege read
				permission::revoke -party_id $assignee_id -object_id $folder_id -privilege read
				lappend returns "Read revoked from $assignee_id on folder $folder_id & project $project_id"
			}
		}
	}
	return $returns
}


ad_proc -public -callback webix::trans::project::after_create -impl kolibri {
	-project_id:required
	-user_id:required
} {
	Prepares a new project for Kolibri

	1. Setup CR Folders
	2. Setup OneDrive Folders
	3. Update the cost center based on project data
	4. Add Timesheet tasks depending on task type
} {
	# 1. Create CR 
	kolibri_create_folders -project_id $project_id
	
	# 2. OneDrive Folders
	kolibri::project_path -project_id $project_id

	# 3. Cost Center
	kolibri_update_project_cost_center -project_id $project_id

	db_1row project_info "select project_type_id from im_projects where project_id = :project_id"
	
	# 4. Timesheet Tasks
	set task_list [list]
	

	switch $project_type_id {
		10000391 - 10000392 {
            cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_Offer] -task_type_id 9501
			cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_copy] -task_type_id 9509
			cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_proof] -task_type_id 9508
			cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_PM] -task_type_id 9501	
            cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Discovery_call] -task_type_id 9501					
		}
        10000808 {
			cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_organisation] -task_type_id 9501
			cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_marketing] -task_type_id 9501
			cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_Tn_internal_support] -task_type_id 9501
			cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_Tn_aftercare] -task_type_id 9501
			cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_client_communication] -task_type_id 9501
			cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_execution] -task_type_id 9501	
			cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_adjustments_presentation] -task_type_id 9501
			cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_final_test] -task_type_id 9501		      
        }
        10000994 {
            cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_Offer] -task_type_id 9501
            cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Pre-Editing] -task_type_id 9501
            cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Post-Editing] -task_type_id 9501
            cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_PM] -task_type_id 9501	
            cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Terminology] -task_type_id 9501
            cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_Final] -task_type_id 9505
        }
		default {
            cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_Offer] -task_type_id 9501
			cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_Glossar] -task_type_id 9505
			cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_Final] -task_type_id 9505
			cog_rest::post::timesheet_task -project_id $project_id -rest_user_id $user_id -task_name [_ intranet-cust-kolibri.Task_PM] -task_type_id 9501					
		}
	}
}

ad_proc -public -callback webix::trans::project::task_types -impl kolibri {
	-project_type_id:required
	-project_id:required
	-user_id:required
} {
	Return the possible task types as per the customer (KOL-2113)
} {
	upvar task_type_ids task_type_ids
	switch $project_type_id {
		87 {
			# trans + proof
			set task_type_ids [list 87 93 95 10000011 10000014]
		}
		89 {
			# Trans + proof + edit
			set task_type_ids [list 87 89 93 95 10000011 10000014]
		}
		10000098 - 10000808 {
			# all - Kolibri intern + Schulung
			set task_type_ids [db_list trans_types "select category_id from im_categories c, im_category_hierarchy ch
			 where enabled_p = 't' and category_type = 'Intranet Project Type'
			 and c.category_id = ch.child_id
			 and ch.parent_id = 2500
			 order by sort_order"]
		}
		10000011 {
			set task_type_ids [list 10000011 10000014]
		}
		10000391 {
			# Content Deutsch
			set task_type_ids [list 10000391 10000014 95]
		}
		10000392 {
			# Content Int.
			set task_type_ids [list 10000392 10000011 95 10000391]
		}
		10000813 {
			# Transcreation
			set task_type_ids [list 87 93 95]
		}
	}
}

ad_proc -public -callback webix::assignment::quality_rating_types -impl kolibri {
    -assignment_ids:required
    -rest_user_id:required
} {
    Callback to define the quality_type_ids and quality_level_ids
} {
	upvar quality_type_ids quality_type_ids
	upvar quality_level_ids quality_level_ids

	set project_type_ids [db_list projects "select distinct project_type_id from im_projects where project_id in (
		select project_id from im_freelance_packages fp, im_freelance_assignments fa 
		where fp.freelance_package_id = fa.freelance_package_id
		and fa.assignment_id in ([template::util::tcl_to_sql_list $assignment_ids])
	)"]

	set kolibri_type_ids [list]
	foreach project_type_id $project_type_ids {
		switch $project_type_id {
			87 - 89 - 10000813 {
				# Translations
				set kolibri_type_ids [concat $kolibri_type_ids [list 7015 7016 7017 7018 7019]]
			}
			10000391 - 10000392 {
				# Texterstellung Deutsch / Fremdsprache
				set kolibri_type_ids [concat $kolibri_type_ids [list 7020 7016 7017 7018 7019]]
			}
			10000011 {
				# Fremdsprachenlektorat
				lappend kolibri_type_ids 7018				
			}
			10000014 {
				# German editing
				lappend kolibri_type_ids 7018
				lappend kolibri_type_ids 7019
			}
		}
	}

	if {[llength $kolibri_type_ids]>0} {
		set quality_type_ids [lsort -unique $kolibri_type_ids]
	}
}

ad_proc -public -callback cog_rest::cr_file_after_upload -impl kolibri_notifications {
	-file_item_id:required
	-file_revision_id
    -context_id
    -context_type
    -user_id
} {
	Notify the users about uploads
} {
	db_1row revision_info "select ci.name, cr.description from cr_items ci, cr_revisions cr 
	where ci.item_id = cr.item_id 
	and cr.revision_id = :file_revision_id"

	switch $context_type {
		im_freelance_package {
		}
		im_freelance_assignment {

			db_1row assignment_info "select fa.assignment_id, fa.assignment_name, fa.assignee_id, fa.assignment_status_id, fa.end_date, p.project_id, project_lead_id, fp.freelance_package_id
				from im_freelance_assignments fa, im_freelance_packages fp, im_projects p
				where p.project_id = fp.project_id
				and fa.freelance_package_id = fp.freelance_package_id
				and fa.assignment_id = :context_id"
			set assignee_name "[im_name_from_id $assignee_id]"

			# Notify the PM that a file was uploaded to the project
			set notification_id [webix::notification::create \
				-creation_user_id $user_id \
				-context_id $assignment_id \
				-project_id $project_id \
				-recipient_id $project_lead_id \
				-message "[_ intranet-cust-kolibri.lt_file_uploaded]" \
				-notification_type_id [webix_notification_type_file_uploaded]]

			webix::notification::action_create \
				-notification_id $notification_id \
				-action_type_id [webix_notification_action_type_cr_direct_download] \
				-action_object_id $file_item_id

			webix::notification::action_create \
				-notification_id $notification_id \
				-action_type_id [webix_notification_action_type_view_comments] \
				-action_object_id $file_item_id \
				-action_help "[_ webix-portal.view_comment]" \
				-action_text $description

			webix::notification::action_create \
				-notification_id $notification_id \
				-action_type_id [webix_notification_action_type_send_email] \
				-action_object_id $assignee_id
		}
		content_folder {
		}
	}
}

ad_proc -public -callback cog_rest::cr_folder_after_upload -impl kolibri_freelancer_mails {
    -file_item_ids
    -folder_id
    -description
    -user_id
} {
	Send out E-Mails to freelancers about the files uploaded

    @param file_item_ids List of ItemIds of the files we upload
    @param folder_id Folder which files where uploaded to 
    @param description String which was provided during the upload
    @param user_id User who uploaded the file
} {

	set context_type [acs_object_type $folder_id]
	switch $context_type {
        im_freelance_assignment {
            # This must be a return file from the freelancer which is associated with the assignment.
			db_1row assignment_info "select assignment_id,p.project_id, project_nr, p.project_status_id,  assignment_status_id, 
                assignee_id, project_lead_id
                from im_freelance_assignments fa, im_freelance_packages fp, im_projects p
                where fa.assignment_id = :folder_id
                and fp.freelance_package_id = fa.freelance_package_id
                and p.project_id = fp.project_id"

            # Confirmation email which confirms that files were uploaded
            # Recipient -> recipient of the file (PM)
			set salutation_pretty [ cog_salutation -person_id $assignee_id]
			set file_names [join [db_list file_names "select name from cr_items where item_id in ([template::util::tcl_to_sql_list $file_item_ids])"] ", "]
			set timestamp [lc_time_fmt [clock format [clock seconds] -format "%Y-%m-%d %H:%M:%S"] "%x %X"]
			set recipient [im_name_from_id $project_lead_id]
            if {$project_status_id eq [im_project_status_open]} {
			    intranet_chilkat::send_mail \
                    -from_party_id $project_lead_id \
                    -to_party_id $assignee_id \
                    -subject "[_ intranet-cust-kolibri.lt_file_uploaded_message_subject]" \
                    -body "[_ intranet-cust-kolibri.lt_file_uploaded_message_body]"
            }
        }
        content_folder {
            # First we need to figure out parent folder
            set parent_folder_id [content::item::get_parent_folder -item_id $folder_id]
            if {$parent_folder_id ne ""} {
                # Now that we have parent_folder_id we can get project_id
                set folder_project_id [intranet_fs::get_project_from_folder_id -folder_id $parent_folder_id]
                if {$folder_project_id ne "" && ([acs_object_type $folder_project_id] eq "im_project")} {
                    # Next let's check project status
                    set project_status_id [db_string get_project_status_id "select project_status_id from im_projects where project_id =:folder_project_id" -default ""]
                    if {$project_status_id ne "" && $project_status_id eq [im_project_status_open]} {
                        set folder_name [fs_get_folder_name $folder_id]
                        if {[string tolower $folder_name] eq [string tolower "Projektinfos extern"] || [string tolower $folder_name] eq [string tolower "Original"]} {
                            set valid_assignment_statuses_ids [list [translation_assignment_status_accepted] [translation_assignment_status_requested] \
                                [translation_assignment_status_created] [translation_assignment_status_in_progress] [translation_assignment_status_delivered] \
                                [translation_assignment_status_delivery_rejected] [translation_assignment_status_delivery_accepted]]
                            db_foreach project_assignments "select fa.assignment_id, fa.assignee_id, fa.assignment_status_id, \
                                p.project_nr, p.project_lead_id from \
                                im_freelance_assignments fa, im_freelance_packages fp, im_projects p \
                                where fp.freelance_package_id = fa.freelance_package_id and p.project_id = fp.project_id \ 
                                and fp.project_id = :folder_project_id and assignment_status_id in([template::util::tcl_to_sql_list $valid_assignment_statuses_ids])" {
                                if {$assignee_id ne ""} {
                                    set internal_company_name [im_name_from_id [im_company_internal]]
                                    set context_name $folder_name
                                    set salutation_pretty [ cog_salutation -person_id $assignee_id]
                                    set locale [lang::user::locale -user_id $assignee_id]
                                    set name [join [db_list file_names "select name from cr_items where item_id in ([template::util::tcl_to_sql_list $file_item_ids])"] ", "]
			                        set timestamp [lc_time_fmt [clock format [clock seconds] -format "%Y-%m-%d %H:%M:%S"] "%x %X"]
			                        set recipient [im_name_from_id $project_lead_id]
                                    set body "[lang::message::lookup $locale webix-portal.lt_file_uploaded_message_body]"
                                    set subject "[lang::message::lookup $locale webix-portal.lt_file_uploaded_message_subject]"
                                    intranet_chilkat::send_mail \
                                        -from_party_id $project_lead_id \
                                        -to_party_id $assignee_id \
                                        -subject $subject \
                                        -body $body
                                }
                            }
                        }
                    }
                }
            }
        }
		default {
			# Do nothing
		}
	}
}

ad_proc -public -callback cog_rest::invoice::linked_objects -impl kolibri {
	-invoice_id
	-cost_type_id 
} {
	Append the package to the purchase order

} {
	upvar linked_object_ids linked_object_ids
	if {$cost_type_id eq [im_cost_type_po]} {
		set package_id [db_string assignment "select max(freelance_package_id) 
			from im_freelance_assignments 
			where purchase_order_id = :invoice_id" -default ""]

		if {$package_id ne ""} {
			lappend linked_object_ids $package_id
		}
	}
}

ad_proc -public -callback im_invoice_before_update -impl kolibri_set_vars {
	{-object_id:required}
	{-status_id ""}
	{-type_id ""}
} {
	Set the invoice variables needed by Kolibri
} {

	# ------------------------------------------------------------------
	# We need the quote number, the quote date and the delivery date
	# ------------------------------------------------------------------

	# Upvar two levels as there is the callback level in between
	upvar 2 quote_no quote_no
	upvar 2 quote_date quote_date
	upvar 2 delivery2_date delivery2_date
	upvar 2 delivery_date end_date
	upvar 2 company_project_nr company_project_nr

	set project_id [db_string project_id "select project_id from im_costs where cost_id = :object_id" -default ""]
	if {"" != $project_id} {
			if {[db_0or1row quote_information "select company_project_nr,cost_name, effective_date, end_date from im_costs c, im_projects p where p.project_id = :project_id and p.project_id = c.project_id and cost_type_id = 3702 order by effective_date desc limit 1"]} {
				set quote_no $cost_name
				set quote_date [lc_time_fmt $effective_date %q]
				set delivery2_date [lc_time_fmt $end_date "%q"]
			}
	}
}

# Prozedur zum automatischen Hinzufügen einer Rabatt Zeile für die Differenz zwischen Trados Matrix und Gesamtzahl
# Überschreiben der Quote Generation, so dass die Einheiten sich auf die Wortzahl der Datei beziehen, nicht auf das von Trados ermittelte Ergebnis.
# Hoffentlich wird das nicht übel werden......

ad_proc -public -callback im_invoice_before_update -impl aaa_kolibri_offer {
    {-object_id:required}
    {-type_id ""}
} {
    If this is an offer, set the valid until date and get the version number of the offer
} {
	upvar 2 valid_until_pretty valid_until_pretty
	upvar 2 offer_name offer_name
	upvar 2 subject_area subject_area
	upvar 2 offer_nr offer_nr

    if {[im_category_is_a $type_id [im_cost_type_quote]]} {
		cog_log Debug "Executing im_invoice_before_update:: aaa_kolibri_offer"

		# Deal with the valid until
		set company_contact_id [db_string company_contact_id "select company_contact_id from im_invoices where invoice_id = :object_id" -default ""]
		if {$company_contact_id ne ""} {
			set locale [lang::user::locale -user_id $company_contact_id]
		} else {
			set locale [lang::system::locale]
		}
		
		set valid_until [db_string valid_until "select effective_date + interval '1 month' from im_costs where cost_id = :object_id" -default ""]
		set valid_until_pretty [lc_time_fmt $valid_until "%x" $locale]
		
		# Take a look at the version number
		
		db_1row invoice_info "select invoice_nr from im_invoices where invoice_id = :object_id"
		set invoice_item_id [content::item::get_id_by_name -name "${invoice_nr}.pdf" -parent_id $object_id]
		
		set offer_nr [db_string offers "select count(*) from cr_revisions where item_id = :invoice_item_id" -default 1]
		incr offer_nr
		
		# Set the title of the offer
		
		set project_exists_p [db_0or1row project_info "
			select * from acs_rels r,
				im_projects p
			where
				r.object_id_one = p.project_id
				and r.object_id_two = :object_id limit 1"]
	
		if {$project_exists_p} {
			set offer_name "${project_name}, [im_category_from_id $source_language_id] - "
			set target_langs [list]
			db_foreach target_lang "select im_category_from_id(language_id) as target_language from im_target_languages where project_id = :project_id order by im_category_from_id(language_id)" {
				lappend target_langs "$target_language"
			}
			append offer_name "[join $target_langs ", "]"
			
			# Dienstleistung (aus Material bzw. Projekt)
			set subject_area [im_category_from_id $subject_area_id]
		} else {
			set offer_name $invoice_nr
		}
    } else {
		set valid_until_pretty ""
		set offer_name ""
		set subject_area ""
		set offer_nr ""
	}
    return 1
}

ad_proc -public -callback intranet_collmex::error -impl notific_freelancer_contact {
    -object_id
    -message
} {
    Called when an error occured updating/ creating freelancer contacts
        
    @param object_id Object which resulted in the error
    @param message Message with the error 
} {
	set object_type [acs_object_type $object_id] 
	switch $object_type {
		im_company {
			set company_type_id [db_string company "select company_type_id from im_companies where company_id = :object_id" -default ""] 
            if {[lsearch [im_sub_categories [im_company_type_provider]] $company_type_id]>-1} {
				set freelancers_contact_email [parameter::get_from_package_key -package_key "webix-portal" -parameter "GeneralFreelancerContact" -default ""]
				intranet_chilkat::send_mail \
            		-to_addr $freelancers_contact_email \
            		-from_add [ad_system_owner] \
            		-subject "Collmex error for [im_name_from_id $object_id]." \
            		-body "There was a error in collmex, data is not transferred. Error Details:<br />$message" \
            		-no_callback
            }
		}
	}
} 

ad_proc -callback im_note_after_create -impl kolibri_notify {
	-object_id
	-status_id
	-type_id
} {
	Notify freelancer and pm about new comments

	@param object_id Note which was created
} {
	if {[db_0or1row context_info "select assignee_id, im_name_from_id(assignee_id) as assignee_name, assignment_status_id, assignment_name, creation_user, assignment_id,
		project_lead_id, p.project_id, p.processing_time as delivery_time, project_nr, n.note, freelance_package_name, project_name, note_id, p.project_status_id as project_status_id
	from im_freelance_assignments fa, im_notes n, acs_objects o, im_projects p, im_freelance_packages fp
	where n.object_id = fa.assignment_id and n.note_id = :object_id and o.object_id = n.note_id
	and p.project_id = fp.project_id and fp.freelance_package_id = fa.freelance_package_id"]} {
		if {$creation_user eq $assignee_id} {
			# Inform the PM
			set notification_id [webix::notification::create \
				-creation_user_id $assignee_id \
				-context_id $assignment_id \
				-project_id $project_id \
				-recipient_id $project_lead_id \
				-message "[_ intranet-cust-kolibri.lt_note_added]" \
				-notification_type_id [webix_notification_type_note_added]]

			webix::notification::action_create \
				-notification_id $notification_id \
				-action_type_id [webix_notification_action_type_view_comments] \
				-action_object_id $note_id \
				-action_help "[_ webix-portal.view_comment]" \
				-action_text $note
		} else {
            set valid_assignment_statuses_ids [list \
                [translation_assignment_status_accepted] [translation_assignment_status_in_progress] \
                [translation_assignment_status_delivered] [translation_assignment_status_delivery_rejected] \
                [translation_assignment_status_delivery_accepted] \
            ]
            if {$project_status_id eq [im_project_status_open] && [lsearch $valid_assignment_statuses_ids $assignment_status_id] > -1 } {
                set salutation_pretty [ cog_salutation -person_id $assignee_id]
                set timestamp [lc_time_fmt [clock format [clock seconds] -format "%Y-%m-%d %H:%M:%S"] "%x %X"]
                set pm_name [im_name_from_id $project_lead_id]
                set pm_email [party::email -party_id $project_lead_id]
                intranet_chilkat::send_mail \
                    -from_party_id $project_lead_id \
                    -to_party_id $assignee_id \
                    -subject "[_ intranet-cust-kolibri.lt_note_added_message_subject]" \
                    -body "[_ intranet-cust-kolibri.lt_note_added_message_body]"
            } 
		}
	}
}


ad_proc -public -callback cog_rest::object_custom_permissions -impl kolibri_additional_permission_check {
    -object_id:required
    -user_id:required
} {
    Called when an error occured updating/ creating freelancer contacts
        
    @param object_id Object which resulted in the error
    @param user_id User for which we check custom permissions
} {
    
    upvar privileges privileges 
    set object_type [acs_object_type $object_id]

    switch $object_type {
        "im_note" {
            # For Kolibri, we neeed additional check for im_note type
            set needed_privilege "write"
            # We do nothing if user already has 'admin' privilage
            if {[lsearch $privileges $needed_privilege] == -1} {
                set is_user_employee_p [im_user_is_employee_p $user_id]
                if {$is_user_employee_p} {
                    # User didn't have 'admin' privilege, but he is an employee so we modify value
                    lappend privileges $needed_privilege
                }
            } 
        }
    }

} 

ad_proc -public -callback cog::price::after_create -impl kolibri_update_price_permissions {
	-price_id:required
	{-company_id ""}
    {-material_id ""}
} {
	Gives permissions to edit/delete price for all PM's and all Employees
} {
        # PM's
        permission::grant -party_id [im_profile_project_managers] -object_id $price_id -privilege "read"
        permission::grant -party_id [im_profile_project_managers] -object_id $price_id -privilege "write"
        permission::grant -party_id [im_profile_project_managers] -object_id $price_id -privilege "admin"
        # Employees
        permission::grant -party_id [im_profile_employees] -object_id $price_id -privilege "read"
        permission::grant -party_id [im_profile_employees] -object_id $price_id -privilege "write"
        permission::grant -party_id [im_profile_employees] -object_id $price_id -privilege "admin"
}