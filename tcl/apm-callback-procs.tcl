
ad_library {
    APM callback procedures.

    @creation-date 2021-06-21
    @author malte.sussdorff@cognovis.de
}

namespace eval kolibri::apm {}

ad_proc -public kolibri::apm::after_upgrade {
    {-from_version_name:required}
    {-to_version_name:required}
} {
    After upgrade callback.
} {
    apm_upgrade_logic \
        -from_version_name $from_version_name \
        -to_version_name $to_version_name \
        -spec {
            5.0.0.1.3 5.0.0.1.4 {
                db_dml disable_trigger "alter table im_projects disable trigger all"
                db_foreach project "select project_id,project_nr, project_name from im_projects where project_name like project_nr || '_%'" {
                    set project_nr_length [string length $project_nr]
            		set project_name [string range $project_name [expr $project_nr_length +1] end]
                    if {$project_name eq ""} {
                        set project_name $project_nr
                    }
                    catch {db_dml update "update im_projects set project_name = :project_name where project_id = :project_id"}
                }
                db_foreach  projects "select project_id, project_nr, project_name from im_projects where project_name like project_nr || ' - %'" {
                    set project_nr_length [string length $project_nr]
                    set project_name [string range $project_name [expr $project_nr_length +3] end]
                    if {$project_name eq ""} {
                        set project_name $project_nr
                    }
                    catch {db_dml update "update im_projects set project_name = :project_name where project_id = :project_id"}
                } 
                db_foreach  projects "select project_id, project_name from im_projects where project_name like '- %'" {
                    set project_name [string range $project_name 2 end]
                    if {$project_name eq ""} {
                        set project_name $project_nr
                    }
                    catch {db_dml update "update im_projects set project_name = :project_name where project_id = :project_id"}
                } 
                db_dml enable_trigger "alter table im_projects enable trigger all"
            }
            5.0.0.1.4 5.0.0.1.5 {
                set project_ids [db_list projects "select project_id from im_projects where project_nr like '2021%' or project_status_id in (71,72,73,74,75,76) order by project_nr"]
                foreach project_id $project_ids {
                    kolibri::migrate::project_files -project_id $project_id
                }
            }
            5.0.0.1.5 5.0.0.1.6 {
                kolibri::migrate::screen_names 
                kolibri::migrate::import_user_portraits
            }
            5.0.0.1.8 5.0.0.1.9 {
                db_foreach comp "select company_id, company_type_id, company_status_id  from im_companies where vat_type_id is null" {
                    callback::im_company_after_update::impl::kolibri_update_vat_and_tax -object_id $company_id -type_id $company_type_id -status_id $company_status_id
                }

                db_foreach comp "select company_id, r.object_id_two as contact_id
                    from	acs_rels r, im_companies c
                    where	r.object_id_one=c.company_id and
                        r.rel_type = 'im_company_employee_rel' 
                                and c.primary_contact_id is null" {
                    db_dml update "update im_companies set primary_contact_id = :contact_id where company_id = :company_id"
                }
            }
            5.0.0.1.9 5.0.0.2.0 {
                kolibri::apm::remove_from_other_companies 
            }
            5.0.0.2.1 5.0.0.2.2 {
                # Recreate the PDFs in Buchhaltungs folder
                db_foreach invoice_info "select invoice_id, invoice_nr from im_invoices, im_costs  where invoice_id = cost_id and cost_type_id in (3700,3725,3740,3704,3735,3741) and effective_date >= to_date('2021-08-01','YYYY-MM-DD') and cost_status_id = [im_cost_status_outstanding]" {
	                set invoice_item_id [content::item::get_id_by_name -name "${invoice_nr}.pdf" -parent_id $invoice_id]
	                if {"" ne $invoice_item_id} {
                        set invoice_revision_id [content::item::get_latest_revision -item_id $invoice_item_id]
	                } else {
                        set invoice_revision_id [intranet_openoffice::invoice_pdf -invoice_id $invoice_id]
                    }
                    
                    set project_id ""
                    db_0or1row project_id "select to_char(effective_date,'MM') as month, to_char(effective_date,'YYYY') as year, project_id,cost_type_id, cost_status_id from im_costs where cost_id = :invoice_id"
                    if {$project_id ne ""} {
                        set project_dir [kolibri::project_path -project_id $project_id]
                        set pdf_path [content::revision::get_cr_file_path -revision_id $invoice_revision_id]
                        set buchhaltung_path "[acs_root_dir][parameter::get_from_package_key -package_key "intranet-cust-kolibri" -parameter "BuchhaltungFolder"]/$year"
                        if {![file exists "$buchhaltung_path"]} {
                            file mkdir "${buchhaltung_path}"
                            file mkdir "${buchhaltung_path}/Rechnungen"
                            file mkdir "${buchhaltung_path}/Gutschriften"
                        }

                        set cost_invoice_file ""
                        switch $cost_type_id {
                            3702 {
                                # Offer, folder already exists
                                set cost_invoice_file "${project_dir}/Angebot/${invoice_nr}.pdf"
                            }
                            3704 - 3735 - 3741 {                                
                                if {$cost_status_id eq [im_cost_status_outstanding]} {
                                    # Bill - Check folder first
                                    set month_path "${buchhaltung_path}/Gutschriften/$month"
                                    if {![file exists $month_path]} {
                                        file mkdir $month_path
                                    }
                                    set cost_invoice_file "${month_path}/${invoice_nr}.pdf"
                                }
                            }
                            3700 - 3725 - 3740 {
                                if {$cost_status_id eq [im_cost_status_outstanding]} {
                                    # Invoice
                                    set month_path "${buchhaltung_path}/Rechnungen/$month"
                                    if {![file exists $month_path]} {
                                        file mkdir $month_path
                                    }
                                    set cost_invoice_file "${month_path}/${invoice_nr}.pdf"
                                }
                            }
                            default {
                            }
                        }
                        cog_log Notice "Updating buchhaltung for $invoice_nr $cost_invoice_file $pdf_path"

                        if {$cost_invoice_file ne ""} {
                            file copy -force $pdf_path $cost_invoice_file
                        }
                    }
                }
            }
            5.0.0.3.1 5.0.0.3.2 {
                set report_id [cog::report::new -report_name "FreelancerDB" -report_sql "select p.party_id, to_char(max(effective_date),'YYYY-MM-DD') as last_bill_date,\
                    array_agg(distinct c.company_id) as company_ids, p.email, array_agg(distinct c.company_status_id) as company_status_ids, \
                    (select array_agg(skill_id) from im_freelance_skills fs where skill_type_id = 2000 and fs.user_id = p.party_id) as source_language_ids,\
                    (select array_agg(skill_id) from im_freelance_skills fs where skill_type_id = 2002 and fs.user_id = p.party_id) as target_language_ids,\
                    (select array_agg(skill_id) from im_freelance_skills fs where skill_type_id = 2014 and fs.user_id = p.party_id) as subject_area_ids,\
                    (select array_agg(skill_id) from im_freelance_skills fs where skill_type_id = 2016 and fs.user_id = p.party_id) as business_sector_ids,\
                    (select array_agg(skill_id) from im_freelance_skills fs where skill_type_id = 10000432 and fs.user_id = p.party_id) as role_ids,\
                    (select to_char(creation_date,'YYYY-MM-DD') from acs_objects o where o.object_id = p.party_id) as creation_date,\
                    (select to_char(kundenschutzvereinbarung,'YYYY-MM-DD') from persons pe where pe.person_id = p.party_id) as kunden_date,\
                    (select array_agg(note) from im_notes n where n.object_id = p.party_id) as notes\
                    from parties p, acs_rels r, im_companies c, im_costs co \
                    where p.party_id = r.object_id_two and c.company_id = r.object_id_one and co.cost_type_id = 3704 \
                    and co.provider_id = c.company_id and r.rel_type = 'im_company_employee_rel' group by p.party_id" -overwrite]

                cog::report::column::new -report_id $report_id -column_name "Freelancer" -column_type_id 15156 -variable_name party_id -sort_order 10
                cog::report::column::new -report_id $report_id -column_name "Email" -column_type_id 15151 -variable_name email -sort_order 20
                cog::report::column::new -report_id $report_id -column_name "Firma" -column_type_id 15163 -variable_name company_ids -sort_order 30
                cog::report::column::new -report_id $report_id -column_name "Status" -column_type_id 15160 -variable_name company_status_ids -sort_order 40
                cog::report::column::new -report_id $report_id -column_name "Ausgangssprache" -column_type_id 15160 -variable_name source_language_ids -sort_order 50
                cog::report::column::new -report_id $report_id -column_name "Zielsprache" -column_type_id 15160 -variable_name target_language_ids -sort_order 60
                cog::report::column::new -report_id $report_id -column_name "Textarten" -column_type_id 15160 -variable_name subject_area_ids -sort_order 70
                cog::report::column::new -report_id $report_id -column_name "Fachgebiete" -column_type_id 15160 -variable_name business_sector_ids -sort_order 80
                cog::report::column::new -report_id $report_id -column_name "Rollen" -column_type_id 15160 -variable_name role_ids -sort_order 90
                cog::report::column::new -report_id $report_id -column_name "Hinzugefügt am" -column_type_id 15153 -variable_name creation_date -sort_order 100
                cog::report::column::new -report_id $report_id -column_name "Rahmenvertrag" -column_type_id 15153 -variable_name kunden_date -sort_order 110
                cog::report::column::new -report_id $report_id -column_name "Letzte Gutschrift" -column_type_id 15153 -variable_name last_bill_date -sort_order 120
                cog::report::column::new -report_id $report_id -column_name "Notizen" -column_type_id 15161 -variable_name notes -sort_order 130  

            }
        }
}

ad_proc kolibri::apm::remove_from_other_companies {
} {
    Remove employees from other companies as primary contact or employee
} {
    db_foreach employee {select user_id, first_names, last_name from group_member_map g, cc_users u where 
            g.member_id = u.user_id 
            and group_id = 463
            and g.member_id not in (
			-- Exclude banned or deleted users
			select	m.member_id
			from	group_member_map m,
				membership_rels mr
			where	m.rel_id = mr.rel_id and
				m.group_id = acs__magic_object_id('registered_users') and
				m.container_id = m.group_id and
				mr.member_state != 'approved')
    } {
        db_dml companies "update im_companies set primary_contact_id = null where primary_contact_id = :user_id and company_name not like 'Kolibri%'"
        db_dml companies "update im_companies set accounting_contact_id = null where accounting_contact_id = :user_id and company_name not like 'Kolibri%'"
        set rels_to_remove [db_list employee_rels "select  r.rel_id
                from	    acs_rels r,
                        im_companies c
                where	r.object_id_one = c.company_id
                and     r.object_id_two = :user_id
                and     c.company_id != [im_company_internal]
                and     r.rel_type = 'im_company_employee_rel'"]
        foreach rel_id $rels_to_remove {
            ds_comment "$rel_id .. $user_id"
            catch {db_dml downgrade_from_employee "update acs_rels set rel_type = 'membership_rel' where rel_id = :rel_id"}
        }
    }
}

ad_proc -public kolibri::apm::enable_staging {
} {
    Code which needs to run when we import into staging
} {
    parameter::set_from_package_key -package_key "intranet-cust-kolibri" -parameter "ProviderBillCC" -value "A"
    parameter::set_from_package_key -package_key "intranet-trans-trados" -parameter "TradosProjectsWinPath" -value "A:\\Projekte"
    parameter::set_from_package_key -package_key "intranet-trans-trados" -parameter "TradosCompanyWinPath" -value "A:\\Terminologiemanagement"
    im_sysconfig_change_server -server_path /var/www/openacs  -server_url https://kolibri.erp4projects.de -system_owner m.lupo@kolbri.online -develop
}