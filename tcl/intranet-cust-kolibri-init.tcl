# packages/intranet-cust-kolibri/tcl/intranet-cust-kolibri-init.tcl

## Copyright (c) 2017, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
# 

ad_library {

    Initialization for intranet-cust-kolibr

}

# Schedule the updating of the sort order daily
if {[parameter::get_from_package_key -package_key "intranet-core" -parameter "TestDemoDevServer"] eq 0} {
    ad_schedule_proc -thread t -schedule_proc ns_schedule_daily [list 1 25] im_translation_update_language_sort_order
    ad_schedule_proc -thread t -schedule_proc ns_schedule_daily [list 0 55] kolibri_decline_quote
    ad_schedule_proc -thread t -schedule_proc ns_schedule_daily [list 1 45] kolibri_inactive_customers
}
