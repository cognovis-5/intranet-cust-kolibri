# packages/intranet-cust-kolibri/tcl/intranet-cust-kolibri-procs.tcl

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#

ad_library {
    
    Kolibri Custom Procs
    
    @author  (kolibri@ubuntu.localdomain)
    @creation-date 2011-10-07
    @cvs-id $Id$
}

# encoding system utf-8

ad_proc -public kolibri_update_project_cost_center {
    {-project_id:required}
    {-type_id ""}
} {
    Update the project cost center for a project
    
    @return cost_center_id the project was set to
} {
    # get the type
    if {$type_id eq ""} {
        set type_id [db_string type "select project_type_id from im_projects where project_id = :project_id"]
    }
    
    set cost_center_id [db_string cost_center "select aux_int2 as cost_center_id from im_categories where category_id = :type_id" -default ""]
    if {"" eq $cost_center_id} {
        # Backwards compatability
        switch $type_id {
            10000014 { set cost_center_id 140731 }
            10000011 { set cost_center_id 140733 }
            10000099 { set cost_center_id 140737 }
            10000097 { set cost_center_id 140725 }
            10000010 { set cost_center_id 140727 }
            10000124 { set cost_center_id 140723 }
            86 { set cost_center_id 140725 }
            10000007 { set cost_center_id 12388 }
            default { set cost_center_id 140729 }
        }
    }
    # Update the cost center
    db_dml update_cost_center "update im_projects set cost_center_id = :cost_center_id where project_id = :project_id"
    
    return $cost_center_id
}

ad_proc -public intranet_kolibri_send_provider_bills {
    {-start_date ""}
} {
    Sends out the provider bills from Purchase orders which are generated since the start_date
} {
    set bill_type_ids [im_sub_categories 3704]
    set bill_ids [db_list provider_bills "select cost_id from im_costs where effective_date >= to_date(:start_date,'YYYY-MM-DD') and cost_type_id in ([template::util::tcl_to_sql_list $bill_type_ids])"]
    foreach invoice_id $bill_ids {
        im_invoice_send_invoice_mail -from_addr ts@kolibri-kommunikation.com -cc_addr jm@kolibri-kommunikation.com -invoice_id $invoice_id
    }
}


ad_proc -public intranet_kolibri_generate_provider_bills {
    -end_date:required
    {-cost_status_id "3804"}
    -start_date:required
} {
    Generate the provider bills from Purchase orders which are generated until end_date
} {
    set po_type_ids [im_sub_categories [im_cost_type_po]]
    set po_ids [db_list purchase_orders "select cost_id from im_costs where cost_status_id = :cost_status_id and effective_date <= to_date(:end_date,'YYYY-MM-DD') and cost_type_id in ([template::util::tcl_to_sql_list $po_type_ids]) and effective_date >= to_date(:start_date,'YYYY-MM-DD')"]
    foreach cost_id $po_ids {
        set invoice_id [cog::invoice::copy_new -source_invoice_ids $cost_id -target_cost_type_id 3704]
	
        # Update the purchase order to status paid
        db_dml update_invoice "update im_costs set cost_status_id = [im_cost_status_filed] where cost_id = $cost_id"

		set cc_addr [parameter::get_from_package_key -package_key "intranet-cust-kolibri" -parameter "ProviderBillCC"]
		im_invoice_send_invoice_mail -from_addr $cc_addr -cc_addr $cc_addr -invoice_id $invoice_id
	
        # Update the provider bill to status outstanding
		cog_rest::put::invoice -invoice_id $invoice_id -rest_user_id [auth::get_user_id] -cost_status_id [im_cost_status_outstanding]
    }
}

ad_proc -public intranet_kolibri_cleanup_invoices {
} {
    Cleanup invoices and add company_contact_id
} {
    set invoice_ids [db_list invoices "select invoice_id from im_invoices where company_contact_id is null"]
    foreach invoice_id $invoice_ids {
		set cost_type_id [db_string cost_type_id "select cost_type_id from im_costs where cost_id = :invoice_id" -default 0]
		set invoice_or_quote_p [expr [im_category_is_a $cost_type_id [im_cost_type_invoice]] || [im_category_is_a $cost_type_id [im_cost_type_quote]] || [im_category_is_a $cost_type_id [im_cost_type_delivery_note]] || [im_category_is_a $cost_type_id [im_cost_type_interco_quote]] || [im_category_is_a $cost_type_id [im_cost_type_interco_invoice]]]
		
		if {$invoice_or_quote_p} {
		    # A Customer document
		    set customer_or_provider_join "ci.customer_id = c.company_id"
		} else {
		    # A provider document
		    set customer_or_provider_join "ci.provider_id = c.company_id"
		    
		    db_0or1row company_info "select primary_contact_id, accounting_contact_id from im_companies c, im_costs ci where ci.cost_id = :invoice_id and $customer_or_provider_join"
		    set company_contact_id $accounting_contact_id
		    if {"" == $company_contact_id} {
				set company_contact_id $primary_contact_id
		    }
		    if {"" != $company_contact_id} {
				ds_comment "updating $invoice_id with $company_contact_id"
				db_dml update_company_contact "update im_invoices set company_contact_id = :company_contact_id where invoice_id = :invoice_id"
		    } else {
				ds_comment "Cant update $invoice_id with a company_contact_id"
		    }
		}
    }
}

ad_proc -public intranet_kolibri_cleanup_customers {
} {
    Find all customers without a primary contact and assign the sole company contact to it, if there is one
} {
    set company_ids [db_list company "select company_id from im_companies where primary_contact_id is null"]
    foreach company_id $company_ids {
		set users [db_list user "
		select DISTINCT
		u.user_id
		from
		cc_users u,
		group_distinct_member_map m,
		acs_rels ur
		where u.member_state = 'approved'
		and u.user_id = m.member_id
		and m.group_id in ([im_customer_group_id], [im_freelance_group_id])
		and u.user_id = ur.object_id_two
		and ur.object_id_one = :company_id
		and ur.object_id_one != 0
		"]
		
		if {[llength $users] ==1} {
		    db_dml update_primary_contact "update im_companies set primary_contact_id = :users where company_id = :company_id"
		} else {
		    set company_name [db_string name "select company_name from im_companies where company_id = :company_id"]
			intranet_chilkat::send_mail \
	    		-from_addr [ad_admin_owner] \
				-to_addr [ad_admin_owner] \
				-subject "Missing primary contact for company_id $company_id" \
				-body "We could not find and assign a primary contact to the company <a href=[export_vars -base "[ad_url]/intranet/companies/view" -url {company_id}]>$company_name</a>. Please assign this company a contact person otherwise you wont be able to send any form of invoice"
		}
	}
}

ad_proc -public intranet_kolibri_created_bills {
    {-start_date:required}
} {
    Find out the bills which have a PDF generated for them and return one combined PDF with all of them
} {
    set bill_type_ids [im_sub_categories 3704]
    set bill_ids [db_list provider_bills "select cost_id from im_costs where effective_date >= to_date(:start_date,'YYYY-MM-DD') and cost_type_id in ([template::util::tcl_to_sql_list $bill_type_ids]) order by effective_date asc, provider_id"]

    set filenames [list]
    foreach invoice_id $bill_ids {
		# Find out if we have a PDF generated already
		set invoice_nr [db_string name "select invoice_nr from im_invoices where invoice_id = :invoice_id"]
		set item_id [content::item::get_id_by_name -name ${invoice_nr}.pdf -parent_id $invoice_id]
		if {"" != $item_id} {
		    set filename [fs::publish_object_to_file_system  -object_id $item_id -path /tmp]
		    lappend filenames $filename
		}
    }

    # Generate the pdf
    set pdf_info [intranet_oo::join_pdf -filenames $filenames -no_import]
    
    # Delete the original PDFs
    foreach filename $filenames {
		file delete $filename
    }
   
    ds_comment "$pdf_info"
    set outputheaders [ns_conn outputheaders]
    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=Bills.pdf"
    ns_returnfile 200 [lindex $pdf_info 0] [lindex $pdf_info 1]
}


ad_proc kolibri_trans_project_component {
    -project_id:required
    {-user_id ""}
    {-return_url ""}
    -sencha:boolean
} {
    Returns a formatted HTML table representing the status of the translation project
} {
    if {![im_project_has_type $project_id "Translation Project"]} { return "" }


    if {$user_id eq ""} {
        set user_id [auth::get_user_id]
    }

    im_project_permissions $user_id $project_id view read write admin
    if {!$write} { return "" }

    if {$return_url eq ""} {
    	set return_url [im_url_with_query]
    }
    
    set params [list \
            [list project_id $project_id] \
            [list user_id $user_id] \
            [list return_url $return_url]    
    ]

    set result ""
    if {[catch {} err_msg]} {
        set result "Error in Translation Project Wizard:<p><pre>$err_msg</pre>"
    }

    if {$sencha_p} {
	    set result [ad_parse_template -params $params "/packages/intranet-cust-kolibri/lib/trans-project-wizard"]
	} {
		set result [ad_parse_template -params $params "/packages/intranet-cust-kolibri/www/trans-project-wizard"]
	}

    return $result

}


ad_proc -public kolibri_bills_provider {
	{-effective_date ""}
} {
	Return the bill provider as CSV
} {
	if {"" eq $effective_date} {set effective_date "2015-01-01"}
	ds_comment "copy (select first_names, last_name, email, bills, avg_amount,amount from im_companies c, cc_users cu, (select round(sum(amount),0) as amount, round(sum(amount) / count(*),0) as avg_amount,count(*) as bills, provider_id from im_costs where cost_type_id = 3704 and effective_date >= :effective_date group by provider_id) s where s.provider_id = c.company_id and c.primary_contact_id = cu.user_id order by bills desc) to '/tmp/malte.csv' delimiter ';'
	;"
}


ad_proc kolibri_trans_task_new_status_helper {
	-task_status_id
	-task_type_id
} {
	Return the new status
} {
	set new_status_id $task_status_id

   	switch $task_status_id {
   		340 {
   			# Created: Maybe in the future there maybe a step between
   			# created and "for Trans", but today it's the same.
   			switch $task_type_id {
	   			88 {
	   				set new_status_id 348
	   			}
	   			95 {
	   				set new_status_id 352
	   			}
	   			default {
	   				set new_status_id 344
	   			}
  			}
   		}
   		342 { # for Trans:
   			set new_status_id 344
   		}
		344 { # Translating:
			if {$task_type_id == [im_project_type_trans]} {
				# we are done, because this task is translation only.
				set new_status_id 358
			} else {
				set new_status_id 345
			}
		}
		345 { # Translated - Waiting for PM to upload new version for editing
			set new_status_id 346
		}
   		346 { # for Edit:
   			set new_status_id 348
   		}
		348 { # Editing:
			if {$task_type_id == [im_project_type_edit] || $task_type_id == [im_project_type_trans_edit] || $task_type_id == [im_project_type_trans_spot]} {
			# we are done, because this task is only until editing
			# (spotcheck = short editing)
			set new_status_id 358
			} else {
			set new_status_id 350
			}
		}
   		350 { # for Proof:
   			set new_status_id 352
   		}
		352 { # Proofing:
			# All types are done when proofed.
			set new_status_id 358
		}
   	}
   	return $new_status_id
}

ad_proc -public -deprecated kolibri_cleanup_quotes {
	
} {
	Cleanup the quote status, set all open & delivered projects to a quote of status accepted.
	Set all declined project to a quote of status rejected
} {
	db_dml disable_trigger "alter table im_costs disable trigger im_costs_project_cache_up_tr"

	set acc_count 0
	# Get all projects with more than one quote.
	db_foreach projects "select p.project_id,project_status_id, count from (select count(*) as count, project_id from im_costs where cost_type_id = 3702 and cost_status_id not in (3814,3820,3818) group by project_id) s, im_projects p where s.project_id = p.project_id" {

		# First set all quotes to filed
		if {$count >1} {
			db_dml update_quotes "update im_costs set cost_status_id = [im_cost_status_filed] where project_id = :project_id"
			set cost_where "cost_id in (select max(cost_id) from im_costs where project_id = :project_id and cost_type_id = 3702)"
		} else {
			set cost_where "project_id = :project_id and cost_type_id = 3702"
		}
		

		# Now update it depending on the project status
		switch $project_status_id {
			83 - 77 - 82 {
		
					cog_log Debug "declined:: $count - $project_status_id :: $project_id"
						db_dml update_quotes "update im_costs set cost_status_id = 3818 where $cost_where"
			}
			81 - 78 - 79 - 76 - 380 {
				incr acc_count
						cog_log Debug "accepted:: $acc_count - $project_status_id :: $project_id"
				db_dml update_quotes "update im_costs set cost_status_id = 3820 where $cost_where"
			}
			default {
										cog_log Debug "created:: $count - $project_status_id :: $project_id"
				db_dml update_quotes "update im_costs set cost_status_id = 3802 where $cost_where"
			}
		}
	}

	db_dml enable_trigger "alter table im_costs enable trigger im_costs_project_cache_up_tr"
}


ad_proc -public kolibri_decline_quote {
	{-days_ago "90"}
} {
	Set all quotes to declined and the projects as well where the quote was generated days_ago
	
	This checks the modification date of the quote.

	@param days_ago Amount of days we need to look back in order to find OLDER quotes which need declining
} {
	
	# Find all not yet accepted quotes
	db_foreach open_quote "select c.cost_id,p.project_id, p.project_status_id, p.project_name, p.project_lead_id from im_costs c, im_projects p, acs_objects o where cost_type_id = 3702 and cost_status_id = 3802 and o.object_id = c.cost_id and o.last_modified < now() - interval '$days_ago days' and c.project_id = p.project_id" {
		
		# Check the project status_id
		switch $project_status_id {
			81 - 78 - 79 - 76 - 380 {
				# The project is accepted, accept the quote as well
				db_dml update_quotes "update im_costs set cost_status_id = 3820 where cost_id =:cost_id"
			}
			83 - 77 - 82 {
				# The project is declined, decline the quote without E-Mail or
				# Resetting the project
				db_dml update_quotes "update im_costs set cost_status_id = 3818 where cost_id = :cost_id"
			}
			default {
				# Potential quote.. Set declined
		
				# set the quote to declined
				db_dml decline_quote "update im_costs set cost_status_id = 3818 where cost_id = :cost_id"
				
				# set project to declined
				db_dml decline_project "update im_projects set project_status_id = 77 where project_id = :project_id"
				
				# Send a Mail to the pm
				
				set project_url [export_vars -base "[ad_url]/intranet/projects/view" -url {project_id}]
				intranet_chilkat::send_mail \
					-from_party_id $project_lead_id \
					-to_party_ids $project_lead_id \ 
					-subject "Project $project_name automatically declined" \
					-body "<html><body><a href='$project_url'>$project_name</a> was automatically declined as the quote is still open after $days_ago days"
			}
		}
	}
}

ad_proc -public kolibri_inactive_customers {
	{-days_ago "365"}
} {
	Set all customers which did not receive an offer within  the last 365 (default) days to "to contact".
} {
	# Exklude inaktive
	set exclude_status_ids [im_sub_categories [im_company_status_inactive]]
	lappend exclude_status_ids [im_company_status_potential]
	lappend exclude_status_ids [im_company_status_inquiries]
	lappend exclude_status_ids [im_company_status_qualifying]

	set company_type_ids [im_sub_categories [im_company_type_customer]]
	
	# Get all inactive customers
	set inactive_customer_ids [db_list inaktive "select company_id from im_companies where company_type_id in ([template::util::tcl_to_sql_list $company_type_ids]) and company_status_id not in ([template::util::tcl_to_sql_list $exclude_status_ids]) and company_id not in (select customer_id from im_costs c, acs_objects o where c.cost_id = o.object_id and o.last_modified < now() - interval '$days_ago' and cost_type_id in (3702, 3700))"]

	if {$inactive_customer_ids ne ""} {
		cog_log Notice "Inactive customers: $inactive_customer_ids"
		db_dml update_companies "update im_companies set company_status_id = 4800 where company_id in ([template::util::tcl_to_sql_list $inactive_customer_ids])"
	}
}

ad_proc -public kolibri_company_info_component {
	user_id
	return_url
} {
	returns company information
} {
    set company_ids [db_list primary_contact_company "select company_id from im_companies where primary_contact_id = :user_id"]
    if {[llength $company_ids]>1} {
	# We have more then one primary company
	set company_ids [db_list company_rels "select company_id from im_companies c, acs_rels r where r.object_id_one = c.company_id and r.object_id_two = :user_id"]
    }
    if {[llength $company_ids] >1} {
	set result "more than one company"
    } elseif {[llength $company_ids] == 0} {
	set result ""
    } else {
	set params [list [list base_url "intranet-core"] [list company_id [lindex $company_ids 0]] [list return_url $return_url]]
	set result [ad_parse_template -params $params "/packages/intranet-cust-kolibri/lib/company-info"]
    }
    return [string trim $result]
}

ad_proc -public kolibri_company_price_component {
	current_user_id
	freelancer_id
	return_url
} {
	returns company information
} {
    set company_ids [db_list primary_contact_company "select company_id from im_companies where primary_contact_id = :freelancer_id"]
    if {[llength $company_ids]>1} {
	# We have more then one primary company
	set company_ids [db_list company_rels "select company_id from im_companies c, acs_rels r where r.object_id_one = c.company_id and r.object_id_two = :freelancer_id"]
    }
    if {[llength $company_ids] >1} {
	set result "more than one company"
    } elseif {[llength $company_ids] == 0} {
	set result ""
    } else {
	set result [im_trans_price_component $current_user_id [lindex $company_ids 0] $return_url]
    }
    return [string trim $result]
}


ad_proc kolibri_material_name_pretty {
	-material_id:required
	{ -user_id "" }
} {
	Returns the Pretty Version of the material. Handles auto generated translation materials

	@param material_id Material we need the pretty name for
	@param user_id Which locale should we return
} {
	db_1row material_info "select * from im_materials where material_id = :material_id"
	if {$material_type_id eq [im_material_type_auto_gen]} {
		set source_language [im_category_from_id -current_user_id $user_id $source_language_id]
		set target_language [im_category_from_id -current_user_id $user_id $target_language_id]
								
		set trans_type [im_category_from_id -current_user_id $user_id $task_type_id]
	
		set material_name "$trans_type:"
		if {$source_language eq $target_language} {
			append material_name " $target_language"
		} else {
			append material_name " $source_language => $target_language"
		}
	}
	return $material_name
}

ad_proc kolibri_generate_buchhaltung {
	{-start_date ""}
} {
    Quick proc to generate buchhaltungs files after a certain date
} {
	if {$start_date eq ""} {
		set start_date "2021-01-01"
	}

    db_foreach invoice_info "select invoice_id, invoice_nr from im_invoices, im_costs  where invoice_id = cost_id and cost_type_id in (3700,3725,3704,3735) and effective_date >= to_date(:start_date,'YYYY-MM-DD')" {   
		set invoice_item_id [content::item::get_id_by_name -name "${invoice_nr}.pdf" -parent_id $invoice_id]

		if {"" ne $invoice_item_id} {
			kolibri::store_buchhaltung_pdf -invoice_id $invoice_id
		}
    }
}

ad_proc -public kolibri_company_info_page_component {
    company_id
    return_url
} {
    returns company information
} {
    set params [list [list base_url "intranet-core"] [list company_id $company_id] [list return_url $return_url]]
    set result [ad_parse_template -params $params "/packages/intranet-cust-kolibri/lib/company-info"]
    return [string trim $result]
}

ad_proc -public kolibri_trados_packages {
    -task_type
    -project_id
    -target_language_id
} {
    returns a list of list with all the package files found and the documents contained in them
} {
    
    im_trans_trados_project_info -project_id $project_id -array_name project_info
	set trados_dir $project_info(trados_dir)
    set packages_path "${trados_dir}/Packages/Out"
    set target_language [im_category_from_id -translate_p 0 $target_language_id] 
	set glob_filter "$project_info(project_nr)*${target_language}-*sdlppx"
	if {[catch {glob -directory $packages_path $glob_filter} trans_out_files]} {
		# Try wit the Master locale
		set main_language [lindex [split $target_language "-"] 0]
		set glob_filter "$project_info(project_nr)*\{${main_language},[string toupper $main_language]\}-*sdlppx"
		if {[catch {glob -directory $packages_path $glob_filter} trans_out_files]} {
			set trans_out_files [list]
		}
	}
	switch $task_type {
		trans {
			# Remove any KORR
			set out_files [list]
			foreach out_file $trans_out_files {
				if {![string match "*KORR*" $out_file]} {
					lappend out_files $out_file
				}
			}
		}
		proof {
			set out_files [list]
			foreach out_file $trans_out_files {
				if {[string match "*KORR*" $out_file]} {
					lappend out_files $out_file
				}
			}
		}
	    default {
			set glob_filter ""
			set out_files [list]
	    }
	}

    set package_info_list [list]

    foreach out_file $out_files {
        set task_ids [list]
		set messages [list]
		set files_in_package_list [split [exec -- /usr/bin/unzip -Z -1 "$out_file"] "\n"]
        foreach file $files_in_package_list {
            if {[string match "${target_language}/*.sdlxliff" $file]} {
                # Substract .sdlxliff at the end
                set source_file [string range [lindex [split $file "/"] 1] 0 end-9]
                set task_id [db_string task "select task_id from im_trans_tasks where task_filename=:source_file and project_id = :project_id and target_language_id = :target_language_id" -default ""]
                if {$task_id eq ""} {
                    lappend messages "Missing task for $source_file in $out_file in $project_info(project_nr)"
                } else {
                    lappend task_ids $task_id
                }
            }
        }
        lappend package_info_list [list $out_file $task_ids $messages]
    }
    return $package_info_list
}

ad_proc -public kolibri_cost_permissions {user_id cost_id view_var read_var write_var admin_var} {
    Fill the "by-reference" variables read, write and admin
    with the permissions of $user_id on $cost_id.<br>

    Cost permissions depend on cog::cost::permissions but overwrite with specific Kolibri permissions
} {
    upvar $view_var view
    upvar $read_var read
    upvar $write_var write
    upvar $admin_var admin

    cog::cost::permissions $user_id $cost_id view read write admin

	if {[im_user_is_employee_p $user_id]} {
		# Do not allow admin for anybody, even ]project-open[ admins
		set admin 0
		set accounting_contact_id [db_string accounting_contact "select accounting_contact_id from im_companies where company_id = [im_company_internal]" -default ""]

		# Check if we have a live revision for the PDF. This means it has been downloaded / send and write should be disabled.
		# Only for invoices & bills
		db_1row cost_type "select cost_type_id, cost_status_id from im_costs where cost_id = :cost_id"
		if {[im_cost_type_is_invoice_or_bill_p $cost_type_id]} {
			set write 0
			switch $cost_status_id {
				3802 {
					set write 1
					if {[im_user_is_pm_p $user_id]} {
						set admin 1
					}
				}
			}	
		} else {
			# po / quote

			switch $cost_type_id {
				3702 {
					# In case of quote
					set linked_document_cost_type_id [im_cost_type_invoice]
				}  
				3706 {
				# In case of PO
					set linked_document_cost_type_id [im_cost_type_bill]
				}
			}
			set linked_invoices_list [im_invoices::linked_invoices -invoice_id $cost_id -cost_type_id $linked_document_cost_type_id]
			if {[llength $linked_invoices_list] eq 0} {
				set write 1
				if {$cost_status_id eq 3802 && [im_user_is_pm_p $user_id]} {
					set admin 1
				}
			} else {
				set write 0
			}
		}
		# We allow to edit everything if current use is accounting_contact_id of internal company
		if {$accounting_contact_id eq $user_id} {
			set write 1
			set admin 1
		}	
	} else {
		set write 0
		set admin 0
	}
}

ad_proc -public kolibri_count_subject_usage {
	-start_date:required
} {
	Update the aux_num2 of the categories for 'Intranet Skill Business Sector' and 'Intranet Translation Subject Area' based of usage

	@param start_date Since when do we look for the amount of times it was used.
} {
	# First subject area.
	db_foreach subject_area { select subject_area_id, im_name_from_id(subject_area_id) as subject, count(subject_area_id) as count from im_projects where start_date > :start_date group by subject_area_id} {
		ds_comment "$subject -- $subject_area_id -- $count"
		db_dml update "update im_categories set aux_num2 = :count where category_id = :subject_area_id"
	}

	db_foreach business_sector {
		select skill_id, im_name_from_id(skill_id) as skill, count(skill_id) as count from im_object_freelance_skill_map fsm, im_projects p where p.project_id = fsm.object_id and start_date > :start_date and fsm.skill_type_id = 2024 group by skill_id
	} {
		ds_comment "$skill -- $skill_id -- $count"
		db_dml update "update im_categories set aux_num2 = :count where category_id = :skill_id"
	}

}

ad_proc -public kolibri_fix_pos {
	 
} {
	Fix problems with POS
} {
	db_foreach purchase_order {
		select cost_id, provider_id, default_tax, default_payment_method_id, co.payment_term_id, co.vat_type_id, i.company_contact_id,
		im_name_from_id(i.company_contact_id), im_name_from_id(cost_type_id), assignee_id, im_name_from_id (assignee_id), p.project_lead_id,
		im_name_from_id(project_lead_id)
		from im_costs c, im_companies co, im_invoices i, im_freelance_assignments fa, im_projects p
		where co.company_id = c.provider_id and  i.invoice_id = c.cost_id and 
		fa.purchase_order_id = c.cost_id and
		p.project_id = c.project_id and
		i.company_contact_id != fa.assignee_id and
		cost_type_id = 3706 and cost_id > 1412439
		order by cost_id asc
	} {
		ds_comment "$cost_id [im_name_from_id $provider_id] [im_name_from_id $vat_type_id]"
		set payment_days [db_string payment_days "select aux_int1 from im_categories where category_id = :payment_term_id" -default ""]
		set default_vat [db_string vat "select aux_int1 from im_categories where category_id = :vat_type_id" -default ""]
		set tax_format [im_l10n_sql_currency_format -style simple]
		db_dml update "update im_costs set vat_type_id = :vat_type_id, payment_term_id = :payment_term_id, 
			payment_days	= :payment_days,
			vat		= to_number(:default_vat,:tax_format),
			tax		= to_number(:default_tax,:tax_format)
		where cost_id = :cost_id"
		
		db_dml invoice "update im_invoices set payment_method_id = :default_payment_method_id, company_contact_id = :assignee_id where invoice_id = :cost_id"
		cog::invoice::send_email -invoice_id $cost_id -recipient_id $assignee_id -from_addr [party::email -party_id $project_lead_id] 

	}

	set assignment_ids [db_list ass "select assignment_id from im_freelance_assignments where assignment_status_id in (4222,4224,4225) and purchase_order_id is null"]
	foreach assignment_id $assignment_ids {
		db_1row ass "select end_date,assignment_status_id from im_freelance_assignments where assignment_id = :assignment_id"

		callback webix::assignment::trans_assignment_after_update -assignment_id $assignment_id -assignment_status_id 4222 -old_assignment_status_id 4221 -old_end_date $end_date -user_id [auth::get_user_id]
		if {$assignment_status_id eq 4224} {
			callback webix::assignment::trans_assignment_after_update -assignment_id $assignment_id -assignment_status_id 4224 -old_assignment_status_id 4222 -old_end_date $end_date -user_id [auth::get_user_id]
		}
		if {$assignment_status_id eq 4225} {
			callback webix::assignment::trans_assignment_after_update -assignment_id $assignment_id -assignment_status_id 4225 -old_assignment_status_id 4222 -old_end_date $end_date -user_id [auth::get_user_id]
		}
	}

	# Fix collmex
	set invoice_ids [db_list invoices "select cost_id from im_costs c, acs_objects o where o.creation_date > '2021-08-21' and c.cost_id = o.object_id and cost_type_id in (3700,3725,3740) and cost_status_id in (3804,3815,3816)"]
	foreach invoice_id $invoice_ids {
		cog_log Debug "Creating invoice in Collmex:: [intranet_collmex::update_customer_invoice -invoice_id $invoice_id]"
	}

	set bill_ids [db_list invoices "select cost_id from im_costs c, acs_objects o where o.creation_date > '2021-08-21' and c.cost_id = o.object_id and cost_type_id in (3704,3735,3741) and cost_status_id in (3804,3815,3816)"]
	foreach bill_id $bill_ids {
		cog_log Debug "Creating invoice in Collmex:: [intranet_collmex::update_provider_bill -invoice_id $bill_id]"
	}
}

namespace eval kolibri {
	ad_proc -public update_start_dates {
		-assignment_id:required
		{-user_id ""}
	} {
		Update the start dates of an assignment. Both it's own as well as the follow up ones

		@param assignment_id Assignment who's start_date I want to update
		@param user_id User doing the creation/update
	} {
		if {$user_id eq ""} {set user_id [auth::get_user_id]}
		
		db_1row current_assignment "select start_date, now() as current_date from im_freelance_assignments where assignment_id = :assignment_id"
		
		set relevant_status_ids [list 4220 4221 4222 4224 4225 4226 4229 4231]

		# Get the previous assignments and calculate the start date based of the end date plus 2 hours
		# If there are not previous assignments set the assignment start date to now().
		set previous_assignment_ids [webix::assignments::previous_assignments -assignment_id $assignment_id -assignment_status_ids $relevant_status_ids]
		if {$previous_assignment_ids eq ""} {
			if {$start_date eq ""} {
				cog_rest::put::translation_assignments -assignment_id $assignment_id -rest_user_id $user_id -start_date $current_date
			}
		} else {
			if {$start_date eq ""} {
				set start_date $current_date
			}
			set new_start_date [db_string end_dates_from_previous "select max(end_date) + interval '2 hours' from im_freelance_assignments where assignment_id 
				in ([template::util::tcl_to_sql_list $previous_assignment_ids])"]

			if {$start_date < $new_start_date} {
				cog_rest::put::translation_assignments -assignment_id $assignment_id -rest_user_id $user_id -start_date $new_start_date
			}
		}
	}

	ad_proc -public store_buchhaltung_pdf {
		{-invoice_id:required}
	} {
		Store the revision_id in the cost folder if this is not a preview
	} {
		set project_id ""
		db_0or1row project_id "select to_char(effective_date,'MM') as month, cost_name as invoice_nr, to_char(effective_date,'YYYY') as year, project_id,cost_type_id, cost_status_id from im_costs where cost_id = :invoice_id"

		if {$project_id ne ""} {
			set project_dir [kolibri::project_path -project_id $project_id]

			set buchhaltung_path "[acs_root_dir][parameter::get_from_package_key -package_key "intranet-cust-kolibri" -parameter "BuchhaltungFolder"]/$year"
			if {![file exists "$buchhaltung_path"]} {
				file mkdir "${buchhaltung_path}"
				file mkdir "${buchhaltung_path}/Rechnungen"
				file mkdir "${buchhaltung_path}/Gutschriften"
			}

			set cost_invoice_file ""
			switch $cost_type_id {
				3702 {
					# Offer, folder already exists
					set cost_invoice_file "${project_dir}/Angebote/${invoice_nr}.pdf"
				}
				3704 - 3735 - 3741 {
					if {$cost_status_id eq [im_cost_status_outstanding]} {
						# Bill - Check folder first
						set month_path "${buchhaltung_path}/Gutschriften/$month"
						if {![file exists $month_path]} {
							file mkdir $month_path
						}
						set cost_invoice_file "${month_path}/${invoice_nr}.pdf"
					}
				}
				3700 - 3725 - 3740 {
					if {$cost_status_id eq [im_cost_status_outstanding]} {
						# Invoice
						set month_path "${buchhaltung_path}/Rechnungen/$month"
						if {![file exists $month_path]} {
							file mkdir $month_path
						}
						set cost_invoice_file "${month_path}/${invoice_nr}.pdf"
					}
				}
				default {
				}
			}

			if {$cost_invoice_file ne ""} {
				set item_id [content::item::get_id_by_name -name ${invoice_nr}.pdf -parent_id $invoice_id]
				if {$item_id eq ""} {
					set invoice_revision_id [intranet_openoffice::invoice_pdf -invoice_id $invoice_id]
				} else {
					set invoice_revision_id [content::item::get_best_revision -item_id $item_id]
				}
				set pdf_path [content::revision::get_cr_file_path -revision_id $invoice_revision_id]
				file copy -force $pdf_path $cost_invoice_file
			}
		}
	}

	ad_proc -public close_assignments_again {

	} {
		Close assignments which should be closed and set those to work delivered which should have been delivered
	} {
		# Close assignments in closed projects
		db_dml update_in_closed_projects "update im_freelance_assignments set assignment_status_id = 4231 where assignment_status_id in (4221,4222,4224,4225,4220) and freelance_package_id in (
				select freelance_package_id from im_freelance_packages fp, im_projects p where p.project_id = fp.project_id and p.project_status_id in (77,78,79,81,82,83)
			)"
		
		# Set to work delivered if we have work delivered.
		db_dml update_assignments_with_files_uploaded "update im_freelance_assignments set assignment_status_id = 4225 where assignment_id in (
			select distinct assignment_id from im_freelance_assignments fa, cr_items where parent_id = assignment_id and assignment_status_id in (4222,4224) and end_date < now()
		)"

		# Update to closed if we have a bill
		db_dml update_in_closed_projects "update im_freelance_assignments set assignment_status_id = 4231 where assignment_id in (
		 	select assignment_id from im_costs c, im_freelance_assignments fa where fa.purchase_order_id = c.cost_id and assignment_status_id in (4224,4222) and cost_status_id in (3810,3814)
		)"
 
 		db_dml update_assignments_without_po "update im_freelance_assignments set assignment_status_id = 4223 where assignment_id in (select assignment_id from im_freelance_assignments where assignment_status_id in (4224,4222) and purchase_order_id is null)"

		set purchase_order_ids [db_list pos "select purchase_order_id from im_freelance_assignments where assignment_status_id in (4224,4222)"]
		foreach purchase_order_id $purchase_order_ids {
			set linked_invoices_list [im_invoices::linked_invoices -invoice_id $purchase_order_id -cost_type_id [im_cost_type_bill]]
			if {[llength $linked_invoices_list] >0} {
				db_dml update_assignments_with_bill "update im_freelance_assignments set assignment_status_id = 4231 where purchase_order_id = :purchase_order_id"
			}
		}

		set open_assignment_ids [db_list open_assignments "select assignment_id from im_freelance_assignments where assignment_status_id in (4222,4224)"]
		foreach assignment_id $open_assignment_ids {
			set next_assignment_ids [webix::assignments::next_assignments -assignment_id $assignment_id]
			if {$next_assignment_ids ne ""} {
				set package_ids [db_list package_ids "select freelance_package_id from im_freelance_assignments where assignment_id in ([template::util::tcl_to_sql_list $next_assignment_ids])"]
				set parent_ids [concat $package_ids $next_assignment_ids]
				set file_ids [db_list files "select item_id from cr_items where parent_id in ([template::util::tcl_to_sql_list $parent_ids])"]
				if {$file_ids ne ""} {
					db_dml update_assignments_with_files "update im_freelance_assignments set assignment_status_id = 4225 where assignment_id = :assignment_id"
				}
			}
		}
	}

    ad_proc -public get_vat_type_based_on_country {
        {-type_id:required}
        {-country_code:required}
	} {
		Close assignments which should be closed and set those to work delivered which should have been delivered
	} {
		switch $country_code {
			de {
				# this is a german company
				set provider_p [im_category_is_a $type_id [im_company_type_provider]]
				if {$provider_p} {
					# Differentiate between large and small
					if {$type_id eq 10000301} {
						set vat 0
						set vat_type 42001
					} else {
						set vat 19
						set vat_type 42000
					}
				} else {
					set vat 19
					set vat_type 42050
				}
			}
			be - bg - cz - dk - ee - ie - el - es - fr - gr - hr - it - cy - lv - lt - lu - hu - mt - nl - at - pl - pt - ro - si - sk - fi - se  {
				# European
				set vat 0
				set provider_p [im_category_is_a $type_id [im_company_type_provider]]
				if {$provider_p} {
					# Enforce the provide type
					set company_type_id [im_company_type_provider]
					set vat_type 42010
				} else {
					set vat_type 42030
				}
			}
			default {
				# ROW
				set vat 0
				set provider_p [im_category_is_a $type_id [im_company_type_provider]]
				if {$provider_p} {
					set company_type_id [im_company_type_provider]
					set vat_type 42020
				} else {
					set vat_type 42040
				}
			}
		}
    }
}

