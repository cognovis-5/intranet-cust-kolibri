
ad_library {
	Rest Procedures for the custom kolibri package
	
	@author malte.sussdorff@cognovis.de
}

namespace eval cog_rest::json_object {
    ad_proc kolibri_url {} {
        @return url string URL to access the T: drive
        @return type string Type of URL we provide
    } - 

    ad_proc kolibri_package {} {
        @return freelance_package object im_freelance_package Package into which we imported the file
        @return trados_filename string name of the file which we imported into the package
        @return success boolean Was the import successful
        @return task_type category "Intranet Trans Task Type" Task Type to return
        @return message string Error message if the import was not successful. Otherwise might contain additional useful information
    } -
}

ad_proc -public cog_rest::get::kolibri_t_urls {
    -project_id:required
} {
    Returns the urls to the T:\ drive for a project

    @param project_id object im_project::read Project ID for which we want to return the url
    
    @return urls json_array kolibri_url URLs for accessing Kolibri systems (T:\ drive)
} {
    set urls [list]

    set drive [parameter::get_from_package_key -package_key "intranet-cust-kolibri" -parameter "LocalDrive"]

    set customer_path_unix "[acs_root_dir][parameter::get_from_package_key -package_key "intranet-cust-kolibri" -parameter "CustomerFolder"]"
    set project_path [kolibri::project_path -project_id $project_id]
    regexp "${customer_path_unix}/(.*)" $project_path match project_folder

    set type "project"
    set url [kolibri::local_path -project_id $project_id -type $type]
    lappend urls [cog_rest::json_object]
    set type "sdlproj"
    set url [kolibri::local_path -project_id $project_id -type $type]
    lappend urls [cog_rest::json_object]
    set type "company"
    set url [kolibri::local_path -project_id $project_id -type $type]
    lappend urls [cog_rest::json_object]
    set type "final_company"
    set url [kolibri::local_path -project_id $project_id -type $type]
    lappend urls [cog_rest::json_object]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::kolibri_tm_tool_url {
    -project_id:required
    -tm_tool_id:required
} {
    Returns the URL to open the project in the TM Tool - Might just provide the download of a project folder for this project

    @param project_id object im_project::read Project for which we want the TM Tool link
    @param tm_tool_id category "Intranet TM Tool" TM Tool for which we want to get the link

    @return tm_tools json_object tm_tool Information about the TM Tool
} {
    set errors [list]
    set download_only false
    set url ""

    set project_type_id [db_string project_type "select project_type_id from im_projects where project_id = :project_id" -default ""]
    
    if {[lsearch [im_sub_categories [im_project_type_translation]] $project_type_id]<0} {
        set err_msg "[im_name_from_id $project_id] is not a translation project."
        set parameter "[im_name_from_id $tm_tool_id] - $tm_tool_id"
        set object_id $project_id
        lappend errors [cog_rest::json_object -object_class "error"]	
    } else {

        switch $tm_tool_id {
            10000534 - 10000535 - 10000763 - 10000379 {
                # Trados

		set project_dir [kolibri::project_path -project_id $project_id]
		if {![file exists "${project_dir}/Trados"]} {
		    kolibri::setup_trados_directory -project_id $project_id
		}
		
                if {[im_trans_trados_project_p -project_id $project_id]} {
                    set url [kolibri::local_path -project_id $project_id -type "sdlproj"]
                } else {
                    set err_msg "$project_id is not a Trados project. Can't help"
                    set parameter "[im_name_from_id $tm_tool_id] - $tm_tool_id"
                    set object_id $project_id
                    lappend errors [cog_rest::json_object -object_class "error"]	
                }
            }
            10000308 {
                # MemoQ
                if {[im_trans_memoq_configured_p]} {
                    set memoq_guid [db_string memoq "select memoq_guid from im_projects where project_id = :project_id" -default ""] 
                    if {$memoq_guid eq ""} {
                        set memoq_guid [im_trans_memoq_create_project -project_id $project_id]
                        im_trans_memoq_file_upload_source_files -project_id $project_id
                        im_trans_memoq_project_analysis -project_id $project_id
                    } 
                    set memoq_server [parameter::get -package_id [im_package_trans_memoq_id] -parameter "MemoQServer"]
                    set memoq_port [parameter::get -package_id [im_package_trans_memoq_id] -parameter "MemoQPort"]
                    set url [export_vars -base "${memoq_server}:${memoq_port}/memoq/pm/projectoverview/index/$memoq_guid" -url {{nocache True}}]
                } else {
                    set err_msg "MemoQ is not configured, can't help you"
                    set parameter "[im_name_from_id $tm_tool_id] - $tm_tool_id"
                    set object_id $project_id
                    lappend errors [cog_rest::json_object -object_class "error"]	
                }
            }
            default {
                set err_msg "We don't support your tm tool [im_name_from_id $tm_tool_id] yet"
                set parameter "[im_name_from_id $tm_tool_id] - $tm_tool_id"
                set object_id $project_id
                lappend errors [cog_rest::json_object -object_class "error"]	
            }
        }
    }
    set tm_tools [cog_rest::json_object]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::update_packages {
    -project_id:required
    -rest_user_id:required
} {
    Return the list of freelance packages which could be used to update

    @param project_id object im_project::read Project ID for which we want to import the packages
    @return freelance_packages json_array kolibri_package Freelance packages which were updated with files.
    
} {
    db_1row project_info "select to_char(start_date,'YYYY-MM-DD HH24:MI') as project_start_date, project_name, project_nr, project_status_id,company_id, subject_area_id,source_language_id,project_lead_id from im_projects where project_id = :project_id"

    set target_language_ids [list]
    set task_type_ids [list]
    set freelance_packages [list]

    db_foreach task_names {
        select aux_string1, target_language_id
        from im_trans_tasks, im_categories
        where project_id = :project_id
        and category_id = task_type_id
    } {
        if {[lsearch $target_language_ids $target_language_id]<0} {lappend target_language_ids $target_language_id}

        # Add the task type ids
        foreach task_type $aux_string1 {
            set task_type_id [db_string type_id "select category_id from im_categories where lower(category) = :task_type and category_type = 'Intranet Trans Task Type'" -default ""]
            if {$task_type_id ne ""} {
                if {[lsearch $task_type_ids $task_type_id]<0} {lappend task_type_ids $task_type_id}
            }
        }
    }

    #---------------------------------------------------------------
    # Find the files and prepare any errors
    #---------------------------------------------------------------
    
    foreach target_language_id $target_language_ids {
        foreach task_type_id $task_type_ids {
            set task_type [im_category_from_id -translate_p 0 $task_type_id]            
            set package_files [kolibri_trados_packages -project_id $project_id -target_language_id $target_language_id -task_type $task_type] 
           	foreach package_file $package_files {
		        set file_path [lindex $package_file 0]
		        set file_task_ids [lindex $package_file 1]
                set messages [lindex $package_file 2]
                set trados_filename [file tail $file_path]
                set freelance_package_id ""
                set success 1
                set message [join $messages " <br \>"]
                lappend freelance_packages [cog_rest::json_object]
            }
        }
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::update_packages {
    -project_id:required
    -rest_user_id:required
} {
    Import the packages from the T: drive into ]project-open[

    @param project_id object im_project::read Project ID for which we want to import the packages
    @return freelance_packages json_array kolibri_package Freelance packages which were updated with files.
} {
    db_1row project_info "select to_char(start_date,'YYYY-MM-DD HH24:MI') as project_start_date, project_name, project_nr, project_status_id,company_id, subject_area_id,source_language_id,project_lead_id from im_projects where project_id = :project_id"

    set target_language_ids [list]
    set task_type_ids [list]
    set freelance_packages [list]

    db_foreach task_names {
        select aux_string1, target_language_id
        from im_trans_tasks, im_categories
        where project_id = :project_id
        and category_id = task_type_id
    } {
        if {[lsearch $target_language_ids $target_language_id]<0} {lappend target_language_ids $target_language_id}

        # Add the task type ids
        foreach task_type $aux_string1 {
            set task_type_id [db_string type_id "select category_id from im_categories where lower(category) = :task_type and category_type = 'Intranet Trans Task Type'" -default ""]
            if {$task_type_id ne ""} {
                if {[lsearch $task_type_ids $task_type_id]<0} {lappend task_type_ids $task_type_id}
            }
        }
    }

    #---------------------------------------------------------------
    # Find the files and prepare any errors
    #---------------------------------------------------------------
    
    foreach target_language_id $target_language_ids {
        foreach task_type_id $task_type_ids {
            set task_type [im_category_from_id -translate_p 0 $task_type_id]            
            set package_files [kolibri_trados_packages -project_id $project_id -target_language_id $target_language_id -task_type $task_type] 
           	foreach package_file $package_files {
		        set file_path [lindex $package_file 0]
		        set file_task_ids [lindex $package_file 1]
                set messages [lindex $package_file 2]
                set trados_filename [file tail $file_path]
                set freelance_package_id ""
                set success 0

    	    	if {$file_task_ids ne ""} {
                    # Find the correct freelance_package_id by looping through all the tasks
                    set freelance_package_ids [db_list packages "select distinct fp.freelance_package_id from im_freelance_packages_trans_tasks fptt, im_freelance_packages fp, im_freelance_assignments fa
                        where trans_task_id in ([template::util::tcl_to_sql_list $file_task_ids])
                        and fp.freelance_package_id = fptt.freelance_package_id
                        and fp.package_type_id = $task_type_id
                        and fa.freelance_package_id = fp.freelance_package_id"]

                    if {[llength $freelance_package_ids] eq 1} {
                        set freelance_package_id [lindex $freelance_package_ids 0]
                        set matched_package_path($freelance_package_id) $file_path
            			db_1row package_info "select freelance_package_name from im_freelance_packages fp
			                where freelance_package_id = :freelance_package_id"

                        set delivered_p [db_string assignment_delivered "select 1 from im_freelance_assignments where freelance_package_id = :freelance_package_id and assignment_status_id in (4225,4226,4227,4231)" -default 0]
                        if $delivered_p {
                            lappend messages "Can't import as the assignment has already been delivered"
                        } else {
                            set freelance_package_file_name "${freelance_package_name}"
			                append freelance_package_file_name [file extension $file_path]

                            set file_id [ content::item::get_id_by_name -name $freelance_package_file_name -parent_id $freelance_package_id]
                            if {$file_id ne ""} {
                                # Check the revision date if we should update
                                set old_revision_id [content::item::get_best_revision -item_id $file_id]
                                set mtime [ns_fmttime [file mtime $file_path] "%Y-%m-%d %H:%M"]
                                set import_p [db_string import {
                                    SELECT 1 from acs_objects o
                                    WHERE o.last_modified < :mtime
                                    AND o.object_id = :old_revision_id
                                } -default 0]
                            } else {
                                set import_p 1
                            }

                            if {$import_p} {
                                set revision_id [cog::file::import_fs_file -context_id $freelance_package_id -file_path $file_path -filename $freelance_package_file_name -user_id $rest_user_id -description "Automatic import from Trados package directory in T:"]
                                set success 1
                                lappend messages "Matched succesfully and imported revision $revision_id"
                            }
                        }
                    } else {
                        if {[llength $freelance_package_ids] eq 0} {
                            lappend messages "Could not find a project-open package for the file. Maybe you should create the batches first?"
                        } else {
                            lappend messages "The files seem to match multiple assignments. Please split the trados package according to the batches."
                        }
                    }
                } else {
                    lappend messages "Could not find a project-open package for the file. Maybe you should create the batches first?"
                }

                set message [join $messages " <br \>"]
                lappend freelance_packages [cog_rest::json_object]

                if {$success} {
                    # notify freelancers.
                    db_foreach assignment {
                        select assignee_id, assignment_id from im_freelance_assignments 
                        where freelance_package_id = :freelance_package_id
                        and assignment_status_id in (4222,4224,4229)
                    } {
        		     	set salutation_pretty [im_invoice_salutation -person_id $assignee_id]
		        		set locale [lang::user::locale -user_id $assignee_id]
                        set location [util_current_location]
		                set assignments_link "${location}/#!/assignment-details?assignment_id=$assignment_id" 
            			set internal_company_name [im_name_from_id [im_company_freelance]]

						# Inform about a new file
			           	set subject "[lang::message::lookup $locale intranet-cust-kolibri.lt_package_updated_message_subject]"
			           	set body "[lang::message::lookup $locale intranet-cust-kolibri.lt_package_updated_message_body]"


			            set signature [db_string signature "select signature from parties where party_id = :project_lead_id" -default ""]
			            if {$signature ne ""} {
			            	append body [template::util::richtext::get_property html_value $signature]
			            }

						intranet_chilkat::send_mail \
							-to_party_ids $assignee_id \
							-from_party_id $project_lead_id \
							-subject $subject \
							-body $body \
					        -object_id $assignment_id
						
						im_freelance_notify \
							-object_id $assignment_id \
							-recipient_ids $assignee_id
                    }
                }
            }
        }
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::send_provider_bills {
    -project_id:required
    -sender_id:required
    -rest_user_id:required
} {
    sends out the provider bills for created purchase orders as well as those
    manually created which are still in status created.

    @param project_id object im_project::read Project ID for which we want to import the packages
    @param sender_id object person::read Who is sending the E-Mails
    @return bills json_array invoice Array of newly send out bills
} {
    #---------------------------------------------------------------
    # Create requested provider bills
    #---------------------------------------------------------------
        
    #  find the purchase orders for the project and update them
    set cost_types [im_sub_categories [im_cost_type_po]]
    set purchase_order_ids [db_list purchase_orders "select distinct cost_id from im_costs c, acs_rels ar
            where c.cost_id = ar.object_id_two and ar.object_id_one = :project_id
            and cost_type_id in ([template::util::tcl_to_sql_list $cost_types]) and cost_status_id = [im_cost_status_accepted]"]

    set provider_ids [list]
    
    # Create only one provider bill for all the purchase orders in this project for this provider
    foreach cost_id $purchase_order_ids {
        db_1row cost_status_id "select cost_status_id,provider_id from im_costs where cost_id = :cost_id"
        if {[im_cost_status_accepted] == $cost_status_id} {
            cog_rest::put::invoice -invoice_id $cost_id -rest_user_id $rest_user_id -cost_status_id [im_cost_status_filed]
            if {[info exists source_arr($provider_id)]} {
                lappend source_arr($provider_id) $cost_id
            } else {
                set source_arr($provider_id) [list $cost_id]
            }
            if {[lsearch $provider_ids $provider_id]<0} {
                lappend provider_ids $provider_id
            }
        }
    }
    
    foreach provider_id $provider_ids {
        set invoice_id [cog::invoice::copy_new -source_invoice_ids [set source_arr($provider_id)] -target_cost_type_id [im_cost_type_bill] -no_callback]
        cog::callback::invoke -object_type "im_invoice" -object_id $invoice_id -action after_create -status_id [im_cost_status_created] -type_id [im_cost_type_bill]
        db_dml update_company "update im_companies set company_status_id = [im_company_status_active] where company_id = :provider_id"
    }

    #  find the bills and mails them
    set bill_ids [db_list quote "select cost_id from im_costs c
        where cost_type_id = [im_cost_type_bill] 
        and cost_status_id = [im_cost_status_created] 
        and project_id = :project_id"]

    set err_msg_list [list]
    foreach bill_id $bill_ids {
        set cc_addr [parameter::get_from_package_key -package_key "intranet-cust-kolibri" -parameter "ProviderBillCC"]

        if {[cog::invoice::send_email -from_addr [party::email -party_id $sender_id] -cc_addr $cc_addr -invoice_id $bill_id] eq ""} {
            lappend err_msg_list "Could not send document for [im_name_from_id $bill_id] using [im_name_from_id $sender_id]"
        }
    }

    if {[llength $err_msg_list]>0} {
        cog_rest::error -http_status 400 -message "[join $err_msg_list "<br/>"]"
    } else {
        if {$bill_ids ne ""} {
            set bills [cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::invoice -invoice_ids $bill_ids -rest_user_id $rest_user_id]]
        } else {
            set bills ""
        }
    }    
    return [cog_rest::json_response]
}

