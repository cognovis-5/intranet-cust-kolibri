# /packages/intranet-sencha-tables-rest-procs

ad_library {
	Rest Procedures for the custom kolibri reporting
	
	@author malte.sussdorff@cognovis.de
}

namespace eval cog_rest::json_object {
    ad_proc kolibri_report_fin_doc {} {
        @return document_nr string Financial document number
        @return cost_center string Cost Center for the financial document
		@return cost_type string Type of financial document
		@return cost_status string Status of the financial document
		@return company string Customer/Provider for the financial document
		@return due_date date Date when the invoice is due
		@return effective_date date Daten when the invoice was issued
		@return total_amount number Total amount of the invoice
		@return company_collmex_id number ID of the company in collmex
		@return net_amount number Net Amount of the invoice
		@return vat number VAT Percentage of the financial document
		@return tax_classification string Type of tax applied to the financial document
		@return vat_number string International VAT number of the customer/provider
    } - 
}

ad_proc -public cog_rest::get::kolibri_report_fin_docs {
	-rest_user_id:required
	{-start_date ""}
	{-cost_type_ids ""}
	{-end_date ""}
} {
	Returns the financial documents for a period of time

	@param cost_type_ids category_array "Intranet Cost Type" List of cost types we want to export
	@param start_date date Which is the first date we look at for the invoice creation - defaults to Beginning of last year
	@param end_date date Which is the last date we look at for invoice creation - defaults to now

	@return fin_docs json_array kolibri_report_fin_doc Financial documents created in the period
} {
	set fin_docs [list]
	if {$start_date eq ""} {
		set start_year [expr [clock format [clock seconds] -format "%Y"] -1]
		set start_date "${start_year}-01-01"
	}
	
	set where_clause_list [list "effective_date >= '$start_date'"]
	if {$end_date eq ""} {
		set end_date [clock format [clock seconds] -format "%Y-%m-%d"]
	}
	lappend where_clause_list "effective_date <= '$end_date'"
	
	set customer_cost_type_ids [list]
	set provider_cost_type_ids [list]
	foreach cost_type_id [im_sub_categories $cost_type_ids] {
		if {[im_cost_type_is_invoice_or_quote_p $cost_type_id]} {
			lappend customer_cost_type_ids $cost_type_id
		} else {
			lappend provider_cost_type_ids $cost_type_id
		}
	}


	set not_in_cost_status_id [im_sub_categories [im_cost_status_deleted]]
	lappend not_in_cost_status_id [im_cost_status_rejected]
	lappend where_clause_list "c.cost_status_id not in ([template::util::tcl_to_sql_list $not_in_cost_status_id]) "

	set sql_start "select cost_name as document_nr, im_name_from_id(cost_center_id) as cost_center,
		im_name_from_id(cost_type_id) as cost_type, cost_type_id, im_name_from_id(cost_status_id) as cost_status,
		c.customer_id, c.provider_id, 
		im_name_from_id(coalesce(c.vat_type_id,co.vat_type_id)) as tax_classification, 
		to_char(c.effective_date, 'YYYY-MM-DD') as effective_date,
		to_date(to_char(c.effective_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') + c.payment_days as due_date,
		vat,
		coalesce(round(c.amount * c.vat / 100 * 100) / 100,0) as vat_amount,
        coalesce(round(c.amount *100/100),0) as net_amount,
		co.collmex_id as company_collmex_id, co.vat_number, co.company_name as company
		from im_costs c, im_companies co"

	# Get customer_invoices
	set customer_where_clause_list $where_clause_list
	if {$customer_cost_type_ids eq ""} {
		set customer_cost_type_ids [im_sub_categories 3708]
	}
	lappend customer_where_clause_list "cost_type_id in ([template::util::tcl_to_sql_list $customer_cost_type_ids])"
	lappend customer_where_clause_list "c.customer_id = co.company_id"
	set company_sql "$sql_start
		where [join $customer_where_clause_list " and "]"

	db_foreach cost_document $company_sql {
		set total_amount [expr $vat_amount + $net_amount]
		lappend fin_docs [cog_rest::json_object]
		cog_log Notice "Company... $company"
	}

	set provider_where_clause_list $where_clause_list
	if {$provider_cost_type_ids eq ""} {
		set provider_cost_type_ids [im_sub_categories 3710]
	}
	lappend provider_where_clause_list "cost_type_id in ([template::util::tcl_to_sql_list $provider_cost_type_ids])"
	lappend provider_where_clause_list "c.provider_id = co.company_id"

	set provider_sql "$sql_start
		where [join $provider_where_clause_list " and "]"

	db_foreach cost_document $provider_sql {
		set total_amount [expr $vat_amount + $net_amount]
		if {[string match "Freelance*" $company]} {
			set company [lrange [split $company " "] 1 end]
		}

cog_log Notice "Provider... $company"
		lappend fin_docs [cog_rest::json_object]
	}	

	return [cog_rest::json_response]

}

ad_proc -public im_rest_get_custom_kolibri_reporting_freelancer_ready_state {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler for getting information on active freelancers and the status of the information needed.

	Returns a JSON with 
	- Freelancer name (and ID)
	- Year of last provider bill
	- Comments from notes company & freelancer (one field)
	- Status (we limit to potential and active)
	- Date of Kundenschutz
	- Date of Diploma
	- Contact Information (Street, PLZ, City, Country) complete (Boolean)
	- Bank Account Information complete (Boolean)
} {

	set permission_p [im_permission $rest_user_id "view_companies_all"]
	if {!$permission_p} {return}


	array set query_hash $query_hash_pairs
	if {$rest_user_id eq "0"} {set rest_user_id [ad_conn user_id]}
	
	# Get locate for translation
	set locale [lang::user::locale -user_id $rest_user_id]

	set company_type_ids [im_sub_categories [im_company_type_provider]]
	set company_status_ids [im_sub_categories 41]
	lappend company_status_ids 46
	
	# Get an array of last bills for providers
	set bill_dates_key_values [db_list_of_lists bill_dates "select provider_id, to_char(max(effective_date),'YYYY') from im_costs co where co.cost_type_id = [im_cost_type_bill] group by co.provider_id"]
	template::util::list_of_lists_to_array $bill_dates_key_values last_provider_bill
		
	set price_list_provider_ids [db_list providers_with_price_list "select distinct c.company_id
		from im_companies c, im_trans_prices t 
		where t.company_id = c.company_id 
		and company_type_id in ([template::util::tcl_to_sql_list $company_type_ids])
		and company_status_id in ([template::util::tcl_to_sql_list $company_status_ids])		 
	"]
	
	set sql "
		select im_name_from_id(primary_contact_id) as freelancer_name,
		c.company_id, 
		company_status_id,
		primary_contact_id as freelancer_id,
		company_name,
		im_name_from_id(company_status_id) as company_status,
		kundenschutzvereinbarung,
		qualification_confirmation,
		bank_account_nr,
		bank_routing_nr,
		bank_name,
		iban,
		bic,
		paypal_email,
		skrill_email,
		address_line1,
		address_postal_code,
		address_country_code,
		address_city
		from im_companies c 
			left outer join persons p on (c.primary_contact_id = p.person_id)
			left outer join im_offices o on (c.main_office_id = o.office_id)
		 where company_type_id in ([template::util::tcl_to_sql_list $company_type_ids])
		 and company_status_id in ([template::util::tcl_to_sql_list $company_status_ids])		 
	"
	
	set valid_vars [list freelancer_name company_id company_name company_status_id company_status kva_p quali_p bank_p address_p last_bill freelancer_id price_list_p notes kundenschutzvereinbarung address_country_code]
	set obj_ctr 0
	
	db_foreach objects $sql {
		if {[info exists last_provider_bill($company_id)]} {
			set last_bill $last_provider_bill($company_id)
		} else {
			set last_bill ""
		}
		
		if {$address_line1 ne "" && $address_postal_code ne "" && $address_country_code ne "" && $address_city ne ""} {
			set address_p 1			
		} else {
			set address_p 0
		}
		
		if {[lsearch $price_list_provider_ids $company_id]<0} {
			set price_list_p 0
		} else {
			set price_list_p 1
		}
		
		# Notes
		set notes ""
		set note_object_ids [list $company_id]
		if {$freelancer_id ne ""} {lappend note_object_ids $freelancer_id}
		db_foreach notes "select note from im_notes where object_id in ([template::util::tcl_to_sql_list $note_object_ids])" {
			append notes "[template::util::richtext::get_property html_value $note] <br />"
		}

		if {$kundenschutzvereinbarung eq ""} {set kva_p 0} else {set kva_p 1}
		if {$qualification_confirmation eq ""} {set quali_p 0} else {set quali_p 1}
		if {$bank_account_nr ne "" && $bank_routing_nr ne ""} {set bank_p 1} else {set bank_p 0}
		if {$iban ne "" && $bic ne ""} {set bank_p 1}
		if {$paypal_email ne "" || $skrill_email ne ""} {set bank_p 1}
		
		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}
		append result "$komma{\"id\": \"$company_id\", \"object_name\": \"[im_quotejson $company_name]\"$dereferenced_result}" 
		incr obj_ctr
	}
	
	set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"kolibri_reporting_freelancer_ready_state: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}

