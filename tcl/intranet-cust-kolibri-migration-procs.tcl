namespace eval kolibri::migrate {
    ad_proc -public nov22 {
    } {
		Upgrade the active customers to one drive
    } {
		set customer_type_ids [im_sub_categories [im_company_type_customer]]
		set active_or_potential_ids [im_sub_categories 40]
		set active_customer_ids [db_list customers "select distinct company_id from (select company_id, company_path from im_companies where company_type_id in ([ns_dbquotelist $customer_type_ids]) and company_status_id in ([ns_dbquotelist $active_or_potential_ids]) order by company_path) c"]
		foreach customer_id $active_customer_ids {
			cog_log Notice "Migrating customer [im_name_from_id $customer_id]"
			kolibri::migrate::onedrive_customer -company_id $customer_id
		}

		set active_final_customers [db_list final_company_id "select distinct final_company_id from (select distinct final_company_id, company_path from im_projects p, im_companies c where c.company_id = p.final_company_id and c.company_type_id = 10247 and company_status_id in ([ns_dbquotelist $active_or_potential_ids]) order by company_path) cp"]
		foreach final_company_id $active_final_customers {
			cog_log Notice "Migrating final customer $final_company_id [im_name_from_id $final_company_id]"
			kolibri::migrate::onedrive_final_customer -final_company_id $final_company_id
		}

		set projects_to_migrate [db_list projects "select project_id from im_projects where project_status_id in (71,72,73,74,75,76,380) and project_type_id not in (100,101) order by project_nr"]
		foreach project_id $projects_to_migrate {
			cog_log Notice "Migrating project [im_name_from_id $project_id] ($project_id)"
			kolibri::migrate::onedrive_project -project_id $project_id
		}
    }
    
    ad_proc -public onedrive_customer {
		-company_id:required
	} {
        Migration November 2022

		Migrates a company with it's folders from the project directory and Terminologiemanagement over to the new company folder in one drive. Excludes PROJECTS.
	} {
		set new_path [kolibri::customer_path -company_id $company_id]
		
		if {$new_path ne ""} {
			# Delete old files
			if {[file exists ${new_path}/TM]} {
			    exec rm -rf ${new_path}/TM/
			    exec rm -rf ${new_path}/TB/
				cog_log Warning "Deleteing old TM/TB at $new_path"
			}

    	    set base_path_unix [parameter::get -package_id [im_package_filestorage_id] -parameter "ProjectBasePathUnix" -default "/tmp/projects"]
		    set company_project_path "${base_path_unix}/[db_string get_customer_path "select company_path from im_companies where company_id=:company_id"]"
		    if {![file exists $company_project_path]} {
				cog_log Warning "Could not find $company_project_path - not migrating any subfolders"
				return ""
		    }
		    cog_log Notice "Migration company_project_path $company_project_path"
		    set files [glob -dir $company_project_path -type d -tails -nocomplain *]

			foreach dir $files {
				# Exclude any project_paths
				if {![string match "20*" $dir]} {
				    im_filestorage_rsync -source_path "$company_project_path/$dir" -target_path "$new_path/$dir"
				}
			}

            # Terminologie Management.
		    set old_terminologie_path [im_trans_trados_folder -company_id $company_id]
		    if {[::fileutil::find $old_terminologie_path is_trados_file] ne ""} {
			
				set new_terminologie_path [kolibri::terminologie_path -company_id $company_id]
				set files [glob -dir $old_terminologie_path -type d -tails -nocomplain *]
				foreach dir $files {
					im_filestorage_rsync -source_path "$old_terminologie_path/$dir" -target_path "$new_terminologie_path/$dir"
				}
		    }
		}
	}

    ad_proc -public onedrive_final_customer {
		-final_company_id:required
	} {
        Migration November 2022

		Migrates a final company with it's Terminologiemanagement folder over to each new company folder in one drive it participated in a project.
        So if a final company had been used in three customers, the TM folder is migrated over to each of these final customers
	} {
        set old_terminologie_path [im_trans_trados_folder -company_id $final_company_id]
        if {[file exists $old_terminologie_path]} {
            set company_ids [db_list company_ids "select distinct company_id from im_projects where final_company_id = :final_company_id"]
            foreach company_id $company_ids {
                set new_terminologie_path [kolibri::terminologie_path -company_id $company_id -final_company_id $final_company_id]
				cog_log Notice "Migrating from $old_terminologie_path to $new_terminologie_path for $company_id"
                if {$new_terminologie_path ne ""} {
                    set files [glob -dir $old_terminologie_path -type d -tails -nocomplain *]
                    foreach dir $files {
                        im_filestorage_rsync -source_path "$old_terminologie_path/$dir" -target_path "$new_terminologie_path/$dir"
                    }
                }
            }
        }
	}

	ad_proc -public onedrive_project {
		-project_id:required
	} {
        Migration November 2022

		Migrates a project into the new company folder on one drive. This will keep the project structure and NOT create / keep the new project structure.

	} {
		set old_path [cog::project::filestorage_project_path -project_id $project_id]
		# Localize the workflow stage directories
		set query "
			select
				p.project_type_id,
				p.project_nr,
				p.company_id,
                p.final_company_id
			from
				im_projects p
			where
				p.project_id = :project_id
			"
	
		if { ![db_0or1row projects_info_query $query] } {
			return "Can't find the project with group id of $project_id"
		}

		if {$final_company_id eq ""} {
			set customer_project_path "[kolibri::customer_path -company_id $company_id]/Projekte"
		} else {
			set customer_project_path "[kolibri::final_customer_path -final_company_id $final_company_id -company_id $company_id]/Projekte"
		}

		set project_dir "${customer_project_path}/$project_nr"
		if {[file exists $project_dir]} {
		    exec rm -rf $project_dir
		}

        set success [im_filestorage_mkdir -dir $project_dir]
        if {$success ne 1} {
            cog_log Error $success
            return ""
        }

		im_filestorage_rsync -source_path "$old_path" -target_path "$project_dir"

		return $project_dir
	}

    ad_proc -deprecated import_user_portraits {
    } {
        Import the user portraits into the content repository
    } {
        set user_ids [db_list users {select distinct user_id from users u where 
                u.user_id not in (
                -- Exclude banned or deleted users
                select	m.member_id
                from	group_member_map m,
                    membership_rels mr
                where	m.rel_id = mr.rel_id and
                    m.group_id = acs__magic_object_id('registered_users') and
                    m.container_id = m.group_id and
                    mr.member_state != 'approved')}]
        
        foreach user_id $user_ids {
            # Get the current user id to not show the current user's portrait
            set base_path "/var/lib/aolserver/projop/filestorage/users/$user_id"
            set base_paths [split $base_path "/"]
            set base_paths_len [llength $base_paths]

            if { [catch {
                file mkdir $base_path
                im_exec chmod ug+w $base_path
                set file_list [im_exec find "$base_path/" -maxdepth 1 -type f]
            } err_msg] } {
                # Probably some permission errors - return empty string
                set file_list ""
            }

            set files [lsort [split $file_list "\n"]]
            set portrait_path ""
            foreach file $files {
                set file_paths [split $file "/"]
                set file_paths_len [llength $file_paths]
                set rel_path_list [lrange $file_paths $base_paths_len $file_paths_len]
                set rel_path [join $rel_path_list "/"]
                if {[regexp "^portrait\....\$" $rel_path match]} {  
                    set portrait_path "${base_path}/$rel_path"               
                }
            }

            if {$portrait_path ne ""} {
                set file_revision_id [cog::file::import_fs_file -context_id $user_id -user_id $user_id -file_path $portrait_path]
                set file_item_id [content::revision::item_id -revision_id $file_revision_id]
                cog_rest::post::relationship -object_id_one $user_id -object_id_two $file_item_id -rel_type "user_portrait_rel" -rest_user_id $user_id
                cog_log Notice "Imported $portrait_path into $file_item_id"
            }
        }
    }
    ad_proc -deprecated screen_names {
    } {
        Upgrade the screen names for employees
    } {
        db_foreach employee {select user_id, first_names, last_name from group_member_map g, cc_users u where 
                g.member_id = u.user_id 
                and group_id = 463
                and g.member_id not in (
                -- Exclude banned or deleted users
                select	m.member_id
                from	group_member_map m,
                    membership_rels mr
                where	m.rel_id = mr.rel_id and
                    m.group_id = acs__magic_object_id('registered_users') and
                    m.container_id = m.group_id and
                    mr.member_state != 'approved')
        } {
            set screenname "[string range $first_names 0 1][string range $last_name 0 1]"
            db_dml update_screen_name "update users set screen_name = :screenname where user_id = :user_id"
        }
    }

    ad_proc -deprecated project_files {
        -project_id:required
    } {
        Upgrade the project files if not already done so and import them from the T: drive
        into the content repository.
    } {
        # Create cr_folders (including default permissions)
        kolibri_create_folders -project_id $project_id
            
        db_1row project_info "
            select
            p.project_nr,
            p.project_path,
            p.project_name,
            c.company_path
        from
            im_projects p,
            im_companies c
        where
            p.project_id=:project_id
            and p.company_id=c.company_id"

        set project_path "/var/lib/aolserver/projop/filestorage/projects/${company_path}/$project_nr"

        cog_log Notice "====== Upgrading $project_nr ======="
        # Load Original into CR folder
        set path "${project_path}/Original"
        foreach file [glob -nocomplain -path ${path}/ *] {
            cog_log Notice "Original $file - [cog::file::import_fs_file -context_id $project_id -folder "Original" -file_path $file]"
        }

        set path "${project_path}/Projektinfos"
        foreach file [glob -nocomplain -path ${path}/ *] {
            cog_log Notice "Projectinfos: $file - [cog::file::import_fs_file -context_id $project_id -folder "Projektinfos" -file_path $file]"
        }

        set path "${project_path}/Projektinfos extern"
        foreach file [glob -nocomplain -path ${path}/ *] {
            cog_log Notice "PInfo Extern: $file [cog::file::import_fs_file -context_id $project_id -folder "Projektinfos extern" -file_path $file]"
        }

        set path "${project_path}/Lieferung"
        foreach file [glob -nocomplain -path ${path}/ *] {
            cog_log Notice "Lieferung: $file [cog::file::import_fs_file -context_id $project_id -folder "Lieferung" -file_path $file]"
        }
        
        set freelance_package_ids [db_list freelance_package_ids "select freelance_package_id from im_freelance_packages where project_id = :project_id"]
        set project_folder_id [intranet_fs::get_project_folder_id -project_id $project_id]

        foreach freelance_package_id $freelance_package_ids {
            # Load out packages into the im_freelance_package
            set out_files [db_list out_files "select file_path from im_freelance_package_files where freelance_package_id = :freelance_package_id and package_file_type_id in (606,608,611,600)"]
            foreach file $out_files {
                if {[file exists $file]} {
                    cog_log Notice "Package: $file [cog::file::import_fs_file -context_id $freelance_package_id -file_path $file]"
                }
            }
            # Load in packages into im_freelance_assignments
            set in_files [db_list out_files "select file_path from im_freelance_package_files where freelance_package_id = :freelance_package_id and package_file_type_id in (607,609,601)"]

            db_foreach assignment "select assignment_id, assignee_id from im_freelance_assignments where freelance_package_id = :freelance_package_id and assignment_status_id in (4221,4222,4224,4225)" {
                foreach file $in_files {
                    if {[file exists $file]} {
                        cog_log Notice "AssignmentFile: $file [cog::file::import_fs_file -context_id $assignment_id -file_path $file]"
                    }
                    cog_log Notice "Granting permission for $assignee_id on $project_id for $assignment_id"
                    permission::grant -party_id $assignee_id -object_id $assignment_id -privilege "read"
                    permission::grant -party_id $assignee_id -object_id $freelance_package_id -privilege "read"
                }

                # Permissions for freelancers on folders
                set folder_ids [db_list folders "select item_id from cr_items where parent_id = :project_folder_id and name in ('original','projektinfos-extern', 'projektinfos')"]
                foreach folder_id $folder_ids {
                    permission::grant -party_id $assignee_id -object_id $folder_id -privilege read
                }
            }
        }
    }


	
}

ad_proc -deprecated kolibri_migrate_final_company {
} {
	Migrate the final company. Needs to be run once after an update. Should probabyl be put into update procs....
} {
	
	# First create all the final companies based on final_company field.
	set final_company_list [db_list final_company "select distinct final_company from im_projects where final_company is not null and final_company not in ('BBDO','Sabro','B+S Card Service')"]
	
	foreach company_name $final_company_list {
		if {[catch {im_company::new -company_name $company_name -company_type_id 10247}]} {
			ds_comment " $company_name FAILED"
		}
	}
	
	# Now link them correctly
	db_dml disable_trigger "ALTER TABLE im_projects DISABLE TRIGGER im_projects_project_cache_up_tr;"
	db_foreach project_updates {select project_id,p.final_company,c.company_id from im_projects p, im_companies c where p.final_company is not null and p.final_company = c.company_name} {
		db_dml update "update im_projects set final_company_id = :company_id where project_id = :project_id"
		ds_comment "$project_id :: $final_company :: $company_id"
	}
	db_dml disable_trigger "ALTER TABLE im_projects ENABLE TRIGGER im_projects_project_cache_up_tr;"

}


ad_proc -deprecated kolibri_migrate_provider_prices {
} {
	Migrate providers prices
} {
		
	# Get the list of most often used languages
	db_foreach target_langs "
		select  distinct target_language_id, parent_id
		from	im_trans_prices left outer join im_category_hierarchy ch on (child_id = target_language_id)
		where target_language_id is not null
	" {
		if {![exists_and_not_null parent_id]} {
			set sub_lang_ids [im_sub_categories $target_language_id]
			set language_id ""
			db_0or1row max_used "select count(language_id) as count_lang, language_id from im_target_languages where language_id in ([template::util::tcl_to_sql_list $sub_lang_ids]) group by language_id order by count_lang desc limit 1"
			
			if {$target_language_id ne $language_id && $language_id ne ""} {
				ds_comment "Target: [im_category_from_id $target_language_id] :: [im_category_from_id $language_id]"
				db_dml update "update im_trans_prices set target_language_id = :language_id where target_language_id = :target_language_id"
			}
		}
	}
	
	# Get the list of most often used languages
	db_foreach target_langs "
		select  distinct source_language_id, parent_id
		from	im_trans_prices left outer join im_category_hierarchy ch on (child_id = source_language_id)
		where source_language_id is not null
	" {

		if {![exists_and_not_null parent_id]} {
			set sub_lang_ids [im_sub_categories $source_language_id]
			set language_id ""
			db_0or1row max_used "select count(language_id) as count_lang, language_id from im_target_languages where language_id in ([template::util::tcl_to_sql_list $sub_lang_ids]) group by language_id order by count_lang desc limit 1"
			if {$source_language_id ne $language_id} {
				db_dml update "update im_trans_prices set source_language_id = :language_id where source_language_id = :source_language_id"
			}
		}
	}
	
	# get the list of providers with prices
	set provider_ids [db_list provicers "select distinct tp.company_id as provider_id from im_trans_prices tp, im_companies c where c.company_id = tp.company_id and c.company_type_id not in (53,57) order by tp.company_id"]

	foreach provider_id $provider_ids {
		# Loop through each price. If the price is already present for the language combination,
		# then put it into the price array
		
		set keys [list]
		db_foreach prices "select count(price) as price_count,price,source_language_id, target_language_id, uom_id, task_type_id from im_trans_prices where company_id = :provider_id and uom_id != [im_uom_unit] and source_language_id is not null and target_language_id is not null group by price, source_language_id, target_language_id, uom_id, task_type_id order by price_count desc" {
			
			set key "$provider_id-${source_language_id}-${target_language_id}-${task_type_id}-$uom_id"
			if {![info exists price_arr($key)]} {
				lappend keys $key
				set price_arr($key) $price
				set source_lang_arr($key) $source_language_id
				set target_lang_arr($key) $target_language_id
				set uom_arr($key) $uom_id
				set task_type_arr($key) $task_type_id
			}
		}

		# Clean up
 		db_dml delete "delete from im_trans_prices where company_id = :provider_id"
		
		# Get the most of
		foreach key $keys {
			
			# Get the variables
			set source_language_id $source_lang_arr($key)
			set target_language_id $target_lang_arr($key)
			set task_type_id $task_type_arr($key)
			set uom_id $uom_arr($key)
			set current_price $price_arr($key)

			cog_log Debug "Provider:: $provider_id ... $source_language_id => $target_language_id :: $task_type_id ... $uom_id for $current_price"
			if {$current_price ne ""} {
				set price_id [db_nextval "im_trans_prices_seq"]
				db_dml price_insert "insert into im_trans_prices (price_id,
							uom_id,
							company_id,
							task_type_id,
							target_language_id,
							source_language_id,
							currency,
							price
						) values ($price_id,
							:uom_id,
							:provider_id,
							:task_type_id,
							:target_language_id,
							:source_language_id,
							'EUR',
							:current_price
						)"
			}
		}
		
		# Delete trans / proof where source = target_language
		db_dml delete_same_lang "delete from im_trans_prices where source_language_id = target_language_id and company_id = :provider_id and task_type_id in (93,95,86,87)"
	}
	
	# Get rid of none (299,10000403)
	db_dml update_tasks "update im_trans_tasks set source_language_id = target_language_id where source_language_id in (299,10000403)"
	
	db_dml update_tasks "update im_trans_tasks set source_language_id = 282 where source_language_id in (299,10000403)"
	db_dml update_tasks "update im_trans_tasks set target_language_id = 282 where target_language_id in (299,10000403)"
	
	set project_ids [db_list projects "select project_id from im_projects where source_language_id in (299,10000403) and project_id is not null"]
	foreach project_id $project_ids {
		set target_language_id [db_string source "select distinct source_language_id from im_trans_tasks where project_id = :project_id and source_language_id = target_language_id limit 1" -default ""]
		if {$target_language_id eq ""} {
			set target_language_id [db_string target "select language_id from im_target_languages where project_id = :project_id limit 1"]
			ds_comment "move project:: $project_id"
		} else {
			ds_comment "Move to $target_language_id"
		}
		db_dml update_projects "update im_projects set source_language_id = :target_language_id where project_id = :project_id"
	}
	
	# Set reminder to german
	db_dml update_projects "update im_projects set source_language_id = 282 where source_language_id in (299,10000403)"
	
	# Remove skills
	db_dml remove_skills "delete from im_freelance_skills where skill_id in (299,10000403)"
	db_dml materials "update im_materials set source_language_id = target_language_id where source_language_id in (299,10000403)"
	db_dml mnater "update im_materials set source_language_id = 282 where source_language_id in (299,10000403)"
	db_dml materials "update im_materials set target_language_id = 282 where target_language_id in (299,10000403)"
		
	# Delete skills, as the target skill is there
	db_dml delete_skill "delete from im_object_freelance_skill_map where skill_id in (299,10000403)"
	db_dml delete_skill "delete from im_target_languages where language_id in (299,10000403)"
	
	set mapping_ids [db_list mappings "select mapping_id from im_trans_main_freelancer where source_language_id in (299,10000403)"]
	foreach mapping_id $mapping_ids {
		catch {db_dml update_main_freelancers "update im_trans_main_freelancer set source_language_id = target_language_id where mapping_id = :mapping_id"}

	}
	
	db_dml delete_dup_mappings "delete from im_trans_main_freelancer where source_language_id in (299,10000403)"
	
	if {[im_table_exists "im_trans_price_history"]} {
		db_dml update_history "update im_trans_price_history set source_language_id = target_language_id where source_language_id in (299,10000403)"
		db_dml delete_history "delete from im_trans_price_history where source_language_id in (299,10000403)"
		db_dml delete_history "delete from im_trans_price_history where target_language_id in (299,10000403)"
	}
	
	db_dml del "delete from im_categories where category_id = 10000403"
	db_dml del "delete from im_categories where category_id = 299"
	
}
