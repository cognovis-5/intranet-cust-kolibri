-- 
-- packages/intranet-cust-kolibri/sql/postgresql/upgrade/upgrade-4.1.0.0.1-4.1.0.0.2.sql
-- 
-- Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- @author Malte Sussdorff (malte.sussdorff@cognovis.de)
-- @creation-date 2012-04-07
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/intranet-cust-kolibri/sql/postgresql/upgrade/upgrade-4.1.0.0.2-4.1.0.0.3.sql','');

select acs_privilege__create_privilege('view_templates','View Templates','View Templates');
select acs_privilege__add_child('admin', 'view_templates');

select acs_privilege__create_privilege('add_templates','Add Templates','Add Templates');
select acs_privilege__add_child('admin', 'add_templates');
select im_priv_create('view_templates','Accounting');
select im_priv_create('view_templates','P/O Admins');
select im_priv_create('add_templates','Accounting');
select im_priv_create('add_templates','Senior Managers');
