SELECT acs_log__debug('/packages/intranet-cust-kolibri/sql/postgresql/upgrade/upgrade-5.0.0.0.0-5.0.0.0.1.sql','');

update im_menus set sort_order = 115 where label = 'freelancer_manager_app';
update im_menus set sort_order = 415 where label = 'rfq';
update im_menus set sort_order = 1610 where label = 'Dataprivacy';
update im_menus set sort_order = 1615 where label = 'Datenschutz';

update im_component_plugins set component_tcl = 'im_timesheet_task_list_component -max_entries_per_page 20 -view_name im_timesheet_task_list_short -restrict_to_mine_p mine -restrict_to_status_id 76 -order_by "end_date"' where plugin_id = 83784;

update im_component_plugins set component_tcl = 'cog_project_finance_component -user_id $user_id -project_id $project_id' where plugin_id = 10813;
update im_component_plugins set component_tcl = 'cog_project_finance_component -show_details_p 0 -show_summary_p 1 -project_id $project_id -no_timesheet_p 1 -show_admin_links_p 0' where plugin_id = 1047044;
update im_component_plugins set component_tcl = 'cog_project_finance_component -user_id $user_id -project_id $project_id -show_details_p 1 -show_summary_p 1 -show_admin_links_p 0 -no_timesheet_p 1' where plugin_id = 965617;