--
-- packages/intranet-cust-kolibri/sql/postgresql/upgrade/upgrade-4.1.0.1.0-4.1.0.1.1.sql
--
-- Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- @author Malte Sussdorff (malte.sussdorff@cognovis.de)
-- @creation-date 2012-04-07
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/intranet-cust-kolibri/sql/postgresql/upgrade/upgrade-4.1.0.1.0-4.1.0.1.1.sql','');

-- Update Trans_id and support Trans+Proof Process

-- alter table im_trans_tasks add column dummy_id integer;
-- alter table im_trans_tasks add column dummy_date timestamptz;
-- update im_trans_tasks set dummy_id = proof_id where proof_id is not null;
-- update im_trans_tasks set proof_id = edit_id where proof_id is not null;
-- update im_trans_tasks set edit_id = dummy_id where dummy_id is not null;
-- update im_trans_tasks set dummy_date = proof_end_date where proof_end_date is not null;
-- update im_trans_tasks set proof_end_date = edit_end_date where proof_end_date is not null;
-- update im_trans_tasks set edit_end_date = dummy_date where dummy_date is not null;
-- update im_trans_tasks set proof_id = edit_id where proof_id is null;
-- update im_trans_tasks set proof_end_date = edit_end_date where proof_end_date is null;
-- alter table im_trans_tasks drop column dummy_id;
-- alter table im_trans_tasks drop column dummy_date;
-- update im_trans_tasks set edit_end_date = null where edit_end_date = proof_end_date ;
-- update im_trans_tasks set edit_id = null where edit_id = proof_id ;
