SELECT acs_log__debug('/packages/intranet-cust-kolibri/sql/postgresql/upgrade/upgrade-5.0.0.0.6-5.0.0.0.7.sql','');

select im_category_new ('7015', 'Inhalt', 'Intranet Translation Quality Type');
update im_categories set aux_string1 = 'Der Text wurde verstanden und korrekt übertragen' where category_id = 7015;
select im_category_new ('7016', 'Lesbarkeit & Stil', 'Intranet Translation Quality Type');
update im_categories set aux_string1 = 'Aktiv, kurze Sätze, nicht zu komplexe Formulierungen, auffordernd, spannend, gute Headlines' where category_id = 7016;
select im_category_new ('7017', 'Fachwissen und Terminologie', 'Intranet Translation Quality Type');
select im_category_new ('7018', 'Beachtung des Briefings', 'Intranet Translation Quality Type');
update im_categories set aux_string1= 'Tonalität, Wording, interne Verlinkungen (Content)' where category_id = 7018;
select im_category_new ('7019', 'Rechtschreibung & Grammatik', 'Intranet Translation Quality Type');
update im_categories set aux_string1='Unter anderem (Tipp)Fehler, Deklinationen, Syntax, Konjugationen, Interpunktion, etc.' where category_id = 7019;
select im_category_new ('7020', 'Erfüllung der SEO-Anforderungen', 'Intranet Translation Quality Type');


