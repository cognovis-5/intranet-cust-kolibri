SELECT acs_log__debug('/packages/intranet-cust-kolibri/sql/postgresql/upgrade/upgrade-5.0.0.0.3-5.0.0.0.4.sql','');

-- Update the workflow steps to the roles

update im_categories set aux_string1 = 'trans' where category = 'Translator' and category_type = 'Intranet Skill Role';
update im_categories set aux_string1 = 'proof' where category = 'Proofreader' and category_type = 'Intranet Skill Role';
update im_categories set aux_string1 = 'copy' where category = 'Copywriter' and category_type = 'Intranet Skill Role';
update im_categories set aux_string1 = 'edit' where category = 'Copy editor' and category_type = 'Intranet Skill Role';
update im_categories set aux_string1 = 'cert' where category = 'Sworn translator' and category_type = 'Intranet Skill Role';