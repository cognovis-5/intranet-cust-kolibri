SELECT acs_log__debug('/packages/intranet-cust-kolibri/sql/postgresql/upgrade/upgrade-5.0.0.2.3-5.0.0.2.4.sql','');

create or replace function inline_0 ()
returns integer as $body$
declare
	v_assignment record;
    v_folder record;
    v_assignee_name text;
    v_affected_rows integer default 0;
begin
    -- First we run update query which breaks security inheritance for all content_folders named Lieferung and Projektinfos
    UPDATE acs_objects SET security_inherit_p = 'f' WHERE (title = 'Lieferung' or title = 'Projektinfos') AND object_type = 'content_folder';
    FOR v_assignment IN select assignee_id from im_freelance_assignments
        -- Next we run two loops to revoke permissions for freelancers
        LOOP
            FOR v_folder IN (select cf.folder_id, cf.label from cr_folders cf, acs_permissions ap where cf.folder_id = ap.object_id and grantee_id = v_assignment.assignee_id and (label = 'Lieferung' or label = 'Projektinfos'))
                LOOP
                    PERFORM acs_permission__revoke_permission(v_folder.folder_id,v_assignment.assignee_id,'read');
                    SELECT im_name_from_user_id(v_assignment.assignee_id) into v_assignee_name;
                    RAISE NOTICE 'Revoked READ permission for % folder for % (%)',v_folder.label,v_assignee_name,v_assignment.assignee_id;
                    v_affected_rows := v_affected_rows + 1;
                END LOOP;
        END LOOP;
        RAISE NOTICE 'affected rows: % ',v_affected_rows;
	return 0;
end;$body$ language 'plpgsql';


select inline_0();
drop function inline_0();