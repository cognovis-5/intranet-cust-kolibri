SELECT acs_log__debug('/packages/intranet-cust-kolibri/sql/postgresql/upgrade/upgrade-5.0.0.4.2-5.0.0.4.3.sql','');

create or replace function grant_price_permissions_for_employees()

    returns integer as $body$

    declare
    v_price record;
    v_affected_rows integer default 0;

    begin

    for v_price in (select * from im_prices)
        loop
            PERFORM acs_permission__grant_permission(v_price.price_id,463,'read');
            PERFORM acs_permission__grant_permission(v_price.price_id,463,'write');
            v_affected_rows := v_affected_rows + 1;
        end loop;
        RAISE NOTICE 'affected im_prices rows: % ',v_affected_rows;
    return 0;

end;$body$ language 'plpgsql';

select grant_price_permissions_for_empoloyees();


create or replace function grant_notes_permissions_for_employees()

    returns integer as $body$

    declare
    v_note record;
    v_affected_rows integer default 0;

    begin

    for v_note in (select * from im_notes)
        loop
            PERFORM acs_permission__grant_permission(v_note.note_id,463,'read');
            PERFORM acs_permission__grant_permission(v_note.note_id,463,'write');
            v_affected_rows := v_affected_rows + 1;
        end loop;
        RAISE NOTICE 'affected im_notes rows: % ',v_affected_rows;
    return 0;

end;$body$ language 'plpgsql';

select grant_notes_permissions_for_employees();