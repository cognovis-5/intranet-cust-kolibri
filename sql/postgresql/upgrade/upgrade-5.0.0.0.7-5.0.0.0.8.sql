SELECT acs_log__debug('/packages/intranet-cust-kolibri/sql/postgresql/upgrade/upgrade-5.0.0.0.7-5.0.0.0.8.sql','');

-- Clean up configuration as the new notifications and notes don't work like this anymore.
update im_projects set project_status_id = 81 where project_id in (select project_id from im_projects where end_date < now() and project_status_id = 76);
delete from im_freelance_notifications;
update im_component_plugins set component_tcl = 'im_notes_component -object_id $project_id' where plugin_id = 29113;