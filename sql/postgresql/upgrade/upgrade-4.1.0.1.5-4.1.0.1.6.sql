SELECT acs_log__debug('/packages/intranet-cust-kolibri/sql/postgresql/upgrade/upgrade-4.1.0.1.5-4.1.0.1.6.sql','');

-- Add acceptance date
create or replace function inline_0 ()
returns integer as $body$
declare
        v_count  integer;
begin
        select count(*) into v_count from user_tab_columns
        where lower(table_name) = 'im_projects' and lower(column_name) = 'acceptance_date';
        IF v_count > 0 THEN
                return 1;
        END IF;

        alter table im_projects
        add column acceptance_date timestamptz;

        return 0;
end;$body$ language 'plpgsql';
select inline_0();
drop function inline_0();

SELECT im_dynfield_attribute_new ('im_project', 'acceptance_date', 'Acceptance Date', 'jq_timestamp', 'timestamp', 't', 25, 't');
update im_dynfield_layout set pos_y = '25', sort_key = '25' where attribute_id = (select attribute_id from im_dynfield_attributes where acs_attribute_id = (select attribute_id from acs_attributes where attribute_name = 'acceptance_date'));
-- insert into im_dynfield_layout (pos_y,sort_key,page_url,attribute_id) values ('25','25','/intranet/projects/index',(select attribute_id from im_dynfield_attributes where acs_attribute_id = (select attribute_id from acs_attributes where attribute_name = 'acceptance_date')));
update im_dynfield_attributes set also_hard_coded_p = 't' where acs_attribute_id = (select attribute_id from acs_attributes where attribute_name = 'acceptance_date');
update im_dynfield_type_attribute_map set display_mode = 'display' where attribute_id = (select attribute_id from im_dynfield_attributes where acs_attribute_id = (select attribute_id from acs_attributes where attribute_name = 'acceptance_date'));

-- Add acceptance user
create or replace function inline_0 ()
returns integer as $body$
declare
        v_count  integer;
begin
        select count(*) into v_count from user_tab_columns
        where lower(table_name) = 'im_projects' and lower(column_name) = 'acceptance_user_id';
        IF v_count > 0 THEN
                return 1;
        END IF;

        alter table im_projects
        add column acceptance_user_id integer references users;

        return 0;
end;$body$ language 'plpgsql';
select inline_0();
drop function inline_0();

SELECT im_dynfield_attribute_new ('im_project', 'acceptance_user_id', 'Acceptance User', 'customer_contact', 'integer', 't', 26, 't');
update im_dynfield_layout set pos_y = '26', sort_key = '26' where attribute_id = (select attribute_id from im_dynfield_attributes where acs_attribute_id = (select attribute_id from acs_attributes where attribute_name = 'acceptance_user_id'));
-- insert into im_dynfield_layout (pos_y,sort_key,page_url,attribute_id) values ('26','26','/intranet/projects/index',(select attribute_id from im_dynfield_attributes where acs_attribute_id = (select attribute_id from acs_attributes where attribute_name = 'acceptance_user_id')));
update im_dynfield_attributes set also_hard_coded_p = 't' where acs_attribute_id = (select attribute_id from acs_attributes where attribute_name = 'acceptance_user_id');
update im_dynfield_type_attribute_map set display_mode = 'display' where attribute_id = (select attribute_id from im_dynfield_attributes where acs_attribute_id = (select attribute_id from acs_attributes where attribute_name = 'acceptance_user_id'));
