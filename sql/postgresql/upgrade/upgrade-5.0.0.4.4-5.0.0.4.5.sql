SELECT acs_log__debug('/packages/intranet-cust-kolibri/sql/postgresql/upgrade/upgrade-5.0.0.4.4-5.0.0.4.5.sql','');

SELECT im_category_new ('10000994', 'MT', 'Intranet Project Type');

UPDATE im_categories SET aux_html2 = 'fas fa-robot' WHERE category_id = 10000994;
UPDATE im_categories SET aux_string1 = 'proof' WHERE category_id = 10000994;
UPDATE im_categories SET sort_order = 22 WHERE category_id = 10000994;
UPDATE im_categories SET enabled_p = 't' WHERE category_id = 10000994;
UPDATE im_categories SET category_description = 'Maschinelle Übersetzung: Nur Korrektur (Pre-Editing & Post-Editing intern)' WHERE category_id = 10000994;