SELECT acs_log__debug('/packages/intranet-cust-kolibri/sql/postgresql/upgrade/upgrade-5.0.0.2.4-5.0.0.2.5.sql','');

create or replace function inline_0 ()
returns integer as $body$
declare
	v_project record;
    v_project_main_folder_id integer;
    v_folder record;
    v_employee_group_id integer;
    v_affected_rows integer default 0;
begin
    -- First let's get employee group id
    SELECT g.group_id from	groups g, im_profiles p where g.group_id = p.profile_id and group_name = 'Employees' into v_employee_group_id;
    -- First we start loop which will iterate thru all im_projects
    FOR v_project IN select project_id from im_projects where project_lead_id is not null
        LOOP
            -- Now we need to figure out folder_id of main project doler
            select 
                object_id_two from acs_rels r, cr_items i where r.object_id_one = v_project.project_id 
                and r.object_id_two = i.item_id 
                and r.rel_type = 'project_folder' 
                order by r.object_id_two asc limit 1
            into v_project_main_folder_id;
            -- Now we iterate thru all project subfolders to add permissions for PM's
            FOR v_folder IN (select object_id, name from fs_objects where parent_id = v_project_main_folder_id and type = 'folder')
                LOOP
                    PERFORM acs_permission__grant_permission(v_folder.object_id,v_employee_group_id,'read');
                    v_affected_rows := v_affected_rows + 1;
                END LOOP;
        END LOOP;
        RAISE NOTICE 'affected rows: % ',v_affected_rows;
return 0;
end;$body$ language 'plpgsql';

select inline_0();
drop function inline_0();